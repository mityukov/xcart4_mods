<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin core class
 */
class Time extends MobileAdmin\Base
{
    const SEARCH_TIME_THIS_DAY      = 'today';
    const SEARCH_TIME_THIS_WEEK     = 'week';
    const SEARCH_TIME_THIS_MONTH    = 'month';
    const SEARCH_TIME_ALL           = 'all';

    /**
     * Get current user time
     *
     * @return integer
     */
    public function getUserTime()
    {
        return $this->getCore()->getTime() + $this->getCore()->getTimeShift();
    }

    /**
     * Get date range in unix timestamp
     *
     * @param string $date Date range type
     *
     * @return array
     */
    public function getDateConditionAsTimestamp($date)
    {
        return $this->getSearchIntervalsByType($date);
    }

    /**
     * Get date range in text format
     *
     * @param string $date Date range type
     *
     * @return array
     */
    public function getDateConditionAsString($date)
    {
        $intervals = $this->getSearchIntervalsByType($date);

        $return = array();

        if (!empty($intervals)) {
            $from = new \DateTime();
            $from->setTimestamp($intervals['from'] + $this->getCore()->getTimeShift());

            $to = new \DateTime();
            $to->setTimestamp($intervals['to'] + 1 + $this->getCore()->getTimeShift());

            $return = array(
                'from'  => $from->format('Y-m-d'),
                'to'    => $to->format('Y-m-d')
            );
        }

        return $return;
    }

    /**
     * Convert server time to user time
     *
     * @param integer $time Server time
     *
     * @return integer
     */
    public function convertToUserTime($time)
    {
        return $time + $this->getCore()->getTimeShift();
    }

    /**
     * Get user month from server timestamp
     *
     * @param integer $time Server time
     *
     * @return integer
     */
    public function getUserMonth($time)
    {
        $userTime = new \DateTime();
        $userTime->setTimestamp($this->convertToUserTime($time));

        return (int) $userTime->format('n');
    }

    /**
     * Get user month name from server timestamp
     *
     * @param integer $time Server time
     * @param boolean $convert Do conversion OPTIONAL
     *
     * @return string
     */
    public function getUserMonthName($time, $convert = true)
    {
        $userTime = new \DateTime();

        if ($convert) {
            $userTime->setTimestamp($this->convertToUserTime($time));
        } else {
            $userTime->setTimestamp($time);
        }

        $xcTime = new \DateTime();
        if ($convert) {$xcTime->setTimestamp($this->convertToUserTime($time));} else {$xcTime->setTimestamp($time);}

        return strtoupper($userTime->format('M'));
    }

    /**
     * Get user day from server timestamp
     *
     * @param integer $time Server time
     * @param boolean $convert Do conversion OPTIONAL
     *
     * @return integer
     */
    public function getUserDay($time, $convert = true)
    {
        $userTime = new \DateTime();

        if ($convert) {
            $userTime->setTimestamp($this->convertToUserTime($time));
        } else {
            $userTime->setTimestamp($time);
        }

        return (int) $userTime->format('d');
    }

    /**
     * Get unix timestamps for search intervals
     *
     * @param string $date
     *
     * @return array
     */
    protected function getSearchIntervalsByType($date)
    {
        $return = array();

        $userTime = new \DateTime();
        $userTime->setTimestamp($this->getUserTime());

        if (static::SEARCH_TIME_THIS_DAY == $date) {

            $from = $userTime->setTime(0, 0, 0)->getTimestamp();
            $to = $userTime->setTime(23, 59, 59)->getTimestamp();

            $return = array(
                'from'  => $from - $this->getCore()->getTimeShift(),
                'to'    => $to - $this->getCore()->getTimeShift()
            );

        } elseif (static::SEARCH_TIME_THIS_WEEK == $date) {

            $currentYear = (int) $userTime->format('Y');
            $currentWeek = (int) $userTime->format('W');

            $from = $userTime->setISODate($currentYear, $currentWeek, 1)->setTime(0,0,0)->getTimestamp();
            $to = $userTime->setISODate($currentYear, $currentWeek, 7)->setTime(23,59,59)->getTimestamp();

            $return = array(
                'from'  => $from - $this->getCore()->getTimeShift(),
                'to'    => $to - $this->getCore()->getTimeShift()
            );

        } elseif (static::SEARCH_TIME_THIS_MONTH == $date) {

            $currentYear = (int) $userTime->format('Y');
            $currentMonth = (int) $userTime->format('n');

            $from = $userTime->setDate($currentYear, $currentMonth, 1)->setTime(0,0,0)->getTimestamp();
            $to = $userTime->setDate($currentYear, $currentMonth + 1, 1)->setTime(0,0,0)->getTimestamp() - 1;

            $return = array(
                'from'  => $from - $this->getCore()->getTimeShift(),
                'to'    => $to - $this->getCore()->getTimeShift()
            );
        }

        return $return;
    }
}
