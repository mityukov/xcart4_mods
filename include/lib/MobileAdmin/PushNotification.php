<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin GCM class
 */
class PushNotification extends MobileAdmin\Base
{
    const QUEUE_STATUS_QUEUED     = 'Q';
    const QUEUE_STATUS_SENDING    = 'S';
    const QUEUE_STATUS_COMPLETE   = 'C';

    const STATUS_INFORMATION    = 'I';

    /**
     * Registered push notification services
     *
     * @var \MobileAdmin\NotificationCervices\PushNotificationService[]
     */
    protected $registeredServices = array();

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->registerService(NotificationCervices\GCM::getInstance());
        $this->registerService(NotificationCervices\APNS::getInstance());
    }

    /**
     * Execute query manager
     *
     * @return void
     */
    public function executeQueueManager()
    {
        ignore_user_abort(true);
        set_time_limit(86400);

        if ($this->getCore()->lockNotificationQueueManager()) {
            $notification = $this->getCore()->getDataHandler()->popQueuedNotification();

            while (isset($notification)) {
                $pushMessage = new MobileAdmin\Containers\PushMessageContainer(
                    json_decode($notification->message, true)
                );

                $this->sendNotification($pushMessage);

                $this->getCore()->getDataHandler()->updateNotificationStatus(
                    $notification->id,
                    static::QUEUE_STATUS_COMPLETE
                );

                sleep(1);

                $notification = $this->getCore()->getDataHandler()->popQueuedNotification();
            }

            $this->getCore()->unlockNotificationQueueManager();
        }
    }

    /**
     * Register new push notification service
     *
     * @param \MobileAdmin\NotificationCervices\PushNotificationService $service Push notification service
     *
     * @return void
     */
    public function registerService($service)
    {
        $this->registeredServices[] = $service;
    }

    /**
     * Add message to the queue
     *
     * @param string $message Message
     *
     * @return boolean
     */
    public function queueNotification($message)
    {
        $return = $this->getCore()->getDataHandler()->addNotificationToQueue($message);

        if ($return) {
            $this->getCore()->callNotificationManager();
        }

        return $return;
    }

    /**
     * Sent push notifications to all registered devices
     *
     * @param \MobileAdmin\Containers\PushMessageContainer $message Message
     *
     * @return array
     */
    public function sendNotification($message)
    {
        $return = array();

        $registeredDevices = $this->getCore()->getDataHandler()->getAllAvailableDevices();

        foreach ($this->registeredServices as $service) {
            try {
                $return[$service->getOsType()] = $service->sendNotification(
                    $service->filterRegisteredDevices($registeredDevices),
                    $message
                );
            } catch (MobileAdmin\Exception $e) {
                $this->getCore()->addLogFile('push_notiffications', $e->getErrorResponse()->toArray());
            }
        }

        return $return;
    }

    /**
     * Get list of registered OS push notification services
     *
     * @return array
     */
    public function getRegisteredOsList()
    {
        $return = array();

        foreach ($this->registeredServices as $service) {
            $return[$service->getOsType()] = $service->getOsName();
        }

        return $return;
    }

    /**
     * Send notification about new order
     *
     * @param integer $orderId Order ID
     *
     * @return boolean
     */
    public function sendNewOrderNotification($orderId)
    {
        $notification = new MobileAdmin\Containers\PushMessageContainer(
            array(
                'status'    => self::STATUS_INFORMATION,
                'message'   => MobileAdmin\Message::getInstance()->translate(
                    MobileAdmin\Message::NEW_ORDER_RECIEVED
                ),
                'data'      => $orderId
            )
        );

        return $this->queueNotification($notification->toJson());
    }

    /**
     * Send notification about low stock product
     *
     * @param integer $productId Product ID
     *
     * @return boolean
     */
    public function sendLowStockNotification($productId)
    {
        $notification = new MobileAdmin\Containers\PushMessageContainer(
            array(
                'status'    => self::STATUS_INFORMATION,
                'message'   => MobileAdmin\Message::getInstance()->translate(
                    MobileAdmin\Message::LOW_STOCK_PRODUCT
                ),
                'data'      => $productId
            )
        );

        return $this->queueNotification($notification->toJson());
    }

    /**
     * Send notification about low stock product
     *
     * @param string $model Product ID
     *
     * @return boolean
     */
    public function sendNewDeviceRegisteredNotification($model)
    {
        $notification = new Containers\PushMessageContainer(
            array(
                'status'    => self::STATUS_INFORMATION,
                'message'   => MobileAdmin\Message::getInstance()->translate(
                    MobileAdmin\Message::DEVICE_REGISTERED
                ),
                'data'      => $model
            )
        );

        return $this->queueNotification($notification->toJson());
    }
}
