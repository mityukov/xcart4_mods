<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

/**
 * Mobile Admin base class
 */
class Base
{
    /**
     * Current core library class name
     *
     * @var string
     */
    protected static $coreClass = null;

    /**
     * Array of instances for all derived classes
     *
     * @var array
     */
    protected static $instances = array();

    /**
     * Destruct and recreate singleton
     *
     * @return static
     */
    public static function resetInstance()
    {
        $className = get_called_class();

        // Create new instance of the object (if it is not already created)
        if (isset(static::$instances[$className])) {
            unset(static::$instances[$className]);
        }

        return static::getInstance();
    }

    /**
     * Method to access a singleton
     *
     * @return static
     */
    public static function getInstance()
    {
        $className = get_called_class();

        // Create new instance of the object (if it is not already created)
        if (!isset(static::$instances[$className])) {
            static::$instances[$className] = new $className();
        }

        return static::$instances[$className];
    }

    /**
     * Returns current core class object
     *
     * @return \MobileAdmin\Core
     */
    public function getCore()
    {
        return static::$coreClass;
    }

    /**
     * Set the Core class
     *
     * @param string $className Class name
     *
     * @return void
     */
    protected function setCore($className)
    {
        if (isset(static::$instances[$className])) {
            static::$coreClass = static::$instances[$className];
        } else {
            static::$coreClass = new $className();
            static::$instances[$className] = static::$coreClass;
        }
    }
}
