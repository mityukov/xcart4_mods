<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin core class
 */
class Message extends MobileAdmin\Base
{
    const SUCCESS               = 'msg_mobad_logic_success';
    const TEST                  = 'msg_mobad_logic_test';

    const NOT_AVAILABLE         = 'lbl_mobad_not_available';

    const DEVICE_REGISTERED     = 'msg_mobad_device_registered';
    const DEVICE_UNREGISTERED   = 'msg_mobad_device_unregistered';

    const REVIEWS_NOT_AVAIL     = 'msg_mobad_review_not_available';
    const REVIEW_NOT_FOUND      = 'msg_mobad_review_not_found';

    const NEW_ORDER_RECIEVED    = 'msg_mobad_new_order_received';
    const LOW_STOCK_PRODUCT     = 'msg_mobad_low_stock_product';

    const DC_CREATE_SUCCESS     = 'msg_mobad_discount_coupon_created';

    /**
     * Translate message
     *
     * @param string $name      Name
     * @param array  $arguments Arguments OPTIONAL
     *
     * @return string
     */
    public function translate($name, array $arguments = array())
    {
        return $this->getCore()->translate($name, $arguments);
    }
}
