<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin base class
 */
class Exception extends \Exception
{
    /**
     * Error Codes
     */
    /**
     * Common errors
     */
    const SUCCESS                           = 0;
    const COMMON_ERROR                      = 9999;
    const WRONG_API_KEY                     = 1;
    const ACTION_NOT_FOUND                  = 2;
    const BAD_INPUT_DATA                    = 3;

    /**
     * Device registration
     */
    const DEVICE_REGISTERED                 = 10;
    const DEVICE_UNREGISTERED               = 11;
    const DEVICE_REG_ERROR                  = 12;
    const DEVICE_NOT_FOUND                  = 13;
    const DEVICE_ALREADY_REGISTERED         = 14;

    /**
     * Reviews
     */
    const REVIEWS_DISABLED                  = 20;
    const REVIEW_NOT_FOUND                  = 21;
    const REVIEW_DATE_NOT_AVAIL             = 22;

    /**
     * Tracking
     */
    const TRACKING_EMPTY                    = 30;

    /**
     * Order info
     */
    const ORDER_NOT_FOUND                   = 41;
    const STATUS_EMPTY                      = 42;
    const INVALID_ORDER_STATUS              = 43;

    /**
     * Customer info
     */
    const CUSTOMER_NOT_FOUND                = 50;
    const CANT_CHANGE_CUSTOMER_STATUS       = 51;

    /**
     * Discount coupon
     */
    const DC_SUCCESS                        = 60;
    const DC_MODULE_NOT_FOUND               = 61;
    const DC_INVALID_CODE                   = 62;
    const DC_CODE_EXISTS                    = 63;
    const DC_DISCOUNT_ERROR                 = 64;

    /**
     * Product management
     */
    const PRODUCT_NOT_FOUND                 = 70;
    const DETAILED_IMG_MODULE_DISABLED      = 71;
    const PRODUCT_OPTIONS_MODULE_DISABLED   = 72;

    /**
     * Store integration errors
     */
    const CONFIG_NO_TRANSLATION_FOR_PATH    = 81;
    const CONFIG_VALUE_NOT_FOUND            = 82;

    /**
     * Network error
     */
    const NET_SOCKET_ERROR                  = 91;

    /**
     * Error code
     *
     * @var integer
     */
    protected $error = self::SUCCESS;

    /**
     * Additional data
     *
     * @var array
     */
    protected $arguments = array();

    /**
     * Error messages
     *
     * @var array
     */
    protected $errorMessages = array(
        self::COMMON_ERROR                      => 'msg_err_mobad_logic_error',
        self::WRONG_API_KEY                     => 'msg_err_mobad_wrong_api_key',
        self::ACTION_NOT_FOUND                  => 'msg_err_mobad_action_not_found',
        self::BAD_INPUT_DATA                    => 'msg_err_mobad_field_data_invalid',
        self::DEVICE_REGISTERED                 => 'msg_err_mobad_device_registered',
        self::DEVICE_UNREGISTERED               => 'msg_err_mobad_device_unregistered',
        self::DEVICE_REG_ERROR                  => 'msg_err_mobad_device_registration_error',
        self::DEVICE_NOT_FOUND                  => 'msg_err_mobad_device_not_registered',
        self::REVIEW_NOT_FOUND                  => 'msg_err_mobad_review_not_found',
        self::REVIEW_DATE_NOT_AVAIL             => 'msg_err_mobad_reviews_by_date_na',
        self::TRACKING_EMPTY                    => 'msg_err_mobad_tracking_empty',
        self::ORDER_NOT_FOUND                   => 'msg_err_mobad_order_not_found',
        self::INVALID_ORDER_STATUS              => 'msg_err_mobad_order_status_invalid',
        self::STATUS_EMPTY                      => 'msg_err_mobad_status_empty',
        self::CUSTOMER_NOT_FOUND                => 'msg_err_mobad_customer_not_found',
        self::CANT_CHANGE_CUSTOMER_STATUS       => 'msg_err_mobad_cant_change_user_status',
        self::DC_SUCCESS                        => 'msg_err_mobad_discount_coupons_add',
        self::DC_MODULE_NOT_FOUND               => 'msg_err_mobad_discount_coupon_nomod',
        self::DC_INVALID_CODE                   => 'msg_err_mobad_discount_coupon_inv_code',
        self::DC_CODE_EXISTS                    => 'msg_err_mobad_discount_coupon_code_exists',
        self::DC_DISCOUNT_ERROR                 => 'msg_err_mobad_discount_coupon_discount_error',
        self::PRODUCT_NOT_FOUND                 => 'msg_err_mobad_product_not_found',
        self::DETAILED_IMG_MODULE_DISABLED      => 'msg_err_mobad_detailed_images_nomod',
        self::PRODUCT_OPTIONS_MODULE_DISABLED   => 'msg_err_mobad_product_options_nomod',
        self::CONFIG_NO_TRANSLATION_FOR_PATH    => 'msg_err_mobad_no_translation_for_config',
        self::CONFIG_VALUE_NOT_FOUND            => 'msg_err_mobad_config_value_not_found',
        self::NET_SOCKET_ERROR                  => 'msg_err_mobad_socket_error'
    );

    /**
     * Construct new error
     *
     * @param integer $error     Error code OPTIONAL
     * @param array   $arguments Data       OPTIONAL
     *
     * @return void
     */
    public function __construct($error = self::COMMON_ERROR, array $arguments = array())
    {
        $this->error = $error;
        $this->arguments = $arguments;

        MobileAdmin\Logger::getInstance()->log(
            array(
                'error' => $this->getErrorResponse()->toArray(),
                'trace' => "\n" . $this->getTraceAsString()
            )
        );
    }

    /**
     * Get error API response
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    public function getErrorResponse()
    {
        return new MobileAdmin\Containers\APIResponseContainer(
            array(
                'status'    => MobileAdmin\Controller::STATUS_ERROR,
                'message'   => $this->getErrorMessage(),
                'data'      => ''
            )
        );
    }

    /**
     * Get error message
     *
     * @return string
     */
    protected function getErrorMessage()
    {
        $name = $this->errorMessages[self::COMMON_ERROR];

        if (isset($this->errorMessages[$this->error])) {
            $name = $this->errorMessages[$this->error];
        }

        return MobileAdmin\Message::getInstance()->translate($name, $this->arguments);
    }
}
