<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin core class
 */
class Logger extends MobileAdmin\Base
{
    /**
     * Add message to the system log file
     *
     * @param string|array $data Data
     * @param boolean      $name Log file name OPTIONAL
     *
     * @return void
     */
    public function log($data, $name = false)
    {
        if (
            defined('MOBILE_ADMIN_DEBUG')
            && 1 == MOBILE_ADMIN_DEBUG
        ) {
            if (false === $name) {
                $name = MobileAdmin\Config::LOG_FILE_NAME;
            }

            if (is_array($data)) {
                $data = var_export($data, true);
            }

            $this->getCore()->addLogFile($name, "\n" . $data);
        }
    }

    /**
     * Add message to the system log file
     *
     * @param string|array $data Data
     *
     * @return void
     */
    public function logAccess($data)
    {
        $data = var_export($data, true);

        $this->log("\n" . $data, MobileAdmin\Config::LOG_ACCESS_FILE_NAME);
    }
}
