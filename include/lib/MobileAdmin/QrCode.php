<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin core class
 */
class QrCode extends MobileAdmin\Base
{
    /**
     * Method to access a singleton
     *
     * @return \MobileAdmin\QrCode
     */
    public static function getInstance()
    {
        $className = get_called_class();

        $return = null;

        if (!isset(static::$instances[$className])) {
            $return = parent::getInstance();

            $return->initQrLib();
        } else {
            $return == parent::getInstance();
        }

        return $return;
    }

    /**
     * Output QR Code image
     *
     * @param $string
     *
     * @return void
     */
    public function getQrCodeFromString($string)
    {
        \QRcode::png($string);
    }

    /**
     * Create temporary directorie if not exist
     *
     * @param $dir
     *
     * @return void
     */
    protected function createCacheDirectory($dir)
    {
        $DS = Config::getInstance()->getDirectorySeparator();

        if (!is_readable($dir)) {
            $requiredDirs = array();

            $dirs = explode($DS, $dir);

            while (!is_writable(implode($DS, $dirs))) {
                $requiredDirs[] = implode($DS, $dirs);

                array_pop($dirs);
            }

            if (!empty($requiredDirs)) {
                for ($i = count($requiredDirs) - 1; $i >= 0; $i--) {
                    @mkdir($requiredDirs[$i]);
                    @chmod($requiredDirs[$i], 0755);
                }
            }
        }
    }

    /**
     * Get QR Code library cache directory
     *
     * @return string
     */
    protected function getQrLibCacheDir()
    {
        return Config::getInstance()->getQrLibCacheDir();
    }

    /**
     * Get QR Code library log directory
     *
     * @return string
     */
    protected function getQrLibLogDir()
    {
        return Config::getInstance()->getQrLibLogDir();
    }

    /**
     * Initiate QR code generating library
     *
     * @return void;
     */
    protected function initQrLib()
    {
        $this->createCacheDirectory($this->getQrLibCacheDir());
        $this->createCacheDirectory($this->getQrLibLogDir());

        define('QR_CACHE_DIR', $this->getQrLibCacheDir());
        define('QR_LOG_DIR', $this->getQrLibLogDir());

        $DS = Config::getInstance()->getDirectorySeparator();

        require_once $this->getCore()->getLibDir() . $DS . 'lib' . $DS . 'phpqrcode' . $DS . 'qrlib.php';
    }
}
