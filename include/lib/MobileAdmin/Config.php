<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin core class
 */
class Config extends MobileAdmin\Base
{
    const DEFAULT_API_KEY           = 'testKey';
    const ALLOWED_KEY_SYMBOLS       = '01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    const APP_SERVER_LISTENER_URL   = 'https://vm-constructor.cloudapp.net/AppServerListener/api';

    const LOG_FILE_NAME             = 'mobile_admin';
    const LOG_ACCESS_FILE_NAME      = 'mobile_admin_access';

    const GCM_URL                   = 'https://android.googleapis.com/gcm/send';
    const GOOGLE_API_KEY            = 'AIzaSyCkei4x6KckaE3VdlfZFcZFvqrbdlWwtNk';

    const APNS_PROVIDER_CERT_FILE   = 'NotificationCervices{sep}certs{sep}CertKeyProd.pem';
    const APNS_PROVIDER_CERT_PASSWD = 'push';
    const APNS_ROOT_AUTH_CERT       = 'NotificationCervices{sep}certs{sep}entrust_2048_ca.cer';

    /**
     * Mobile Admin API key for current shop
     *
     * @var string
     */
    protected $apiKey = '';

    /**
     * Mobile Admin API entry point for current shop
     *
     * @var string
     */
    protected $connectorUrl = '';

    /**
     * Timeout for connections
     *
     * @var integer
     */
    protected $connectionTimeout = 10;

    /**
     * Directory separator
     *
     * @var string
     */
    protected $directorySeparator = '/';

    /**
     * QR Code library cache directory
     *
     * @var string
     */
    protected $qrLibCacheDir = '';

    /**
     * QR Code library log directory
     *
     * @var string
     */
    protected $qrLibLogDir = '';

    /**
     * Notification queue manager lock file
     *
     * @var string
     */
    protected $queueManagerLockFile = '';

    /**
     * Configuration data
     *
     * @param array $data Data
     *
     * @return void
     */
    public function setData($data)
    {
        foreach ($data as $name => $value) {
            if (property_exists(__CLASS__, $name)) {
                $this->$name = $value;
            }
        }
    }

    /**
     * Get API key
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Get Connection Timeout
     *
     * @return integer
     */
    public function getConnectionTimeout()
    {
        return $this->connectionTimeout;
    }

    /**
     * Get connector URL
     *
     * @param boolean $skipKey Skip api key part OPTIONAL
     *
     * @return string
     */
    public function getConnectorUrl($skipKey = false)
    {
        $hasParams = false;

        $url = parse_url($this->connectorUrl);

        $connectorUrl = $url['scheme'] . '://' . $url['host'] . $url['path'];

        if (
            isset($url['query'])
            && !empty($url['query'])
        ) {
            $hasParams = true;
            $connectorUrl .= '?' . $url['query'];
        }

        if (!$skipKey) {
            $connectorUrl .= ($hasParams ? '&key=' : '?key=') . $this->getApiKey();
        }

        return $connectorUrl;
    }

    /**
     * Get directory separator
     *
     * @return string
     */
    public function getDirectorySeparator()
    {
        return $this->directorySeparator;
    }

    /**
     * Get QR Code library cache directory
     *
     * @return string
     */
    public function getQrLibCacheDir()
    {
        return $this->qrLibCacheDir;
    }

    /**
     * Get QR Code library log directory
     *
     * @return string
     */
    public function getQrLibLogDir()
    {
        return $this->qrLibLogDir;
    }

    /**
     * Get APNS environment
     *
     * @return integer
     */
    public function getApnsEnvironment()
    {
        return \ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION;
    }

    /**
     * Get APNS provider certificate file path
     *
     * @return string
     */
    public function getApnsProviderCertificate()
    {
        $DS = $this->getDirectorySeparator();

        return $this->getCore()->getLibDir() . $DS . str_replace('{sep}', $DS, self::APNS_PROVIDER_CERT_FILE);
    }

    /**
     * Get APNS root certificate file path
     *
     * @return string
     */
    public function getApnsRootCertificate()
    {
        $DS = $this->getDirectorySeparator();

        return $this->getCore()->getLibDir() . $DS . str_replace('{sep}', $DS, self::APNS_ROOT_AUTH_CERT);
    }

    /**
     * Get notification queue manager lock file
     *
     * @return string
     */
    public function getNotificationManagerLockFile()
    {
        return $this->queueManagerLockFile;
    }
}
