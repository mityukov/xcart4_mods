<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin data handler class
 */
abstract class DataHandler extends MobileAdmin\Base
{
    /**
     * Register device within shop
     *
     * @param \MobileAdmin\Containers\DeviceDataContainer $device Device data
     *
     * @return boolean
     */
    abstract public function registerDevice($device);

    /**
     * Get device info by registration ID
     *
     * @param string $regId Registration ID
     *
     * @return mixed
     */
    abstract public function getDevice($regId);

    /**
     * Delete registered device
     *
     * @param string $regId Registration ID
     *
     * @return boolean
     */
    abstract public function deleteDevice($regId);

    /**
     * Get all registered devices
     *
     * @return \MobileAdmin\Containers\DeviceDataContainer[]
     */
    abstract public function getAllDevices();

    /**
     * Get all devices available for receiving messages
     *
     * @return \MobileAdmin\Containers\DeviceDataContainer[]
     */
    abstract public function getAllAvailableDevices();

    /**
     * Get all registered IDs
     *
     * @return array
     */
    abstract public function getAllRegIds();

    /**
     * Get dashboard data
     *
     * @return \MobileAdmin\Containers\DashboardDataContainer
     */
    abstract public function getDashboardData();

    /**
     * Get orders list
     *
     * @param \MobileAdmin\Containers\ActionData\LastOrdersInput $input Input data
     *
     * @return \MobileAdmin\Containers\OrdersListItemContainer[]
     */
    abstract public function getLastOrdersList($input);

    /**
     * Get order info
     *
     * @param \MobileAdmin\Containers\ActionData\OrderInfoInput $input Input data
     *
     * @return \MobileAdmin\Containers\OrderDataContainer
     */
    abstract public function getOrderInfo($input);

    /**
     * Set tracking numbers
     *
     * @param \MobileAdmin\Containers\ActionData\ChangeTrackingInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    abstract public function setTrackingNumbers($input);

    /**
     * Set order status
     *
     * @param \MobileAdmin\Containers\ActionData\ChangeStatusInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    abstract public function setOrderStatus($input);

    /**
     * Set product availability
     *
     * @param \MobileAdmin\Containers\ActionData\ChangeAvailableInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    abstract public function setProductAvailability($input);

    /**
     * Get products list
     *
     * @param \MobileAdmin\Containers\ActionData\ProductsInput $input Input data
     *
     * @return \MobileAdmin\Containers\ProductsListItemContainer[]
     */
    abstract public function getProductsList($input);

    /**
     * Get product info by product ID
     *
     * @param \MobileAdmin\Containers\ActionData\ProductInfoInput $input Input data
     *
     * @return \MobileAdmin\Containers\ProductInfoContainer
     */
    abstract public function getProductInfo($input);

    /**
     * Update product price
     *
     * @param \MobileAdmin\Containers\ActionData\UpdateProductPriceInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    abstract public function updateProductPrice($input);

    /**
     * Get reviews
     *
     * @param \MobileAdmin\Containers\ActionData\ReviewsInput $input Input data
     *
     * @return \MobileAdmin\Containers\ReviewContainer[]
     */
    abstract public function getReviews($input);

    /**
     * Delete review
     *
     * @param \MobileAdmin\Containers\ActionData\DeleteReviewInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    abstract public function deleteReview($input);

    /**
     * Get users list
     *
     * @param \MobileAdmin\Containers\ActionData\UsersInput $input Input Data
     *
     * @return \MobileAdmin\Containers\UsersListItemContainer[]
     */
    abstract public function getUsers($input);

    /**
     * Get user info
     *
     * @param \MobileAdmin\Containers\ActionData\UserInfoInput $input Input data
     *
     * @return \MobileAdmin\Containers\UserInfoContainer
     */
    abstract public function getUserInfo($input);

    /**
     * Set user status
     *
     * @param \MobileAdmin\Containers\ActionData\UserStatusInput $input Input data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    abstract public function setUserStatus($input);

    /**
     * Get orders list for user
     *
     * @param \MobileAdmin\Containers\ActionData\UserOrdersInput $input Input data
     *
     * @return \MobileAdmin\Containers\OrdersListItemContainer[]
     */
    abstract public function getUserOrdersList($input);

    /**
     * Generate discount coupon
     *
     * @param \MobileAdmin\Containers\ActionData\GenerateDiscountCouponInput $input Input data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    abstract public function generateDiscountCoupon($input);

    /**
     * Add notification to queue
     *
     * @param string $message Notification message
     *
     * @return boolean
     */
    abstract public function addNotificationToQueue($message);

    /**
     * Get notification from queue
     *
     * @return \MobileAdmin\Containers\PushNotifications\QueuedNotificationContainer
     */
    abstract public function popQueuedNotification();

    /**
     * Update notification status
     *
     * @param integer $id     Notification ID
     * @param string  $status Notification status
     *
     * @return void
     */
    abstract public function updateNotificationStatus($id, $status);
}
