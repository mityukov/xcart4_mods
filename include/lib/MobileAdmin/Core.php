<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

if (!defined('MOBILE_ADMIN_QUEUE_RUN_TYPE')) {
    define('MOBILE_ADMIN_QUEUE_RUN_TYPE', Core::PUSH_QUEUE_RUN_PHP);
}

if (!defined('MOBILE_ADMIN_DEBUG')) {
    define('MOBILE_ADMIN_DEBUG', 1);
}

/**
 * Mobile Admin core class
 */
abstract class Core extends MobileAdmin\Base
{
    const IPV4 = 4;
    const IPV6 = 6;

    const QUEUE_MANAGER_LOCK_TIMEOUT = 180;

    const IOS_NULL_VALUE = '(null)';

    const PUSH_QUEUE_RUN_AJAX   = 1;
    const PUSH_QUEUE_RUN_PHP    = 2;
    const PUSH_QUEUE_RUN_BOTH   = 3;

    /**
     * Mobile Admin config
     *
     * @var \MobileAdmin\DataHandler
     */
    protected $dataHandler = null;

    /**
     * Subscription info
     *
     * @var array
     */
    protected $subscriptionStatus = null;

    /**
     * Config path translation
     *
     * @var array
     */
    protected $configPathTranslation = array(
        'General:store_engine'  => 'getConfigStoreEngine',
        'General:store_version' => 'getConfigStoreVersion',
        'Order:statuses'        => 'getConfigOrderStatuses'
    );

    /**
     * Get store config value
     *
     * @param string $valuePath Value path as string separated by :
     *
     * @return mixed
     */
    abstract public function getConfigValue($valuePath);

    /**
     * Get HTTP host
     *
     * @return string
     */
    abstract public function getHttpHost();

    /**
     * Get HTTPS host
     *
     * @return string
     */
    abstract public function getHttpsHost();

    /**
     * Perform HTTP request
     *
     * @param string  $url      Request body
     * @param string  $body     Request URL
     * @param string  $method   Request method OPTIONAL
     * @param array   $headers  Request headers OPTIONAL
     * @param integer $timeout  Request timeout OPTIONAL
     * @param boolean $skipCert Skip certificate check OPTIONAL
     * @param array   $options  Additional connection options
     *
     * @return array
     */
    abstract public function httpRequest($url, $body, $method = 'GET', $headers = array(), $timeout = 60, $skipCert = false, $options = array());

    /**
     * Get the language variable value / perform the phrase translation
     *
     * @param string $name      Language variable name
     * @param array  $arguments Substitute array OPTIONAL
     *
     * @return string
     */
    abstract public function translate($name, array $arguments = array());

    /**
     * Add log file to the system log
     *
     * @param string $name Log name
     * @param string $data Log data
     *
     * @return void
     */
    abstract public function addLogFile($name, $data);

    /**
     * Get server time in Unix timestamp
     *
     * @return integer
     */
    abstract public function getTime();

    /**
     * Get time shift in seconds
     *
     * @return integer
     */
    abstract public function getTimeShift();

    /**
     * Get allowed order statuses
     *
     * @return array
     */
    abstract public function getAllowedOrderStatuses();

    /**
     * Get current store engine
     *
     * @return string
     */
    abstract protected function getConfigStoreEngine();

    /**
     * Get current store version
     *
     * @return string
     */
    abstract protected function getConfigStoreVersion();

    /**
     * Get list of order statuses used by the store
     *
     * @return array
     */
    abstract protected function getConfigOrderStatuses();

    /**
     * Initialize Mobile Admin config
     *
     * @return \MobileAdmin\Config
     */
    abstract protected function initConfig();

    /**
     * Initiate Mobile Admin data handler
     *
     * @return \MobileAdmin\DataHandler
     */
    abstract protected function initDataHandler();

    /**
     * Method to access a singleton
     *
     * @return static
     */
    public static function getInstance()
    {
        $core = parent::getInstance();

        if (is_null($core->getCore())) {
            $core->setCore(get_called_class());
        }

        return $core;
    }

    /**
     * Get push notification manager
     *
     * @return \MobileAdmin\PushNotification
     */
    public function getPushNotification()
    {
        return \MobileAdmin\PushNotification::getInstance();
    }

    /**
     * Get data handler
     *
     * @return \MobileAdmin\DataHandler
     */
    public function getDataHandler()
    {
        if (is_null($this->dataHandler)) {
            $this->dataHandler = $this->initDataHandler();
        }

        return $this->dataHandler;
    }

    /**
     * Handle Mobile Admin API requests
     *
     * @param string $action Action
     * @param array  $data   Request data
     *
     * @return void
     */
    public function handleRequests($action, $data)
    {
        \MobileAdmin\Controller::getInstance()->handleRequests($action, $data);
    }

    /**
     * Generate new random API key
     *
     * @return string
     */
    public function generateNewKey()
    {
        $chars  = MobileAdmin\Config::ALLOWED_KEY_SYMBOLS;

        $len    = 8;
        $key    = '';
        $i      = 0;

        while ($i < $len) {
            $newChar = substr($chars, rand(0, strlen($chars) -1), 1);

            if (!strstr($key, $newChar)) {
                $key .= $newChar;
                $i++;
            }
        }

        \MobileAdmin\Config::getInstance()->setData(array('apiKey' => $key));

        return $key;
    }

    /**
     * Get default API key
     *
     * @return string
     */
    public function getDefaultApiKey()
    {
        return MobileAdmin\Config::DEFAULT_API_KEY;
    }

    /**
     * Get API key
     *
     * @return string
     */
    public function getApiKey()
    {
        return MobileAdmin\Config::getInstance()->getApiKey();
    }

    /**
     * Get connector URL
     *
     * @param boolean $skipKey Skip api key part OPTIONAL
     *
     * @return string
     */
    public function getConnectorUrl($skipKey = false)
    {
        return MobileAdmin\Config::getInstance()->getConnectorUrl($skipKey);
    }

    /**
     * Get MobileAdmin application login from connector URL
     *
     * @return string
     */
    public function getMobileAdminLogin()
    {
        $url = parse_url($this->getConnectorUrl());

        $url['path'] = str_replace('/mobile_admin_api.php', '', $url['path']);

        $login = $url['host'];

        return $login;
    }

    /**
     * Register store on company servers
     *
     * @return void
     */
    public function registerStore()
    {
        $request = new MobileAdmin\Containers\RegisterStoreRequestContainer(
            array(
                'shopUrl'       => $this->getHttpHost(),
                'shopKey'       => $this->getApiKey(),
                'shopApiUrl'    => $this->getConnectorUrl(true)
            )
        );

        list($headers, $response) = $this->httpRequest(
            MobileAdmin\Config::APP_SERVER_LISTENER_URL . '/register',
            $request->toJson(),
            'POST',
            array(
                'Content-Type'  => 'application/json'
            ),
            MobileAdmin\Config::getInstance()->getConnectionTimeout(),
            true
        );

        MobileAdmin\Logger::getInstance()->log(
            array(
                MobileAdmin\Config::APP_SERVER_LISTENER_URL . '/register',
                $request->toJson(),
                $headers,
                $response
            )
        );
    }

    /**
     * Get subscription info
     *
     * @return array
     */
    public function getSubscriptionInfo()
    {
        if (is_null($this->subscriptionStatus)) {
            $shopUrl = $this->getHttpHost();

            $response = $this->httpRequest(
                MobileAdmin\Config::APP_SERVER_LISTENER_URL . '/shops/' . $shopUrl . '/checksubscription',
                '',
                'GET',
                array(
                    'Content-Type'  => 'application/json'
                ),
                MobileAdmin\Config::getInstance()->getConnectionTimeout(),
                true
            );

            if (!empty($response)) {
                $this->subscriptionStatus = json_decode($response[1], true);
            }
        }

        return $this->subscriptionStatus;
    }

    /**
     * Get library directory
     *
     * @return string
     */
    public function getLibDir()
    {
        return dirname(__FILE__);
    }

    /**
     * Output QR Code image
     *
     * @param $string
     *
     * @return void
     */
    public function getQrCodeFromString($string)
    {
        QrCode::getInstance()->getQrCodeFromString($string);
    }

    /**
     * Lock notification queue manager
     *
     * @return boolean
     */
    public function lockNotificationQueueManager()
    {
        $return = true;

        if (is_writable(dirname(MobileAdmin\Config::getInstance()->getNotificationManagerLockFile()))) {
            $file = fopen(MobileAdmin\Config::getInstance()->getNotificationManagerLockFile(), 'c+');

            if (flock($file, LOCK_SH)) {
                $status = fgets($file);
                $status = (int) $status;

                if (
                    empty($status)
                    || 0 === $status
                    || $status < ($this->getTime() - static::QUEUE_MANAGER_LOCK_TIMEOUT)
                ) {
                    $currentTime = (string) $this->getTime();

                    ftruncate($file, 0);
                    rewind($file);
                    fputs($file, $currentTime);
                    fflush($file);
                } else {
                    $return = false;
                }
            }

            flock($file, LOCK_UN);
            fclose($file);
        }

        return $return;
    }

    /**
     * Unlock notification queue manager
     *
     * @return void
     */
    public function unlockNotificationQueueManager()
    {
        if (is_writable(dirname(MobileAdmin\Config::getInstance()->getNotificationManagerLockFile()))) {
            $file = fopen(MobileAdmin\Config::getInstance()->getNotificationManagerLockFile(), 'w');

            ftruncate($file, 0);
            rewind($file);
            fputs($file, '0');
            fclose($file);
        }
    }

    /**
     * Call notification manager
     *
     * @return boolean
     */
    public function callNotificationManager()
    {
        if ($this->isPHPQueryCallAllowed()) {
            try {
                $return = $this->touchUrl($this->getNotificationManagerUrl());
            } catch (MobileAdmin\Exception $e) {
                $return = false;
            }
        } else {
            $return = false;
        }

        return $return;
    }

    /**
     * Get notification manager URL
     *
     * @return string
     */
    public function getNotificationManagerUrl()
    {
        $url = parse_url($this->getConnectorUrl());

        $query = explode('&', $url['query']);

        $query[] = 'request=run_push_queue_manager';

        $url['query'] = implode('&', $query);

        return $url['scheme'] . '://' . $url['host'] . $url['path'] . '?' . $url['query'];
    }

    /**
     * Touch URL
     *
     * @param string  $url  URL
     * @param string  $ip   IP OPTIONAL
     * @param integer $port Port OPTIONAL
     *
     * @return boolean
     *
     * @throws \MobileAdmin\Exception
     */
    public function touchUrl($url, $ip = '', $port = 80)
    {
        $parsedUrl = parse_url($url);

        if (empty($ip)) {
            $ip = gethostbyname($parsedUrl['host']);
        }

        if (function_exists('socket_create')) {
            $socketDomain = static::IPV4 == $this->getIpVersion($ip) ? AF_INET : AF_INET6;

            $headers = array(
                'GET ' . $parsedUrl['path']
                . (empty($parsedUrl['query']) ? '' : '?' . $parsedUrl['query']) . ' HTTP/1.1' . "\n",
                'Host: ' . $parsedUrl['host'] . "\n",
                'Accept: text/html' . "\n",
                'Connection: close' . "\n",
                "\n"
            );

            try {
                $sock = socket_create($socketDomain, SOCK_STREAM, SOL_TCP);

                if (socket_connect($sock, $ip, $port)) {
                    foreach ($headers as $header) {
                        socket_write($sock, $header);
                    }
                }

                $return = true;
                socket_close($sock);
            } catch (\Exception $e) {
                if (isset($sock)) {
                    socket_close($sock);
                }

                throw new MobileAdmin\Exception(
                    MobileAdmin\Exception::NET_SOCKET_ERROR,
                    array(
                        'error' => socket_last_error()
                    )
                );
            }

        } else {
            $headers = array(
                'Connection'        => 'close',
            );

            $options = array(
                CURLOPT_TIMEOUT_MS      => 100,
            );

            $url = $parsedUrl['scheme'] . '://' . (!empty($ip) ? $ip : $parsedUrl['host']) . $parsedUrl['path'] . '?' . $parsedUrl['query'];

            $this->httpRequest($url, '', 'GET', $headers, 1, true, $options);

            $return = true;
        }

        return $return;
    }

    /**
     * Check if order status is allowed
     *
     * @param string $statusField Status field
     * @param string $status      Status
     *
     * @return boolean
     */
    public function isAllowedOrderStatus($statusField, $status)
    {
        $return = false;

        $allowedStatuses = $this->getAllowedOrderStatuses();

        if (
            isset($allowedStatuses[$statusField])
            && !empty($allowedStatuses[$statusField])
            && is_array($allowedStatuses[$statusField])
        ) {
            foreach ($allowedStatuses[$statusField] as $statusInfo) {
                if ($status == $statusInfo['code']) {
                    $return = true;
                    break;
                }
            }
        }

        return $return;
    }

    /**
     * Check if AJAX call is allowed
     *
     * @return boolean
     */
    public function isAjaxQueryCallAllowed()
    {
        return MOBILE_ADMIN_QUEUE_RUN_TYPE == static::PUSH_QUEUE_RUN_AJAX
            || MOBILE_ADMIN_QUEUE_RUN_TYPE == static::PUSH_QUEUE_RUN_BOTH;
    }

    /**
     * Check if PHP query call allowed
     *
     * @return boolean
     */
    public function isPHPQueryCallAllowed()
    {
        return MOBILE_ADMIN_QUEUE_RUN_TYPE == static::PUSH_QUEUE_RUN_PHP
        || MOBILE_ADMIN_QUEUE_RUN_TYPE == static::PUSH_QUEUE_RUN_BOTH;
    }

    /**
     * Mobile Admin Core class constructor
     */
    protected function __construct()
    {
        $this->initConfig();

        $this->initDataHandler();
    }

    /**
     * Get IP protocol version
     *
     * @param string $ip IP address
     *
     * @return integer
     */
    protected function getIpVersion($ip)
    {
        return (false === strpos(':', $ip)) ? static::IPV4 : static::IPV6;
    }
}
