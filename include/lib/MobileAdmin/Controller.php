<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin;

use MobileAdmin;

/**
 * Mobile Admin core class
 */
class Controller extends MobileAdmin\Base
{
    /**
     * Status codes
     */
    const STATUS_ERROR    = 'E';
    const STATUS_SUCCESS  = 'O';

    /**
     * Handle Mobile Admin API requests
     *
     * @param string $action Action
     * @param array  $data   Request data
     *
     * @return void
     *
     * @throws \MobileAdmin\Exception
     */
    public function handleRequests($action, $data)
    {
        $data = $this->preprocessData($action, $data);

        MobileAdmin\Logger::getInstance()->logAccess(
            array_merge(
                array('ip' => $_SERVER['REMOTE_ADDR']),
                $data
            )
        );

        if (!isset($data['key'])) {
            $data['key'] = '';
        }

        try {
            $this->checkAccess($data['key']);

            if (!empty($action)) {
                $action = 'action_' . $action;
                $doAction = 'do' . str_replace(' ', '', ucwords(str_replace('_',' ', $action)));

                if (method_exists(__CLASS__, $doAction)) {
                    /**
                     * Call action by action name
                     */
                    $this->sendResponse($this->{$doAction}($data));

                } else {
                    throw new MobileAdmin\Exception(
                        MobileAdmin\Exception::ACTION_NOT_FOUND,
                        array(
                            'action' => $action
                        )
                    );
                }
            }
        } catch (MobileAdmin\Exception $e) {
            $errorData = $e->getErrorResponse();

            $this->sendResponse($errorData);
        }
    }

    /**
     * Check access
     *
     * @param string $key Key
     *
     * @return void
     *
     * @throws \MobileAdmin\Exception
     */
    protected function checkAccess($key)
    {
        if ($key != $this->getCore()->getApiKey()) {
            throw new MobileAdmin\Exception(
                MobileAdmin\Exception::WRONG_API_KEY,
                array(
                    'key' => $key
                )
            );
        }
    }

    /**
     * Output API response
     *
     * @param \MobileAdmin\Containers\AContainer $response API response
     *
     * @return void
     */
    protected function sendResponse($response)
    {
        echo ($response->toJson());
    }

    /**
     * Register device
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     *
     * @throws \MobileAdmin\Exception
     */
    protected function registerDevice($data)
    {
        /**
         * TODO: change the incoming request format
         * Workaround for old API request format
         */
        if (isset($data['android_version'])) {
            $data['os_version'] = $data['android_version'];
        }

        $data = new MobileAdmin\Containers\ActionData\RegisterDeviceInput($data);

        $registeredDevice = $this->getCore()->getDataHandler()->getDevice($data->regid);

        if (!is_null($registeredDevice)) {
            throw new MobileAdmin\Exception(\MobileAdmin\Exception::DEVICE_ALREADY_REGISTERED);
        }

        $deviceRegistered = $this->getCore()->getDataHandler()->registerDevice(
            new MobileAdmin\Containers\DeviceDataContainer($data)
        );

        if (!$deviceRegistered) {
            throw new MobileAdmin\Exception(\MobileAdmin\Exception::DEVICE_REG_ERROR);
        } else {
            MobileAdmin\PushNotification::getInstance()->sendNewDeviceRegisteredNotification($data->manufacturer);
        }

        return new MobileAdmin\Containers\APIResponseContainer(
            array(
                'status'    => self::STATUS_SUCCESS,
                'message'   => MobileAdmin\Message::getInstance()->translate(MobileAdmin\Message::DEVICE_REGISTERED),
                'device'    => $data,
                'data'      => '',
                'answer'    => ''
            )
        );
    }

    /**
     * Do action 'register_gcm'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    protected function doActionRegisterGcm($data)
    {
        $data['os_type'] = NotificationCervices\GCM::getInstance()->getOsType();

        return $this->registerDevice($data);
    }

    /**
     * Do action 'unregister_gcm'
     *
     * @param array $data Device data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     *
     * @throws Exception
     */
    protected function doActionUnregisterGcm($data)
    {
        $data = new MobileAdmin\Containers\ActionData\UnregisterDeviceInput($data);

        $deviceData = $this->getCore()->getDataHandler()->getDevice($data->regid);

        if (is_null($deviceData)) {
            throw new MobileAdmin\Exception(MobileAdmin\Exception::DEVICE_NOT_FOUND);
        }

        $this->getCore()->getDataHandler()->deleteDevice($deviceData->regid);

        return new MobileAdmin\Containers\APIResponseContainer(
            array(
                'status'    => self::STATUS_ERROR,
                'message'   => MobileAdmin\Message::getInstance()->translate(MobileAdmin\Message::DEVICE_UNREGISTERED),
                'device'    => $deviceData,
            )
        );
    }

    /**
     * Do action 'register_gcm'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    protected function doActionRegisterApns($data)
    {
        $data['os_type'] = NotificationCervices\APNS::getInstance()->getOsType();

        return $this->registerDevice($data);
    }

    /**
     * Do action 'register_gcm'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    protected function doActionUnregisterApns($data)
    {
        $data['os_type'] = NotificationCervices\APNS::getInstance()->getOsType();

        return $this->doActionUnregisterGcm($data);
    }

    /**
     * Do action 'get_config'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\ConfigurationValuesContainer
     */
    protected function doActionGetConfig($data)
    {
        $configPaths = unserialize(stripcslashes(urldecode($data['cfgs_array'])));

        if (empty($configPaths)) {
            $configPaths = array(
                'General:currency_symbol',
                'General:currency_format',
                'General:store_engine',
                'General:store_version',
                'Order:statuses',
            );
        }

        $return = array();

        foreach ($configPaths as $path) {
            $return[$path] = $this->getCore()->getConfigValue($path);
        }

        return new Containers\ConfigurationValuesContainer($return);
    }

    /**
     * Do action 'dashboard'
     *
     * @return \MobileAdmin\Containers\DashboardDataContainer
     */
    protected function doActionDashboard()
    {
        $dashboardData = $this->getCore()->getDataHandler()->getDashboardData();

        return $dashboardData;
    }

    /**
     * Do action 'last_orders'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\ArrayContainer
     */
    protected function doActionLastOrders($data)
    {
        $lastOrders = $this->getCore()->getDataHandler()->getLastOrdersList(
            new \MobileAdmin\Containers\ActionData\LastOrdersInput($data)
        );

        return new Containers\ArrayContainer($lastOrders);
    }

    /**
     * Do action 'order_info'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\OrderDataContainer
     */
    protected function doActionOrderInfo($data)
    {
        return $this->getCore()->getDataHandler()->getOrderInfo(
            new \MobileAdmin\Containers\ActionData\OrderInfoInput($data)
        );
    }

    /**
     * Do action 'change_tracking'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    protected function doActionChangeTracking($data)
    {
        return $this->getCore()->getDataHandler()->setTrackingNumbers(
            new \MobileAdmin\Containers\ActionData\ChangeTrackingInput($data)
        );
    }

    /**
     * Do action 'change_status'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    protected function doActionChangeStatus($data)
    {
        $input = new \MobileAdmin\Containers\ActionData\ChangeStatusInput($data);

        return $this->getCore()->getDataHandler()->setOrderStatus($input);
    }

    /**
     * Do action 'change_available'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    protected function doActionChangeAvailable($data)
    {
        return $this->getCore()->getDataHandler()->setProductAvailability(
            new \MobileAdmin\Containers\ActionData\ChangeAvailableInput($data)
        );
    }

    /**
     * Do action 'products'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\ArrayContainer
     */
    protected function doActionProducts($data)
    {
        $products = $this->getCore()->getDataHandler()->getProductsList(
            new \MobileAdmin\Containers\ActionData\ProductsInput($data)
        );

        return new Containers\ArrayContainer($products);
    }

    /**
     * Do action 'products'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\ArrayContainer
     */
    protected function doActionProductInfo($data)
    {
        return $this->getCore()->getDataHandler()->getProductInfo(
            new \MobileAdmin\Containers\ActionData\ProductInfoInput($data)
        );
    }

    /**
     * Do action 'update_product_price'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    protected function doActionUpdateProductPrice($data)
    {
        return $this->getCore()->getDataHandler()->updateProductPrice(
            new Containers\ActionData\UpdateProductPriceInput($data)
        );
    }

    /**
     * Do action 'update_product_price'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\ArrayContainer
     */
    protected function doActionReviews($data)
    {
        $return = $this->getCore()->getDataHandler()->getReviews(
            new \MobileAdmin\Containers\ActionData\ReviewsInput($data)
        );

        return new \MobileAdmin\Containers\ArrayContainer($return);
    }

    /**
     * Do action 'delete_review'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    protected function doActionDeleteReview($data)
    {
        return $this->getCore()->getDataHandler()->deleteReview(
            new \MobileAdmin\Containers\ActionData\DeleteReviewInput($data)
        );
    }

    /**
     * Do action 'users'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\ArrayContainer
     */
    protected function doActionUsers($data)
    {
        $return = $this->getCore()->getDataHandler()->getUsers(
            new \MobileAdmin\Containers\ActionData\UsersInput($data)
        );

        return new \MobileAdmin\Containers\ArrayContainer($return);
    }

    /**
     * Do action 'user_info'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\UserInfoContainer
     */
    protected function doActionUserInfo($data)
    {
        return $this->getCore()->getDataHandler()->getUserInfo(
            new \MobileAdmin\Containers\ActionData\UserInfoInput($data)
        );
    }

    /**
     * Do action 'user_info'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    protected function doActionUserStatus($data)
    {
        return $this->getCore()->getDataHandler()->setUserStatus(
            new \MobileAdmin\Containers\ActionData\UserStatusInput($data)
        );
    }

    /**
     * Do action 'user_orders'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\ArrayContainer
     */
    protected function doActionUserOrders($data)
    {
        $return = $this->getCore()->getDataHandler()->getUserOrdersList(
            new \MobileAdmin\Containers\ActionData\UserOrdersInput($data)
        );

        return new Containers\ArrayContainer($return);
    }

    /**
     * Do action 'generate_discount_coupon'
     *
     * @param array $data Data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    protected function doActionGenerateDiscountCoupon($data)
    {
        return $this->getCore()->getDataHandler()->generateDiscountCoupon(
            new \MobileAdmin\Containers\ActionData\GenerateDiscountCouponInput($data)
        );
    }

    /**
     * Do action 'run_push_queue_manager'
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    protected function doActionRunPushQueueManager()
    {
        $this->getCore()->getPushNotification()->executeQueueManager();

        return new Containers\APIResponseContainer(
            array(
                'status'    => self::STATUS_SUCCESS,
                'message'   => MobileAdmin\Message::getInstance()->translate(Message::SUCCESS),
                'data'      => '',
            )
        );
    }

    /**
     * Test push notifications
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     */
    protected function doActionTestPush()
    {
        $response = $this->getCore()->getPushNotification()->sendLowStockNotification(1);

        $this->getCore()->callNotificationManager();

        return new Containers\APIResponseContainer(
            array(
                'status'    => self::STATUS_SUCCESS,
                'message'   => MobileAdmin\Message::getInstance()->translate(Message::TEST),
                'data'      => $response,
            )
        );
    }

    /**
     * Preprocess input data
     *
     * @param string $action Action
     * @param array  $data   Request data
     *
     * @return array
     */
    protected function preprocessData($action, $data)
    {
        switch ($action) {
            case 'change_status':
                $data = $this->preprocessChangeStatusData($data);
                break;
        }

        return $data;
    }

    /**
     * Preprocess
     *
     * @param $data
     *
     * @return array
     */
    protected function preprocessChangeStatusData($data)
    {
        if (
            isset($data['pph_order_details'])
            && !empty($data['pph_order_details'])
            && !is_null($data['pph_order_details'])
            && '(null)' != $data['pph_order_details']
        ) {
            $_tmp = json_decode(stripslashes($data['pph_order_details']), true);

            if (is_array($_tmp)) {
                $data['pph_order_details'] = $_tmp;
            }

            $fields = array(
                'InvoiceId',
                'Number',
                'TxId',
                'Type',
                'oid',
                'Tip',
                'Email'
            );

            if (
                !is_array($data['pph_order_details'])
                && strpos($data['pph_order_details'], 'Number') !== false
                && strpos($data['pph_order_details'], '=') !== false
            ) {
                $_tmp = explode('=', $data['pph_order_details']);

                $data['Number'] = $_tmp[1];
            }

            $pphOrderDetails = array();

            foreach ($fields as $field) {
                $lField = strtolower($field);

                if (isset($data[$field])) {
                    $pphOrderDetails[strtolower($field)] = $data[$field];
                } elseif (isset($data['pph_order_details'][$field])) {
                    $pphOrderDetails[strtolower($field)] = $data['pph_order_details'][$field];
                } elseif (isset($data[$lField])) {
                    $pphOrderDetails[$lField] = $data[$lField];
                } elseif (isset($data['pph_order_details'][$lField])) {
                    $pphOrderDetails[$lField] = $data['pph_order_details'][$lField];
                }
            }

            $data['pph_order_details'] = $pphOrderDetails;

            if (
                isset($data['order_id'])
                && empty($data['order_id'])
            ) {
                if (isset($data['Number'])) {
                    $data['order_id'] = $data['Number'];
                } elseif (isset($data['number'])) {
                    $data['order_id'] = $data['number'];
                } elseif (isset($data['NUMBER'])) {
                    $data['order_id'] = $data['NUMBER'];
                } elseif (isset($data['pph_order_details']['number'])) {
                    $data['order_id'] = $data['pph_order_details']['number'];
                }
            }
        }

        return $data;
    }
}