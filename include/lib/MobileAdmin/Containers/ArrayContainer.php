<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

use MobileAdmin\Containers;

/**
 * Array container
 */
class ArrayContainer extends Containers\AContainer
{
    /**
     * Data
     *
     * @var \MobileAdmin\Containers\AContainer[]
     */
    public $data = array();

    /**
     * Populate container with data
     *
     * @param null|mixed $data Data OPTIONAL
     *
     * @return void
     */
    public function __construct($data = null)
    {
        if (
            !is_null($data)
            && !empty($data)
        ) {
            $this->data = $data;
        }
    }

    /**
     * Get content as array
     *
     * @return array
     */
    public function toArray()
    {
        $return = array();

        if (!empty($this->data)) {
            foreach ($this->data as $i => $elem) {
                $return[$i] = $elem->toArray();
            }
        }

        return $return;
    }

    /**
     * Check if container is empty
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return empty($this->data);
    }
}
