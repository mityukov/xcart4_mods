<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

use MobileAdmin\Containers;

/**
 * Dashboard data container
 */
class DashboardDataContainer extends Containers\AContainer
{
    /**
     * Today sales
     *
     * @var string
     */
    public $today_sales = '';

    /**
     * Number of low stock products
     *
     * @var string
     */
    public $low_stock = '';

    /**
     * Today visitors
     *
     * @var string
     */
    public $today_visitors = '';

    /**
     * Amount sold today
     *
     * @var string
     */
    public $today_sold = '';

    /**
     * Number of reviews posted today
     *
     * @var string
     */
    public $reviews_today = '';

    /**
     * First 3 of today's orders
     *
     * @var array
     */
    public $today_orders = array();

    /**
     * Number of today orders
     *
     * @var string
     */
    public $today_orders_count = '';

    /**
     * Technical info required for fields validation
     *
     * @var array
     */
    protected $_fieldsValidationData = array(
        'today_sales'           => 'price',
        'low_stock'             => 'string',
        'today_visitors'        => 'string',
        'today_sold'            => 'string',
        'reviews_today'         => 'string',
        'today_orders'          => 'array',
        'today_orders_count'    => 'string'
    );
}
