<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

use MobileAdmin\Containers;

/**
 * Order data container
 */
class OrderDataContainer extends Containers\AContainer
{
    /**
     * @var string
     */
    public $orderid = '0';

    /**
     * @var string
     */
    public $status = '';

    /**
     * Payment status
     *
     * @var string
     */
    public $payment_status = '';

    /**
     * Shipping status
     *
     * @var string
     */
    public $fulfilment_status = '';

    /**
     * @var string
     */
    public $tracking = '';

    /**
     * @var string
     */
    public $payment_method = '';

    /**
     * @var string
     */
    public $shipping = '';

    /**
     * @var string
     */
    public $date = '0';

    /**
     * @var string
     */
    public $userid = '0';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $firstname = '';

    /**
     * @var string
     */
    public $lastname = '';

    /**
     * @var string
     */
    public $b_title = '';

    /**
     * @var string
     */
    public $b_firstname = '';

    /**
     * @var string
     */
    public $b_lastname = '';

    /**
     * @var string
     */
    public $b_address = '';

    /**
     * @var string
     */
    public $b_city = '';

    /**
     * @var string
     */
    public $b_county = '';

    /**
     * @var string
     */
    public $b_state = '';

    /**
     * @var string
     */
    public $b_country = '';

    /**
     * @var string
     */
    public $b_zipcode = '';

    /**
     * @var string
     */
    public $b_zip4 = '';

    /**
     * @var string
     */
    public $b_phone = '';

    /**
     * @var string
     */
    public $b_fax = '';

    /**
     * @var string
     */
    public $b_address_2 = '';

    /**
     * @var string
     */
    public $b_statename = '';

    /**
     * @var string
     */
    public $b_countryname = '';

    /**
     * @var string
     */
    public $s_title = '';

    /**
     * @var string
     */
    public $s_firstname = '';

    /**
     * @var string
     */
    public $s_lastname = '';

    /**
     * @var string
     */
    public $s_address = '';

    /**
     * @var string
     */
    public $s_city = '';

    /**
     * @var string
     */
    public $s_county = '';

    /**
     * @var string
     */
    public $s_state = '';

    /**
     * @var string
     */
    public $s_country = '';

    /**
     * @var string
     */
    public $s_zipcode = '';

    /**
     * @var string
     */
    public $s_zip4 = '';

    /**
     * @var string
     */
    public $s_phone = '';

    /**
     * @var string
     */
    public $s_fax = '';

    /**
     * @var string
     */
    public $s_address_2 = '';

    /**
     * @var string
     */
    public $s_statename = '';

    /**
     * @var string
     */
    public $s_countryname = '';

    /**
     * Order details
     *
     * @var \MobileAdmin\Containers\OrderItemContainer[]
     */
    public $details = array();

    /**
     * @var string
     */
    public $customer_notes = '';

    /**
     * @var string
     */
    public $subtotal = '0.00';

    /**
     * @var string
     */
    public $giftcert_discount = '0.000';

    /**
     * @var string
     */
    public $discount = '0.00';

    /**
     * @var string
     */
    public $coupon_discount = '0.00';

    /**
     * @var string
     */
    public $shipping_cost = '0.00';

    /**
     * @var string
     */
    public $payment_surcharge = '0.00';

    /**
     * @var string
     */
    public $total = '0.00';

    /**
     * @var string
     */
    public $pph_url = '';

    /**
     * Technical info required for fields validation
     *
     * @var array
     */
    protected $_fieldsValidationData = array(
        'orderid'           => 'string',
        'status'            => 'string',
        'payment_status'    => 'string',
        'fulfilment_status' => 'string',
        'tracking'          => 'string',
        'payment_method'    => 'string',
        'shipping'          => 'string',
        'date'              => 'integer',
        'userid'            => 'string',
        'title'             => 'string',
        'firstname'         => 'string',
        'lastname'          => 'string',
        'b_title'           => 'string',
        'b_firstname'       => 'string',
        'b_lastname'        => 'string',
        'b_address'         => 'string',
        'b_city'            => 'string',
        'b_county'          => 'string',
        'b_state'           => 'string',
        'b_country'         => 'string',
        'b_zipcode'         => 'string',
        'b_zip4'            => 'string',
        'b_phone'           => 'string',
        'b_fax'             => 'string',
        'b_address_2'       => 'string',
        'b_statename'       => 'string',
        'b_countryname'     => 'string',
        's_title'           => 'string',
        's_firstname'       => 'string',
        's_lastname'        => 'string',
        's_address'         => 'string',
        's_city'            => 'string',
        's_county'          => 'string',
        's_state'           => 'string',
        's_country'         => 'string',
        's_zipcode'         => 'string',
        's_zip4'            => 'string',
        's_phone'           => 'string',
        's_fax'             => 'string',
        's_address_2'       => 'string',
        's_statename'       => 'string',
        'details'           => 'array',
        'customer_notes'    => 'string',
        'subtotal'          => 'price',
        'giftcert_discount' => 'price',
        'discount'          => 'price',
        'coupon_discount'   => 'price',
        'shipping_cost'     => 'price',
        'payment_surcharge' => 'price',
        'total'             => 'price',
        'pph_url'           => 'string'
    );

    /**
     * Get content as array
     *
     * @return array
     */
    public function toArray()
    {
        $return = parent::toArray();

        if (empty($return['pph_url'])) {
            unset($return['pph_url']);
        }

        return $return;
    }
}
