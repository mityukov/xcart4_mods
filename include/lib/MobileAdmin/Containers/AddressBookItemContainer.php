<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

use MobileAdmin\Containers;

/**
 * Users list item container
 */
class AddressBookItemContainer extends Containers\AContainer
{
    /**
     * @var string
     */
    public $id = '0';

    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $firstname = '';

    /**
     * @var string
     */
    public $lastname = '';

    /**
     * @var string
     */
    public $address = '';

    /**
     * @var string
     */
    public $city = '';

    /**
     * @var string
     */
    public $county = '';

    /**
     * @var string
     */
    public $state = '';

    /**
     * @var string
     */
    public $country = '';

    /**
     * @var string
     */
    public $zipcode = '';

    /**
     * @var string
     */
    public $zip4 = '';

    /**
     * @var string
     */
    public $phone = '';

    /**
     * @var string
     */
    public $fax = '';

    /**
     * @var string
     */
    public $statename = '';

    /**
     * @var string
     */
    public $countryname = '';

    /**
     * Technical info required for fields validation
     *
     * @var array
     */
    protected $_fieldsValidationData = array(
        'id'            => 'string',
        'title'         => 'string',
        'firstname'     => 'string',
        'lastname'      => 'string',
        'address'       => 'string',
        'city'          => 'string',
        'county'        => 'string',
        'state'         => 'string',
        'country'       => 'string',
        'zipcode'       => 'string',
        'zip4'          => 'string',
        'phone'         => 'string',
        'fax'           => 'string',
        'statename'     => 'string',
        'countryname'   => 'string'
    );
}
