<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

use MobileAdmin\Containers;

/**
 * GCM device registration fields
 */
class DeviceDataContainer extends Containers\AContainer
{
    /**
     * Device ID in the storage
     *
     * @var integer
     */
    public $id = 0;

    /**
     * registration ID
     *
     * @var string
     */
    public $regid = '0';

    /**
     * Manufacturer
     *
     * @var string
     */
    public $manufacturer = 'Unknown';

    /**
     * Model
     *
     * @var string
     */
    public $model = 'Unknown';

    /**
     * Serial numer
     *
     * @var string
     */
    public $serial = 'Unknown';

    /**
     * OS Type
     *
     * @var string
     */
    public $os_type = '';

    /**
     * OS Version
     *
     * @var string
     */
    public $os_version = 'Unknown';

    /**
     * IMEI
     *
     * @var string
     */
    public $imei = 'Unknown';

    /**
     * IMSI
     *
     * @var string
     */
    public $imsi = 'Unknown';

    /**
     * Status
     *
     * @var string
     */
    public $status = 'Y';

    /**
     * @var string
     */
    public $date = '';

    /**
     * Populate container with data
     *
     * @param null|mixed $data Data OPTIONAL
     *
     * @return void
     */
    public function __construct($data = null)
    {
        parent::__construct($data);

        if (empty($this->os_type)) {
            $this->os_type = \MobileAdmin\NotificationCervices\GCM::getInstance()->getOsType();
        }
    }
}
