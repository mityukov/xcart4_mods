<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

use MobileAdmin\Containers;

/**
 * Product variant container
 */
class ProductVariantContainer extends Containers\AContainer
{
    /**
     * @var integer
     */
    public $productid = '0';

    /**
     * @var integer
     */
    public $variant_id = '0';

    /**
     * @var integer
     */
    public $avail = '0';

    /**
     * @var float
     */
    public $weight = '0.00';

    /**
     * @var string
     */
    public $productcode = '';

    /**
     * @var string
     */
    public $def = '';

    /**
     * @var float
     */
    public $price = '0.00';

    /**
     * @var string
     */
    public $image_path_W = '';

    /**
     * @var string
     */
    public $image_W_x = '';

    /**
     * @var string
     */
    public $image_W_y = '';

    /**
     * @var \MobileAdmin\Containers\ArrayContainer
     */
    public $options = array();

    /**
     * @var \MobileAdmin\Containers\ArrayContainer
     */
    public $options_arr = array();

    /**
     * Technical info required for fields validation
     *
     * @var array
     */
    protected $_fieldsValidationData = array(
        'productid'     => 'string',
        'variant_id'    => 'string',
        'avail'         => 'string',
        'weight'        => 'float',
        'productcode'   => 'string',
        'def'           => 'string',
        'price'         => 'price',
        'image_path_W'  => 'string',
        'image_W_x'     => 'string',
        'image_W_y'     => 'string',
        'options'       => 'array',
        'options_arr'   => 'array'
    );

    /**
     * Populate container with data
     *
     * @param null|mixed $data Data OPTIONAL
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->options = new \MobileAdmin\Containers\ArrayContainer(array());
        $this->options_arr = new \MobileAdmin\Containers\ArrayContainer(array());

        parent::__construct($data);
    }
}
