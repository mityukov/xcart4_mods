<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers\ActionData;

use MobileAdmin\Containers;
use XLite\Logger;

/**
 * Mobile Admin API response
 */
class ChangeStatusInput extends Containers\AContainer
{
    const ORDER_STATUS_FILED            = 'status';
    const ORDER_PAYMENT_STATUS_FIELD    = 'payment_status';
    const ORDER_FULFILMENT_STATUS_FIELD = 'fulfilment_status';

    /**
     * Order id
     *
     * @var integer
     */
    public $order_id = 0;

    /**
     * Order status
     *
     * @var string
     */
    public $status = '';

    /**
     * Order payment status
     *
     * @var string
     */
    public $payment_status = '';

    /**
     * Order fulfilment status
     *
     * @var string
     */
    public $fulfilment_status = '';

    /**
     * @var array
     */
    public $pph_order_details = array();

    /**
     * Status fields to validate
     *
     * @var array
     */
    protected $_fieldsValidationData = array(
        'status',
        'payment_status',
        'fulfilment_status'
    );

    /**
     * Add value to container
     *
     * @param string $name  Property name
     * @param mixed  $value Property value
     *
     * @return void
     */
    protected function addValue($name, $value)
    {
        if ('pph_order_details' == $name) {
            if (!is_array($value)) {
                $value = json_decode(stripslashes($value), true);
            }

            if (
                is_array($value)
                && !empty($value)
            ) {
                $data = array();

                foreach ($value as $n => $v) {
                    $data[strtolower($n)] = $v;
                }

                $value = $data;
            }
        }

        parent::addValue($name, $value);
    }

    /**
     * Validate container content
     *
     * @param array $allowedStatuses Allowed order statuses
     *
     * @return boolean
     *
     * @throws \MobileAdmin\Exception
     */
/*
    public function validate($allowedStatuses)
    {
        foreach ($this->_fieldsValidationData as $field) {
            if (
                !empty($this->{$field})
                && !in_array($this->{$field}, $allowedStatuses[$field])
            ) {
                throw new \MobileAdmin\Exception(
                    \MobileAdmin\Exception::INVALID_ORDER_STATUS,
                    array(
                        'status'    => $field . ' : ' . $this->{$field}
                    )
                );
            }
        }

        return true;
    }
*/
}
