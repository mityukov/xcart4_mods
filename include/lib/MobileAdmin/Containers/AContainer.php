<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

/**
 * Mobile Admin core class
 */
abstract class AContainer
{
    /**
     * Technical info required for fields validation
     *
     * @var array
     */
    protected $_fieldsValidationData = array();

    /**
     * Populate container with data
     *
     * @param null|mixed $data Data OPTIONAL
     *
     * @return void
     */
    public function __construct($data = null)
    {
        if (
            !is_null($data)
            && !empty($data)
        ) {
            if (is_object($data)) {
                $properties = array_keys(get_class_vars(get_class($data)));
                if (!empty($properties)) {
                    foreach ($properties as $name) {
                        $this->addValue($name, $data->{$name});
                    }
                }
            } else {
                foreach ($data as $name => $value) {
                    $this->addValue($name, $value);
                }
            }
        }

        $this->validate();
    }

    /**
     * Get content as array
     *
     * @return array
     */
    public function toArray()
    {
        $return = array();

        $properties = $this->getDataFields();

        foreach ($properties as $value) {
            if (
                is_object($this->{$value})
                && method_exists($this->{$value}, 'toArray')
            ) {
                $return[$value] = $this->{$value}->toArray();
            } else {
                $return[$value] = $this->{$value};

                if (isset($this->_fieldsValidationData[$value])) {
                    $return[$value] = $this->validateValueByType(
                        $return[$value],
                        $this->_fieldsValidationData[$value]
                    );
                }
            }
        }

        return $return;
    }

    /**
     * Check if container is empty
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return false;
    }

    /**
     * Get content as JSON
     *
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Validate container content
     *
     * @return boolean
     */
    public function validate()
    {
        return true;
    }

    /**
     * Add value to container
     *
     * @param string $name  Property name
     * @param mixed  $value Property value
     *
     * @return void
     */
    protected function addValue($name, $value)
    {
        if (
            '_fieldsValidationData' != $name
            && property_exists($this, $name)
        ) {
            if (\MobileAdmin\Core::IOS_NULL_VALUE != $value) {
                $this->{$name} = $value;
            } else {
                $this->{$name} = null;
            }
        }
    }

    /**
     * Get container data fields list
     *
     * @return array
     */
    protected function getDataFields()
    {
        $return = get_class_vars(get_class($this));

        unset($return['_fieldsValidationData']);

        return array_keys($return);
    }

    /**
     * Validate value by type
     *
     * @param mixed  $value Value
     * @param string $type  Type
     *
     * @return array
     */
    protected function validateValueByType($value, $type)
    {
        switch ($type) {
            case 'array':
                $return = $this->validateArray($value);
                break;
            case 'string':
                $return = $this->validateString($value);
                break;
            case 'integer':
                $return = $this->validateInteger($value);
                break;
            case 'price':
                $return = $this->validatePrice($value);
                break;
            case 'float':
                $return = $this->validateFloat($value);
                break;
            default:
                $return = $value;
        }

        return $return;
    }

    /**
     * Validate array
     *
     * @param mixed $value Value
     *
     * @return array
     */
    protected function validateArray($value)
    {
        return (empty($value) || is_null($value) || !is_array($value)) ? array() : $value;
    }

    /**
     * Validate array
     *
     * @param mixed $value Value
     *
     * @return string
     */
    protected function validateString($value)
    {
        $value = is_array($value) ? '' : (string) $value;

        return $value;
    }

    /**
     * Validate array
     *
     * @param mixed $value Value
     *
     * @return integer
     */
    protected function validateInteger($value)
    {
        $value = is_array($value) ? 0 : $value;

        return (int) $value;
    }

    /**
     * Validate array
     *
     * @param mixed $value Value
     *
     * @return string
     */
    protected function validatePrice($value)
    {
        $value = is_array($value) ? 0 : $value;

        return sprintf("%.2f", round(((double) $value) + 0.00000000001, 2));
    }

    /**
     * Validate array
     *
     * @param mixed $value Value
     *
     * @return string
     */
    protected function validateFloat($value)
    {
        $value = is_array($value) ? 0 : $value;

        $value = sprintf("%.4f", round(((double) $value) + 0.00000000001, 2));

        $value = preg_replace('/[0]+$/', '', $value);

        return $value;
    }
}