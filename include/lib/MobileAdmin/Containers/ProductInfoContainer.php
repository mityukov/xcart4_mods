<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\Containers;

use MobileAdmin\Containers;

/**
 * Product info container
 */
class ProductInfoContainer extends Containers\AContainer
{
    /**
     * @var string
     */
    public $productid = '0';

    /**
     * @var string
     */
    public $productcode = '';

    /**
     * @var string
     */
    public $product = '';

    /**
     * @var string
     */
    public $producttitle = '';

    /**
     * @var string
     */
    public $descr = '';

    /**
     * @var string
     */
    public $fulldescr = '';

    /**
     * @var string
     */
    public $url = '';

    /**
     * @var string
     */
    public $image_url = '';

    /**
     * @var array
     */
    public $images = array();

    /**
     * @var string
     */
    public $weight = '0.00';

    /**
     * @var string
     */
    public $price = '0.00';

    /**
     * @var string
     */
    public $list_price = '0.00';

    /**
     * @var string
     */
    public $avail = '0';

    /**
     * @var \MobileAdmin\Containers\ArrayContainer
     */
    public $variants = array();

    /**
     * @var string
     */
    public $forsale = '';

    /**
     * @var integer
     */
    public $add_date = 0;

    /**
     * @var string
     */
    public $title_tag = '';

    /**
     * @var string
     */
    public $meta_keywords = '';

    /**
     * @var string
     */
    public $meta_description = '';

    /**
     * Technical info required for fields validation
     *
     * @var array
     */
    protected $_fieldsValidationData = array(
        'productid'         => 'string',
        'productcode'       => 'string',
        'product'           => 'string',
        'producttitle'      => 'string',
        'descr'             => 'string',
        'fulldescr'         => 'string',
        'url'               => 'string',
        'image_url'         => 'string',
        'images'            => 'array',
        'weight'            => 'float',
        'price'             => 'price',
        'list_price'        => 'price',
        'avail'             => 'string',
        'variants'          => 'array',
        'forsale'           => 'string',
        'add_date'          => 'integer',
        'title_tag'         => 'string',
        'meta_keywords'     => 'string',
        'meta_description'  => 'string'
    );

    /**
     * Populate container with data
     *
     * @param null|mixed $data Data OPTIONAL
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->variants = new \MobileAdmin\Containers\ArrayContainer(array());

        parent::__construct($data);
    }

    /**
     * Get content as array
     *
     * @return array
     */
    public function toArray()
    {
        $return = parent::toArray();

        if ($this->variants->isEmpty()) {
            $return['variants'] = null;
        }

        return $return;
    }
}
