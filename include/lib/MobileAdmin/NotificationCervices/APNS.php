<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\NotificationCervices;

use MobileAdmin;

/**
 * Mobile Admin GCM class
 */
class APNS extends MobileAdmin\NotificationCervices\PushNotificationService
{
    const APNS_CONNECTION_TIMEOUT   = 15;
    const APNS_CONNECTION_RETRY     = 2;

    const DEVICE_TYPE = 'I';

    /**
     * APNS PHP Class
     *
     * @var \ApnsPHP_Push
     */
    protected $apns = null;

    /**
     * Send push notification
     *
     * @param string|array                                 $regIds  Registration IDs
     * @param \MobileAdmin\Containers\PushMessageContainer $message Message
     *
     * @return string
     */
    public function sendNotification($regIds, $message)
    {
        $response = '';

        if (!is_array($regIds)) {
            $regIds = array($regIds);
        }

        if (!empty($regIds)) {
            $payload = '';

            try {
                $this->getAPNS()->connect();

                foreach ($regIds as $regId) {
                    $msg = new \ApnsPHP_Message($regId);

                    $msg->setText($message->message);
                    $msg->setCustomProperty('data', $message->toArray());
                    $msg->setBadge(3);
                    $msg->setSound();

                    $payload = $msg->getPayload();

                    $this->getAPNS()->add($msg);
                }

                $this->getAPNS()->send();
                $this->getAPNS()->disconnect();
            } catch (\ApnsPHP_Exception $e) {
                throw new \MobileAdmin\Exception(\MobileAdmin\Exception::COMMON_ERROR);
            }

            $response = $this->getAPNS()->getErrors();

            \MobileAdmin\Logger::getInstance()->log(
                array(
                    'REQUEST'   => $payload,
                    'RESPONSE'  => $response
                ),
                'APNS_request'
            );
        }

        return $response;
    }

    /**
     * Filter registered devices and return only those available for implemented service
     *
     * @param \MobileAdmin\Containers\DeviceDataContainer[] $devices Registered devices
     *
     * @return \MobileAdmin\Containers\DeviceDataContainer[]
     */
    public function filterRegisteredDevices($devices)
    {
        $return = array();

        foreach ($devices as $device) {
            if ($this->getOsType() == $device->os_type) {
                $return[] = $device->regid;
            }
        }

        return $return;
    }

    /**
     * Get OS type
     *
     * @return string
     */
    public function getOsType()
    {
        return 'I';
    }

    /**
     * Get OS name label
     *
     * @return string
     */
    public function getOsName()
    {
        return $this->getCore()->translate('lbl_mobad_ios');
    }

    /**
     * Get APNS PHP class
     *
     * @return \ApnsPHP_Push
     */
    protected function getAPNS()
    {
        if (is_null($this->apns)) {
            $DS = MobileAdmin\Config::getInstance()->getDirectorySeparator();

            include_once $this->getCore()->getLibDir() . $DS . 'lib' . $DS . 'ApnsPHP' . $DS .  'Autoload.php';

            $this->apns = new \ApnsPHP_Push(
                MobileAdmin\Config::getInstance()->getApnsEnvironment(),
                MobileAdmin\Config::getInstance()->getApnsProviderCertificate()
            );

            $this->apns->setProviderCertificatePassphrase(MobileAdmin\Config::APNS_PROVIDER_CERT_PASSWD);

            $this->apns->setRootCertificationAuthority(MobileAdmin\Config::getInstance()->getApnsRootCertificate());

            $this->apns->setConnectTimeout(static::APNS_CONNECTION_TIMEOUT);

            $this->apns->setConnectRetryTimes(static::APNS_CONNECTION_RETRY);
        }

        $this->apns->setLogger(MobileAdmin\NotificationCervices\APNSLogger::getInstance());

        return $this->apns;
    }
}
