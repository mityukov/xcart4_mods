<?php
// vim: set ts=4 sw=4 sts=4 et:

/**
 * X-Cart
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the software license agreement
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.x-cart.com/license-agreement.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to licensing@x-cart.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not modify this file if you wish to upgrade X-Cart to newer versions
 * in the future. If you wish to customize X-Cart for your needs please
 * refer to http://www.x-cart.com/ for more information.
 *
 * @category  X-Cart 5
 * @author    Qualiteam software Ltd <info@x-cart.com>
 * @copyright Copyright (c) 2011-2014 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license   http://www.x-cart.com/license-agreement.html X-Cart 5 License Agreement
 * @link      http://www.x-cart.com/
 */

namespace MobileAdmin\NotificationCervices;

use MobileAdmin;

/**
 * Mobile Admin GCM class
 */
class GCM extends MobileAdmin\NotificationCervices\PushNotificationService
{
    /**
     * Send push notification
     *
     * @param string|array                                 $regIds  Registration IDs
     * @param \MobileAdmin\Containers\PushMessageContainer $message Message
     *
     * @return string
     */
    public function sendNotification($regIds, $message)
    {
        $response = '';

        if (!is_array($regIds)) {
            $regIds = array($regIds);
        }

        if (!empty($regIds)) {
            $fields = new MobileAdmin\Containers\GCMPushNotificationContainer(
                array(
                    'registration_ids'  => $regIds,
                    'data'              => $message,
                )
            );

            list ($headers, $response) = $this->getCore()->httpRequest(
                MobileAdmin\Config::GCM_URL,
                $fields->toJson(),
                'POST',
                array(
                    'Authorization' => 'key=' . MobileAdmin\Config::GOOGLE_API_KEY,
                    'Content-Type'  => 'application/json'
                ),
                MobileAdmin\Config::getInstance()->getConnectionTimeout()
            );

            \MobileAdmin\Logger::getInstance()->log(
                array(
                    'REQUEST'   => $fields->toJson(),
                    'RESPONSE'  => array(
                        $headers,
                        $response
                    )
                ),
                'GCM_request'
            );
        }

        return $response;
    }

    /**
     * Filter registered devices and return only those available for implemented service
     *
     * @param \MobileAdmin\Containers\DeviceDataContainer[] $devices Registered devices
     *
     * @return array
     */
    public function filterRegisteredDevices($devices)
    {
        $return = array();

        foreach ($devices as $device) {
            if ($this->getOsType() == $device->os_type) {
                $return[] = $device->regid;
            }
        }

        return $return;
    }

    /**
     * Get OS type
     *
     * @return string
     */
    public function getOsType()
    {
        return 'A';
    }

    /**
     * Get OS name label
     *
     * @return string
     */
    public function getOsName()
    {
        return $this->getCore()->translate('lbl_mobad_android');
    }
}
