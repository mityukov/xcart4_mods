DELETE FROM xcart_modules WHERE module_name = 'Mobile_Admin';

INSERT INTO  `xcart_modules` (`moduleid` , `module_name` , `module_descr` , `active`) VALUES (NULL ,  'Mobile_Admin',  'Mobile Admin connector module',  'Y');

REPLACE INTO `xcart_config` (`name`, `comment`, `value`, `category`, `orderby`, `type`, `defvalue`, `variants`, `validation`) VALUES ('mobile_admin_api_key', 'API key', '', 'Mobile_Admin', '10', 'text', '', '', '');

CREATE TABLE IF NOT EXISTS xcart_gcm_users (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` varchar(128),
  `model` varchar(128),
  `serial` varchar(32),
  `os_type` char(1) NOT NULL DEFAULT 'A',
  `os_version` varchar(32),
  `imei` varchar(16),
  `imsi` varchar(16),
  `status` varchar(1) default 'N',
  `gcm_regid` varchar(255) UNIQUE,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY (`status`));

CREATE TABLE IF NOT EXISTS xcart_notification_queue (
  `id` int NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'Q',
  `date` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY(`id`),
  KEY hash (`hash`),
  KEY status (`status`));
