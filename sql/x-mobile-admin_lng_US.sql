REPLACE INTO `xcart_languages` SET `code`='en', `name`='module_name_Mobile_Admin', `value`='Mobile Admin', `topic`='Modules';

REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_discount_coupon_nomod', `value`='Discount Coupons module is disabled or not installed', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_discount_coupon_inv_code', `value`='Coupon code is invalid or empty. Allowed characters are alphanumeric characters (A-Z, a-z, 0-9), point (.), hyphen (-), and underscore (_)', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_discount_coupon_code_exists', `value`='Coupon code already exists in the store', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_discount_coupon_discount_error', `value`='Coupon discount cannot be empty for \'percent\' or \'absolute\' coupon type or the coupon discount cannot be more than 100%', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_device_registered', `value`='New user registered', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_device_registration_error', `value`='Error of registration of the user', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_device_unregistered', `value`='User unregistered', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_device_not_registered', `value`='No such user', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_review_not_found', `value`='Review not found', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_tracking_empty', `value`='Tracking number is empty', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_status_empty', `value`='Status is empty', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_customer_not_found', `value`='User not found', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_field_data_invalid', `value`='Request data in one of the fields is absent or invalid: {{field}}', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_product_not_found', `value`='Product not found', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_detailed_images_nomod', `value`='Detailed Product Images module is disabled or not installed', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_product_options_nomod', `value`='Product Options module is disabled or not installed', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_reviews_by_date_na', `value`='N/A', `topic`='Errors';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_err_mobad_socket_error', `value`='Socket error: {{error}}', `topic`='Errors';

REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_logic_success', `value`='Success.', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_logic_test', `value`='Test.', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_device_registered', `value`='New device registered.', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_device_unregistered', `value`='Device unregistered.', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_review_not_available', `value`='N/A', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_review_not_found', `value`='Review not found.', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_new_order_received', `value`='New order received', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_low_stock_product', `value`='Low stock for one or more products', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='msg_mobad_discount_coupon_created', `value`='Discount coupon was successfully created.', `topic`='Labels';

REPLACE INTO `xcart_languages` SET `code`='en', `name`='eml_link_device_notif_subj', `value`='Mobile Admin: Mobile device was linked', `topic`='E-Mail';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='eml_unlink_device_notif_subj', `value`='Mobile Admin: Mobile device was unlinked', `topic`='E-Mail';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='eml_link_device_notif', `value`='A new mobile device was linked  with your mobile administration feature', `topic`='E-Mail';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='eml_unlink_device_notif', `value`='A new mobile device was unlinked from your mobile administration functionality', `topic`='E-Mail';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_device_data', `value`='Device data', `topic`='E-Mail';

REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_device_manufacturer', `value`='Manufacturer', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_device_model', `value`='Model', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_device_serial', `value`='Serial', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_device_os', `value`='OS', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_device_imei', `value`='IMEI', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_os_android', `value`='Android', `topic`='Labels';
REPLACE INTO `xcart_languages` SET `code`='en', `name`='lbl_os_ios', `value`='iOS', `topic`='Labels';
