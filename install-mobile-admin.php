<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Mobile Admin install script
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Mobile Admin
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id$
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

define('XCART_INSTALL', 1);

define('XMODULE_DIR', 'Mobile_Admin');

$module_definition = array(
    'name' => 'Mobile Admin',
    'script' => 'install-mobile-admin.php',
    'prefix' => 'x-mobile-admin',
    'successmessage' => 'func_success',
    'is_installed' => 'func_is_installed',
);

if (!@include('./top.inc.php')) {
    die('X-Cart not found in ' . dirname(__FILE__));
}

if (!@include('./init.php')) {
    die('init.php not found. Please, unpack ' . $module_definition['name'] . ' module in &lt;xcart&gt; directory');
}

function func_success()
{
    global $xcart_web_dir;

    ?>
    <ol>
        <li>
            <a target="_blank" style="text-decoration: underline; font-weight: bold;"
               href="<?php echo $xcart_web_dir . DIR_ADMIN; ?>/configuration.php?option=Mobile_Admin">Set up Mobile
                Admin</a>
        </li>
    </ol>
<?php

}

function func_is_installed()
{
    global $sql_tbl, $xcart_dir;

    require_once  $xcart_dir . '/modules/' . XMODULE_DIR . '/install.php';

    return false !== func_query_first_cell("SELECT moduleid FROM $sql_tbl[modules] WHERE module_name = 'Mobile_Admin'");
}

$xcartVersion = func_query_first_cell("SELECT value FROM $sql_tbl[config] WHERE name = 'version'");
if (version_compare($xcartVersion, '4.7.0') >= 0 && version_compare($xcartVersion, '4.8.0') < 0) {
    define('COMPATIBLE_VERSION', '4.7.x');
}

require_once $xcart_dir . '/include/install.php';
