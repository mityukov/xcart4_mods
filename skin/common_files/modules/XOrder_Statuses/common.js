/* vim: set ts=2 sw=2 sts=2 et: */
/**
 * Common JavaScript variables and functions
 * 
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage ____sub_package____
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v3 (xcart_4_7_5), 2016-02-18 13:44:28, common.js, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

function func_orderstatuses_change_circle(elem, new_status)
{
  var circle_div = $(elem).parent().parent().children('div[class^="xostatus-search-status-indicator xostatus-orderstatus-background"]');
  if (
    $(circle_div).length > 0
  ) {
    $(circle_div).attr('class', 'xostatus-search-status-indicator xostatus-orderstatus-background-' + $(elem).val());
  }
}
