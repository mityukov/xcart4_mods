{*
3395638dec28cf6d726eb2daf13393ae4faf07c3, v8 (xcart_4_7_4), 2015-10-16 10:41:14, status_selector.tpl, random

vim: set ts=2 sw=2 sts=2 et:
*}

{tpl_order_statuses var="avail_statuses"}
{if $extended eq "" and $status eq ""}

{$lng.lbl_wrong_status}

{elseif $mode eq "select"}{*if $extended eq "" and $status eq ""*}

{if $config.XOrder_Statuses.xostat_use_colors eq 'Y'}
<div class="xostatus-search-status-indicator xostatus-orderstatus-background-{$status|escape}">&nbsp;</div>

<div class="xostatus-orderstatus-select-container">
{/if}

{if $usertype eq 'C'}
    {load_defer file="modules/XOrder_Statuses/common.js" type="js"}
{/if}

<select name="{$name}" {$extra}{if $config.XOrder_Statuses.xostat_use_colors eq 'Y'} onchange="javascript: func_orderstatuses_change_circle(this);"{/if}>
{if $extended ne ""}
  <option value="">&nbsp;</option>
{/if}
{foreach from=$avail_statuses item=st}
{if $st.code ne 'A' || $st.code eq $status || ($st.code eq 'A' && $display_preauth)}
  <option value="{$st.code}"{if $status eq $st.code} selected="selected"{/if}>{$st.name}</option>
{/if}
{/foreach}
</select>

{if $config.XOrder_Statuses.xostat_use_colors eq 'Y'}
</div>

<div class="clearing"></div>
{/if}

{elseif $mode eq "static"}{*if $extended eq "" and $status eq ""*}

{foreach from=$avail_statuses item=st}
  {if ($st.code ne '' && $status eq $st.code) || ($st.code eq '' && $status eq $st.statusid)}{$st.name}{/if}
{/foreach}

{/if}
