{*
eaea44bd56213edaefae074ba688c9ef3303be57, v2 (xcart_4_7_3), 2015-06-19 13:09:40, dhl.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}
<form action="http://www.dhl.com/en/express/tracking.html" name="trackingIndex" id="trackingIndex" method="get" target="_blank">
<input type="hidden" id="AWB" name="AWB" value="{$order.tracking|escape}" />
<input type="submit" value="{$lng.lbl_track_it|strip_tags:false|escape}" />
<br />
{$lng.txt_dhl_redirection}
</form>
