{*
626362abdced0086c1c116748026db88b0984243, v1 (xcart_4_7_4), 2015-10-01 14:20:13, cart_totals.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
{if $cart.{XCPitneyBowesDefs::CART_TRANSACTIONID}}
  {* Show details section only for Pitney Bowes*}
  <tr class="pb-shipping-details pb-transportation">
    <td class="total-name">
      <span>{$lng.lbl_pitney_bowes_transportation_part}:</span>
    </td>
    <td class="total-value">{currency value={pb_transportation_part subtotal=$cart.subtotal country=$userinfo.s_country cart=$cart}}</td>
  {if $config.General.checkout_module eq 'Fast_Lane_Checkout'}
    <td class="total-value-alt">{alter_currency value={pb_transportation_part subtotal=$cart.subtotal country=$userinfo.s_country cart=$cart}}</td>
  {/if}
  </tr>
  <tr class="pb-shipping-details pb-importation">
    <td class="total-name">
      <span>{$lng.lbl_pitney_bowes_importation_part}:</span>
    </td>
    <td class="total-value">{currency value={pb_importation_part subtotal=$cart.subtotal country=$userinfo.s_country cart=$cart}}</td>
  {if $config.General.checkout_module eq 'Fast_Lane_Checkout'}
    <td class="total-value-alt">{alter_currency value={pb_importation_part subtotal=$cart.subtotal country=$userinfo.s_country cart=$cart}}</td>
  {/if}
  </tr>
{/if}
