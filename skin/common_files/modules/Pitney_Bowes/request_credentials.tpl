{*
5d46eb610d03c80bf6c1366bd379bd2b390fc172, v3 (xcart_4_7_4), 2015-10-20 13:54:56, request_credentials.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
<!-- MAIN -->
{capture name=dialog}

  <form action="configuration.php?option={$smarty.const.PITNEY_BOWES}&amp;{XCPitneyBowesDefs::CONTROLLER}={XCPitneyBowesDefs::CONTROLLER_CREDENTIALS}" method="post" name="{XCPitneyBowesDefs::CONTROLLER_CREDENTIALS}form">

    <table width="100%" cellspacing="3" cellpadding="2" class="pb-request-credentials">
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr valign="middle"{if $fill_errors and $fill_errors.pb_cr_name}class="pb-field-error"{/if}>
        <td>{$lng.lbl_your_name}:</td>
        <td class="Star">*</td>
        <td nowrap="nowrap">
          <input type="text" name="pb_cr_name" size="32" maxlength="64" value="{if $fill_errors and $fill_errors.pb_cr_name}{$pb_cr_name}{else}{$pb_cr_name|default:"{$userinfo.firstname} {$userinfo.lastname}"}{/if}" maxlength="255" />
        </td>
      </tr>
      <tr valign="middle"{if $fill_errors and $fill_errors.pb_cr_company}class="pb-field-error"{/if}>
        <td>{$lng.lbl_company_name}:</td>
        <td class="Star">*</td>
        <td nowrap="nowrap">
          <input type="text" name="pb_cr_company" size="32" maxlength="128" value="{if $fill_errors and $fill_errors.pb_cr_company}{$pb_cr_company}{else}{$pb_cr_company|default:"{$config.Company.company_name}"}{/if}" maxlength="255" />
        </td>
      </tr>
      <tr valign="middle"{if $fill_errors and $fill_errors.pb_cr_address}class="pb-field-error"{/if}>
        <td>{$lng.opt_location_address}:</td>
        <td class="Star">*</td>
        <td nowrap="nowrap">
          <input type="text" name="pb_cr_address" size="32" maxlength="32" value="{if $fill_errors and $fill_errors.pb_cr_address}{$pb_cr_address}{else}{$pb_cr_address|default:"{$config.Company.location_address}, {$config.Company.location_city}, {$config.Company.location_state}, {$config.Company.location_country}"}{/if}" maxlength="255" />
        </td>
      </tr>
      <tr valign="middle"{if $fill_errors and $fill_errors.pb_cr_email}class="pb-field-error"{/if}>
        <td>{$lng.lbl_email_address}:</td>
        <td class="Star">*</td>
        <td nowrap="nowrap">
          <input type="text" name="pb_cr_email" size="32" maxlength="128" value="{if $fill_errors and $fill_errors.pb_cr_email}{$pb_cr_email}{else}{$pb_cr_email|default:"{$userinfo.email}"}{/if}" maxlength="255" />
        </td>
      </tr>
      <tr valign="middle"{if $fill_errors and $fill_errors.pb_cr_phone}class="pb-field-error"{/if}>
        <td>{$lng.lbl_phone_number}:</td>
        <td class="Star">*</td>
        <td nowrap="nowrap">
          <input type="text" name="pb_cr_phone" size="32" maxlength="32" value="{if $fill_errors and $fill_errors.pb_cr_phone}{$pb_cr_phone}{else}{$pb_cr_phone|default:"{$config.Company.company_phone}"}{/if}" maxlength="255" />
        </td>
      </tr>
      <tr valign="middle"{if $fill_errors and $fill_errors.pb_cr_version}class="pb-field-error"{/if}>
        <td>{$lng.lbl_env_software_version}:</td>
        <td class="Star">*</td>
        <td nowrap="nowrap">
          <input type="text" name="pb_cr_version" size="32" maxlength="32" value="{if $fill_errors and $fill_errors.pb_cr_version}{$pb_cr_version}{else}{$pb_cr_version|default:"{$xcart_version}"}{/if}" maxlength="255" />
        </td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3"><hr /></td>
      </tr>
      <tr class="main-button">
        <td colspan="3" align="right"><input type="submit" class="big-main-button" value="{$lng.lbl_pitney_bowes_request_credentials}"/></td>
      </tr>
    </table>
  </form>
  <script type="text/javascript">
    $(function () {
        $('input:submit, input:button, button, a.simple-button').button();
    });
  </script>
{/capture}
{include file="dialog.tpl" content=$smarty.capture.dialog extra='width="100%"'}
<!-- /MAIN -->
