{*
626362abdced0086c1c116748026db88b0984243, v1 (xcart_4_7_4), 2015-10-01 14:20:13, order_parcels_button.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}
{if $order.{XCPitneyBowesDefs::CART_TRANSACTIONID}}
  {if $order.status eq 'A' or $order.status eq 'P' or $order.status eq 'C'}
    <td class="ButtonsRowRight">
      <div class="bp-order-parcels">
        {include file="buttons/button.tpl" button_title=$lng.lbl_pitney_bowes_parcels href="order.php?orderid={$orderid}&controller={XCPitneyBowesDefs::CONTROLLER_PARCELS}" substyle="link"}
      </div>
    </td>
  {/if}
{/if}
