/* vim: set ts=2 sw=2 sts=2 et: */
/**
 * Pitney Bowes controller
 */

(function ($) {

    init = function () {
        $('[name="order_number"]').parent('form').on('submit', setOrderNumber);
        $('[name="hub_address"]').parent('form').on('submit', setHubAddress);

        $('.pb-delete-parcel-btn').parent('form').on('submit', onDeleteParcel);
        $('.pb-delete-parcel-item-btn').parent('form').on('submit', onDeleteParcelItem);

        $('.pb-order-number').on('change', onOrderSelected);

        $('.pb-request-credentials').on('click', onRequestCredentials);
    };

    setOrderNumber = function()
    {
        $(this).find('[name=order_number]').val($('.pb-order-number > option:selected').text());
    };

    setHubAddress = function()
    {
        $(this).find('[name=hub_address]').val($('.pb-hub-address').val());
    };

    onDeleteParcel = function()
    {
        var initiator = $(this).find('input[type="submit"]');
        var question = 'Are you sure you want to delete?';

        if (
            initiator
            && initiator.attr('data-question')
        ) {
            question = initiator.attr('data-question');
        }

        return confirm(question);
    };

    onDeleteParcelItem = function()
    {
        var initiator = $(this).find('input[type="submit"]');
        var question = 'Are you sure you want to delete?';

        if (
            initiator
            && initiator.attr('data-question')
        ) {
            question = initiator.attr('data-question');
        }

        return confirm(question);
    };

    onOrderSelected = function()
    {
        $('.pb-hub-address').val($(this).val());
    };

    onRequestCredentials = function()
    {
        popupOpen('configuration.php?option=Pitney_Bowes&controller=Credentials', '', { width: 450 });
    };

    $(document).ready(init);

})(jQuery);
