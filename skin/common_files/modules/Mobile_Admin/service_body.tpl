{*
2177942f6502972343136b91d2371dfe7971ebb0, v2 (xcart_4_6_5), 2014-09-05 17:49:07, service_body.tpl, aim
vim: set ts=2 sw=2 sts=2 et:
*}
{if $mobileAdminNotificationManagerUrl ne ''}
<script type="text/javascript">
//<![CDATA[
  var mobileAdminNotificationManagerUrl = '{$mobileAdminNotificationManagerUrl|escape:"javascript"}';

{literal}
  (function ($) {
    $(window).bind(
        'load',
        function () {
          $.ajax({
            url: mobileAdminNotificationManagerUrl,
            timeout: 2000,
            cache: false
          });
        }
    );
  })(jQuery);
{/literal}
//]]>
</script>
{/if}