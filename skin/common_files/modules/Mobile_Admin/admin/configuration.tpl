{*
vim: set ts=2 sw=2 sts=2 et:

MobileAdmin module settings template
*}

{include file="modules/Mobile_Admin/admin/buy_subscription.tpl"}

<form action="configuration.php?option={$option|escape}" method="post" name="mobile_admin_form" id="mobile_admin_form"
      xmlns="http://www.w3.org/1999/html">
    <input type="hidden" name="mobile_admin_mode" id="mobile_admin_mode" value=""/>
    <input type="hidden" name="mobile_admin_dev_id" id="mobile_admin_dev_id" value=""/>
    {include file="main/subheader.tpl" title="Overview"}

    <p> X-Cart Mobile Admin lets you easily manage your online store from your iPhone and Android smartphones.</br>
    </p>

    {include file="main/subheader.tpl" title="Subscriptions"}

     {if $mobile_admin_subscription eq "active"}
         Paid subscription till: {$mobile_admin_subscription_till}<br>
     {elseif $mobile_admin_subscription eq "trial"}
         Trial subscription till: {$mobile_admin_subscription_till}<br>
	<br>
         <input type="button" value="Buy subscription" onclick="javascript: buySubscription();"/><br>
     {elseif $mobile_admin_subscription eq "expired"}
         Trial subscription till: {$mobile_admin_subscription_till}<br>
        <br>
        <input type="button" value="Buy subscription" onclick="javascript: buySubscription();"/><br>
    {else}
         Free subscription.
         </br>
         <input type="submit" value="Get trial" onclick="$('#mobile_admin_mode').val('trial');"/>
         <input type="button" value="Buy subscription" onclick="javascript: buySubscription();"/><br>

    {/if}
    <br>


    {include file="main/subheader.tpl" title="Get started"}
    <p>
        <h2>1.  Download and install X-Cart Mobile Admin application</h2>
        <!-- img id="appstore" src="{$ImagesDir}/app_store.jpg" alt="" width="14%"/ -->
        <a href="https://play.google.com/store/apps/details?id=com.xcart.admin"><img id="googleplay" src="{$ImagesDir}/play_store.jpg" alt="" width="13%"/></a>
    </p>

    <p>
        <h2>2.Get Authorization key</h2>
        {if $mobile_admin_api_key eq "testKey"}
            <input type="submit" value="Generate key" onclick="$('#mobile_admin_mode').val('get_new_key')"/>
        {else}
            Current Api key is : "<b>{$mobile_admin_api_key}</b>". <input type="submit" value="Generate new key" onclick="$('#mobile_admin_mode').val('get_new_key')"/>
        {/if}
    </p>

    <p>
        <h2>3. Link your mobile application with your store.</h2>
        At login page of X-Cart Mobile Admin click <b>Scan QR</b> button an scan this code: </br>
        <img id="QRCode" src="{$mobile_admin_QR_url}&url={$mobile_admin_connector_URL_encoded}" alt="" />
        </br>
        <h3>OR</h3>
        </br>
        Manually enter "<b>{$mobile_admin_connector_URL}</b>" as Shop url and "<b>{$mobile_admin_api_key}</b>" as Authorization key at Login page of the app </br></br>
        <img id="login_r" src="{$ImagesDir}/login_r.png" alt="" width="20%"/>
    </p>

    <p><h2>4. That's All!</h2></p>

    </br>

    {include file="main/subheader.tpl" title="The list of linked devices"}

    <table cellpadding="3" cellspacing="1" width="100%" id="mobileadmindeviceslist">

    <tr class="TableHead" style="height: 28px; font-size: 14px;">
        <td>Manufacturer</td>
        <td>Model</td>
        <td>Serial</td>
        <td>OS</td>
        <td>IMEI</td>
        <td>Created</td>
        <td>Push Notifications</td>
        <td></td>
    </tr>

    {foreach item=device from=$mobile_admin_devices}

        <tr>
            <td align="center" id="mobile_admin_dev_id_{$device.manufacturer}">{$device.manufacturer}</td>
            <td align="center">{$device.model}</td>
            <td align="center">{$device.serial}</td>
            <td align="center">{if $device.os_type eq "I"}{$lng.lbl_os_ios}{else}{$lng.lbl_os_android}{/if}&nbsp;{$device.os_version}</td>
            <td align="center">{$device.imei}</td>
            <td align="center">{$device.date}</td>
            <td align="center">
              <select name="device_status_{$device.id}" style="width: 100px;" onchange="javascript: disableDevice(this, '{$device.id}');">
                <option value="Y" {if $device.status eq "Y"} selected="selected"{/if}>Enabled</option>
                <option value="N" {if $device.status eq "N"} selected="selected"{/if}>Disabled</option>
              </select>
            </td>
            <td align="center"> <input type="submit" value="unlink" onclick="javascript: unlinkDevice(this, '{$device.id}');"/></td>
            </tr>

    {/foreach}

</table>
</form>
