{*
$Id$
vim: set ts=2 sw=2 sts=2 et:
MobileAdmin module settings template
*}
{if $plain eq 'Y'}
{$lng.lbl_device_manufacturer}: {$device_data.manufacturer}
{$lng.lbl_device_model}: {$device_data.model}
{$lng.lbl_device_serial}: {$device_data.serial}
{$lng.lbl_device_os}: {$device_data.android_version}
{$lng.lbl_device_imei}: {$device_data.imei}
{else}
<table cellpadding="2" cellspacing="0">
  <tr>
    <th>{$lng.lbl_device_manufacturer}</th>
    <th>{$lng.lbl_device_model}</th>
    <th>{$lng.lbl_device_serial}</th>
    <th>{$lng.lbl_device_os}</th>
    <th>{$lng.lbl_device_imei}</th>
  </tr>
  <tr>
    <td>{$device_data.manufacturer}</td>
    <td>{$device_data.model}</td>
    <td>{$device_data.serial}</td>
    <td>{$device_data.android_version}</td>
    <td>{$device_data.imei}</td>
  </tr>
</table>
{/if}
