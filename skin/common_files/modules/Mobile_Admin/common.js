/* vim: set ts=2 sw=2 sts=2 et: */
/**
 * Mobile Admin JavaScript variables and functions
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage ____sub_package____
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id$
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

/**
 * Custom dialog with reload buttons using jQuery UI
 *
 * @return void
 * @see    ____func_see____
 */
function buySubscription()
{
  document.forms.buy_subscription.submit();
}

function disableDevice(obj, id)
{
  $('#mobile_admin_mode').val('change_dev_status');
  $('#mobile_admin_dev_id').val(id);

  submitForm(obj);
}

function unlinkDevice(obj, id)
{
  $('#mobile_admin_mode').val('unlink');
  $('#mobile_admin_dev_id').val(id);

  submitForm(obj);
}
