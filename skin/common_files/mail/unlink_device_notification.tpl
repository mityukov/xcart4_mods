{*
$Id$
vim: set ts=2 sw=2 sts=2 et:
*}
{include file="mail/mail_header.tpl"}

{$lng.eml_unlink_device_notif}

{$lng.lbl_device_data}:
---------------------
{include file="modules/Mobile_Admin/device_data.tpl" device_data=$device_data plain='Y'}

{include file="mail/signature.tpl"}
