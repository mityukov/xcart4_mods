{*
$Id$
vim: set ts=2 sw=2 sts=2 et:
*}
{include file="mail/html/mail_header.tpl"}

<br />{$lng.eml_link_device_notif}

<br />{$lng.lbl_device_data}:

{include file="modules/Mobile_Admin/device_data.tpl" device_data=$device_data}

{include file="mail/html/signature.tpl"}
