{*
6f27c8266fc469c8cf1ddac70ea6b7edfdce6489, v1 (xcart_4_7_5), 2016-02-10 23:46:37, payment_xcc.tpl, mixon

vim: set ts=2 sw=2 sts=2 et:
*}

{include file="widgets/creditcardform/creditcardform.tpl" containerName="transparent-redirect-box" cardCode="Y"}

{load_defer file="lib/jquery.bind-first.min.js" type="js"}
{load_defer file="js/ps_paypal_redirect.js" type="js"}

{getvar var='paypal_redirect_payment_id' func='func_paypal_get_redirect_payment_id'}
<script type="text/javascript">
    //<![CDATA[
    var pptr_msg_token_error   = '{$lng.txt_ajax_error_note|wm_remove|escape:"javascript"}';
    var pptr_msg_being_placed = '{$lng.msg_order_is_being_placed|wm_remove|escape:"javascript"}';
    $(document).ready(function() {ldelim}
        new ajax.widgets.paypal_redirect('{$paypal_redirect_payment_id}');
    {rdelim});
    //]]>
</script>
