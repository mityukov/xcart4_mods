{*
c0b5bca07617f5c9acaab782a0dec4b735db0d59, v7 (xcart_4_7_4), 2015-08-21 09:17:23, service_body_js.tpl, aim

vim: set ts=2 sw=2 sts=2 et:
*}

{if $amazon_enabled}
    <script type="text/javascript" src="{$amazon_widget_url}"></script>
{/if}

{if $active_modules.Amazon_Payments_Advanced}
  {include file="modules/Amazon_Payments_Advanced/service_body.tpl"}
{/if}

{if $active_modules.Google_Analytics
  and $config.Google_Analytics.ganalytics_version eq 'Asynchronous'}
  {*The first part is loaded in modules/Google_Analytics/ga_code_async.tpl*}
  {capture name=ga_code_async_js_part2}
    (function() {ldelim}
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    {rdelim})();
  {/capture}
  {load_defer file="ga_code_async_js_part2" direct_info=$smarty.capture.ga_code_async_js_part2 type="js"}
{/if}
