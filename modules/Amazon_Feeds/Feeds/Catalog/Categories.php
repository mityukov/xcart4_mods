<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Categories
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    039f19367a96265362bfed075aa4e6a50af287c8, v1 (xcart_4_7_5), 2016-01-30 17:39:30, Categories.php, mixon
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\AmazonFeeds\Feeds\Catalog;

class Categories extends \XC_Singleton { // {{{

    // <editor-fold desc="Categories" defaultstate="collapsed">
    protected $cats = array(
        'Beauty' => array(
            'ProductType' => array(
                'BeautyMisc'
            )
        ),
        'Books' => array(
            'ProductType' => array(
                'BooksMisc' => array(
                    'Binding' => array(
                        'Accessory',
                        'Album',
                        'Audiocd',
                        'Audiodownload',
                        'Bathbook',
                        'Boardbook',
                        'Bondedleather',
                        'Calendar',
                        'Cardbook',
                        'Cards',
                        'Cassette',
                        'Cdrom',
                        'Comic',
                        'Diary',
                        'Dvdrom',
                        'Flexibound',
                        'Foambook',
                        'Game',
                        'Hardcover',
                        'Hardcovercomic',
                        'Hardcoverspiral',
                        'Imitationleather',
                        'Journal',
                        'Kindleedition',
                        'Leatherbound',
                        'Library',
                        'Libraryaudiocd',
                        'Libraryaudiomp3',
                        'Looseleaf',
                        'Map',
                        'Massmarket',
                        'Microfiche',
                        'Microfilm',
                        'Miscsupplies',
                        'Mook',
                        'Mp3cd',
                        'Pamphlet',
                        'Paperback',
                        'Paperbackbunko',
                        'Paperbackshinsho',
                        'Perfect',
                        'Plasticcomb',
                        'Popup',
                        'Preloadeddigitalaudioplayer',
                        'Ragbook',
                        'Ringbound',
                        'Roughcut',
                        'School',
                        'Sheetmusic',
                        'Singleissuemagazine',
                        'Slide',
                        'Spiralbound',
                        'Stationery',
                        'Tankobonhardcover',
                        'Tankobonsoftcover',
                        'Textbook',
                        'Toy',
                        'Transparency',
                        'Turtleback',
                        'Unbound',
                        'Vinylbound',
                        'Wallchart',
                        'Workbook',
                    )
                )
            )
        ),
        'CameraPhoto' => array(
            'ProductType' => array(
                'FilmCamera',
                'Camcorder',
                'DigitalCamera',
                'DigitalFrame',
                'Binocular',
                'SurveillanceSystem',
                'Telescope',
                'Microscope',
                'Darkroom',
                'Lens',
                'LensAccessory',
                'Filter',
                'Film',
                'BagCase',
                'BlankMedia',
                'PhotoPaper',
                'Cleaner',
                'Flash',
                'TripodStand',
                'Projection',
                'PhotoStudio',
                'LightMeter',
                'PowerSupply',
                'OtherAccessory',
            )
        ),
        'CE' => array(
            'ProductType' => array(
                'Antenna',
                'AudioVideoAccessory',
                'AVFurniture',
                'BarCodeReader',
                'CEBinocular',
                'CECamcorder',
                'CameraBagsAndCases',
                'CEBattery',
                'CEBlankMedia',
                'CableOrAdapter',
                'CECameraFlash',
                'CameraLenses',
                'CameraOtherAccessories',
                'CameraPowerSupply',
                'CarAlarm',
                'CarAudioOrTheater',
                'CarElectronics',
                'ConsumerElectronics',
                'CEDigitalCamera',
                'DigitalPictureFrame',
                'DigitalVideoRecorder',
                'DVDPlayerOrRecorder',
                'CEFilmCamera',
                'GPSOrNavigationAccessory',
                'GPSOrNavigationSystem',
                'HandheldOrPDA',
                'Headphones',
                'HomeTheaterSystemOrHTIB',
                'KindleAccessories',
                'KindleEReaderAccessories',
                'KindleFireAccessories',
                'MediaPlayer',
                'MediaPlayerOrEReaderAccessory',
                'MediaStorage',
                'MiscAudioComponents',
                'PC',
                'PDA',
                'Phone',
                'PhoneAccessory',
                'PhotographicStudioItems',
                'PortableAudio',
                'PortableAvDevice',
                'PowerSuppliesOrProtection',
                'RadarDetector',
                'RadioOrClockRadio',
                'ReceiverOrAmplifier',
                'RemoteControl',
                'Speakers',
                'StereoShelfSystem',
                'CETelescope',
                'Television',
                'Tuner',
                'TVCombos',
                'TwoWayRadio',
                'VCR',
                'CEVideoProjector',
                'VideoProjectorsAndAccessories',
            )
        ),
        'ClothingAccessories' => array(),
        'Computers' => array(
            'ProductType' => array(
                'CarryingCaseOrBag',
                'ComputerAddOn',
                'ComputerComponent',
                'ComputerCoolingDevice',
                'ComputerDriveOrStorage',
                'ComputerInputDevice',
                'ComputerProcessor',
                'ComputerSpeaker',
                'Computer',
                'FlashMemory',
                'InkOrToner',
                'Keyboards',
                'MemoryReader',
                'Monitor',
                'Motherboard',
                'NetworkingDevice',
                'NotebookComputer',
                'PersonalComputer',
                'Printer',
                'RamMemory',
                'Scanner',
                'SoundCard',
                'SystemCabinet',
                'SystemPowerDevice',
                'TabletComputer',
                'VideoCard',
                'VideoProjector',
                'Webcam',
            )
        ),
        'FoodAndBeverages' => array(
            'ProductType' => array(
                'Food',
                'HouseholdSupplies',
                'Beverages',
                'HardLiquor',
                'AlcoholicBeverages',
                'Wine',
            )
        ),
        'Health' => array(
            'ProductType' => array(
                'HealthMisc',
                'PersonalCareAppliances',
                'PrescriptionDrug',
            )
        ),
        'Home' => array(
            'ProductType' => array(
                'BedAndBath',
                'FurnitureAndDecor',
                'Kitchen',
                'OutdoorLiving',
                'SeedsAndPlants',
                'Art',
            )
        ),
        'Jewelry' => array(
            'ProductType' => array(
                'Watch',
                'FashionNecklaceBraceletAnklet',
                'FashionRing',
                'FashionEarring',
                'FashionOther',
                'FineNecklaceBraceletAnklet',
                'FineRing',
                'FineEarring',
                'FineOther',
            )
        ),
        'Miscellaneous' => array(
            'ProductType' => array(
                'Antiques',
                'Art',
                'Car_Parts_and_Accessories',
                'Coins',
                'Collectibles',
                'Crafts',
                'Event_Tickets',
                'Flowers',
                'Gifts_and_Occasions',
                'Gourmet_Food_and_Wine',
                'Hobbies',
                'Home_Furniture_and_Decor',
                'Home_Lighting_and_Lamps',
                'Home_Organizers_and_Storage',
                'Jewelry_and_Gems',
                'Luggage',
                'Major_Home_Appliances',
                'Medical_Supplies',
                'Motorcycles',
                'Musical_Instruments',
                'Pet_Supplies',
                'Pottery_and_Glass',
                'Prints_and_Posters',
                'Scientific_Supplies',
                'Sporting_and_Outdoor_Goods',
                'Sports_Memorabilia',
                'Stamps',
                'Teaching_and_School_Supplies',
                'Watches',
                'Wholesale_and_Industrial',
                'Misc_Other',
            )
        ),
        'Music' => array(
            'ProductType' => array(
                'MusicPopular',
                'MusicClassical',
            )
        ),
        'MusicalInstruments' => array(
            'ProductType' => array(
                'BrassAndWoodwindInstruments',
                'Guitars',
                'InstrumentPartsAndAccessories',
                'KeyboardInstruments',
                'MiscWorldInstruments',
                'PercussionInstruments',
                'SoundAndRecordingEquipment',
                'StringedInstruments',
            )
        ),
        'Office' => array(
            'ProductType' => array(
                'ArtSupplies',
                'EducationalSupplies',
                'OfficeProducts',
                'PaperProducts',
                'WritingInstruments',
                'BarCode',
                'Calculator',
                'InkToner',
                'MultifunctionDevice',
                'OfficeElectronics',
                'OfficePhone',
                'OfficePrinter',
                'OfficeScanner',
                'VoiceRecorder',
            )
        ),
        'Outdoors' => array(
            'ProductType' => array(
                'OutdoorRecreationProduct'
            ),
        ),
        'PetSupplies' => array(
            'ProductType' => array(
                'PetSuppliesMisc'
            ),
        ),
        'AutoAccessory' => array(
            'ProductType' => array(
                'AutoAccessoryMisc',
                'AutoPart',
                'PowersportsPart',
                'PowersportsVehicle',
                'ProtectiveGear',
                'Helmet',
                'RidingApparel',
            ),
        ),
        'Shoes' => array(
            'ClothingType' => array(
                'Accessory',
                'Bag',
                'Shoes',
                'ShoeAccessory',
                'Handbag',
                'Eyewear',
            ),
        ),
        'Sports' => array(
            'ProductType' => array(
                'SportingGoods',
                'GolfClubHybrid',
                'GolfClubIron',
                'GolfClubPutter',
                'GolfClubWedge',
                'GolfClubWood',
                'GolfClubs',
            ),
        ),
        'SoftwareVideoGames' => array(
            'ProductType' => array(
                'Software',
                'HandheldSoftwareDownloads',
                'SoftwareGames',
                'VideoGames',
                'VideoGamesAccessories',
                'VideoGamesHardware',
            ),
        ),
        'TiresAndWheels' => array(
            'ProductType' => array(
                'Tires',
                'Wheels',
            )
        ),
        'Tools' => array(),
        'Toys' => array(
            'ProductType' => array(
                'ToysAndGames',
                'Hobbies',
                'CollectibleCard',
                'Costume',
            )
        ),
        'ToysBaby' => array(
            'ProductType' => array(
                'ToysAndGames',
                'BabyProducts',
            )
        ),
        'Video' => array(
            'ProductType' => array(
                'VideoDVD',
                'VideoVHS',
            )
        ),
        'Wireless' => array(
            'ProductType' => array(
                'WirelessAccessories',
                'WirelessDownloads',
            )
        ),
    );
    // </editor-fold>

    public function getCategories()
    { // {{{
        return $this->cats;
    } // }}}

    public function getFlattenCategories()
    { // {{{
        static $result = array();

        if (!empty($result)) { return $result; }

        $iterator = function ($elements, &$result, $path = '', $code = 0, $level = 0) use (&$iterator) {
            if (is_array($elements)) {
                foreach ($elements as $key => $element) {
                    if ($level === 0) {
                        $code = $key;
                    }
                    if (is_numeric($key)) {
                        $key = $element;
                    }
                    $separator = !empty($path) ? \XCAmazonFeedsDefs::FEED_PATH_DELIMITER : '';

                    $iterator($element, $result, $path . $separator . $key, $code, $level + 1);
                }
            } else {
                $result[$code][$path] = $elements;
            }
        };

        $iterator($this->cats, $result);

        return $result;
    } // }}}

    public static function getInstance()
    { // {{{
        // Call parent getter
        return parent::getClassInstance(__CLASS__);
    } // }}}

} // }}}
