<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Classes
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v4 (xcart_4_7_5), 2016-02-18 13:44:28, File.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Catalog\Import;

/**
 * @see https://wiki.ecommerce.pb.com/display/TECH4/Catalog
 */
abstract class File extends \XCart\Modules\PitneyBowes\Catalog\File { // {{{

    protected $_step_name = self::STEP_NAME_IMPORT;

    protected $_filter;

    public function load()
    { // {{{

        foreach ($filenames = glob($this->getRealPathFilter()) as $filename) {
            $this->loadFile($filename);
        }

    } // }}}

    protected function getFilenameFilter()
    { // {{{
        if (empty($this->_filter)) {
            $parts = array(
                'Sender_ID'         => static::PITNEY_BOWES_RECIPIENT_ID,
                'Data_Feed_Name'    => $this->_feedname,
                'Operation'         => $this->_operation,
                'Recipient_ID'      => $this->_config->sender_id,
                'UTC_Date_Time'     => '*',
                'Random_6_Digits'   => '*',
            );
            $this->_filter = implode('_', $parts) . '.csv';
        }

        return $this->_filter;
    } // }}}

    protected function getRealPathFilter()
    { // {{{
        return $this->_filepath . XC_DS . $this->getFilenameFilter();
    } // }}}

    protected function rewindToLine($lineNumber)
    {
        $count = 0;

        $count += intval($this->readImportHeader());

        while (
            ($data = fgetcsv($this->_filePointer, 0, self::CSV_DELIMITER, self::CSV_ENCLOSURE)) !== FALSE
            && $count < $lineNumber
        ) {
            $count++;
        }

        return $count;
    }

    protected function loadFile($filename)
    { // {{{

        $this->_filename = pathinfo($filename, PATHINFO_BASENAME);

        $this->_filePointer = fopen($this->getRealPath(), 'r');

        if (!$this->_filePointer) {
            func_pitney_bowes_debug_log(func_get_langvar_by_name('err_pitney_bowes_cannot_open_import_file', array('file' => $this->getRealPath())));
            return false;
        }

        if ($this->_state == self::FILE_STATUS_NEW) {
            $this->_count = $this->getLinesCount($this->getRealPath());
            $this->_pid = $this->getPid();
        }

        if (
            $this->_position < $this->_count
            && $this->_position > 0
        ) {
            $this->rewindToLine($this->_position);
        }

        $loop_guard = 0;
        while (
            $this->_position < $this->_count
            && $loop_guard < $this->_count
        ) {

            if ($this->_position == 0) {
                $this->_state = self::FILE_STATUS_WORKING;
                $this->readImportHeader();
            }

            for (
                $count = 1;
                $count <= $this->_step
                    && $this->_position <= $this->_count;
                $count++
            ) {
                $this->readImportLine();
                $this->_position++;
            }
            $loop_guard += max(1, $count);

            func_flush('.');

            if ($this->_step_time === 0) {
                $this->_step_time = time() - $this->_start_time;
            }

            $this->setCacheClassState();

            $this->_step_progress = ($this->_position / $this->_count) * 100;
        }

        fclose($this->_filePointer);

        if ($this->_position == $this->_count) {
            $this->_state = self::FILE_STATUS_FINISHED;
            $this->setCacheClassState();

            if (method_exists($this, 'onFinished')) {
                $this->onFinished();
            }
        }
    } // }}}

    /**
     * Read HEADER from file
     *
     * @return mixed
     */
    protected function readImportHeader()
    { // {{{
        $result = false;

        $expected_columns = $this->_columns;
        $columns = fgetcsv($this->_filePointer, 0, self::CSV_DELIMITER, self::CSV_ENCLOSURE);

        if (!empty($columns)) {
            $diff = array_diff(array_keys($expected_columns), array_values($columns));
            if (($result = empty($diff))) {
                // Re-sort columns using order in CSV file
                $resorted_columns = array();
                foreach (array_values($columns) as $column) {
                    $resorted_columns[$column] = $expected_columns[$column];
                }
                $this->_columns = $resorted_columns;
            }
        }

        return $result;
    } // }}}

    /**
     * Read import LINE from file
     *
     * @return mixed
     */
    protected function readImportLine()
    { // {{{
        $result = false;

        if (!empty($this->_columns)) {
            if (
                ($csv_line = fgetcsv($this->_filePointer, 0, self::CSV_DELIMITER, self::CSV_ENCLOSURE))
                && !empty($csv_line)
            ) {
                // Combine columns with values
                $line = array_combine(array_keys($this->_columns), array_values($csv_line));

                // Empty record
                $record = array();

                $accepted = true;

                // Process columns
                foreach ($this->_columns as $column => $info) {
                    $method_name = "set{$column}ColumnValue";

                    if (method_exists($this, $method_name)) {
                        if (false === ($accepted = $this->{$method_name}($record, $line, $column, $info))) {
                            break;
                        }
                    }
                }

                // Process dataset
                if ($accepted && !empty($record)) {
                    $result = func_array2insert(
                        $this->_dataset[self::DATA_SOURCE],
                        $record, true
                    );
                }
            }
        }

        return $result;
    } // }}}

} // }}}
