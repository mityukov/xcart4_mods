<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Classes
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v5 (xcart_4_7_5), 2016-02-18 13:44:28, Products.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Catalog\Export;

/**
 * @see https://wiki.ecommerce.pb.com/display/TECH4/Catalog
 */
class Products extends \XCart\Modules\PitneyBowes\Catalog\File { // {{{

    const className = __CLASS__;

    protected function defineFeedName()
    { // {{{
        return self::PITNEY_BOWES_FEED_NAME_PRODUCTS;
    } // }}}

    protected function defineOperation()
    { // {{{
        return self::PITNEY_BOWES_OPERATION_UPDATE;
    } // }}}

    protected function defineColumns()
    { // {{{
        $columns = array(
            'MERCHANT_COMMODITY_REF_ID'     => array(),
            'COMMODITY_NAME_TITLE'          => array(),
            'SHORT_DESCRIPTION'             => array(),
            'LONG_DESCRIPTION'              => array(),
            'RETAILER_ID'                   => array(),
            'COMMODITY_URL'                 => array(),
            'RETAILER_UNIQUE_ID'            => array(),
            'PCH_CATEGORY_ID'               => array(),
            'RH_CATEGORY_ID'                => array(),
            'STANDARD_PRICE'                => array(),
            'WEIGHT_UNIT'                   => array(),
            'DISTANCE_UNIT'                 => array(),
            'COO'                           => array(),
            'IMAGE_URL'                     => array(),
            'PARENT_SKU'                    => array(),
            'CHILD_SKU'                     => array(),
            'PARCELS_PER_SKU'               => array(),
            'UPC'                           => array(),
            'UPC_CHECK_DIGIT'               => array(),
            'GTIN'                          => array(),
            'MPN'                           => array(),
            'ISBN'                          => array(),
            'BRAND'                         => array(),
            'MANUFACTURER'                  => array(),
            'MODEL_NUMBER'                  => array(),
            'MANUFACTURER_STOCK_NUMBER'     => array(),
            'COMMODITY_CONDITION'           => array(),
            'COMMODITY_HEIGHT'              => array(),
            'COMMODITY_WIDTH'               => array(),
            'COMMODITY_LENGTH'              => array(),
            'PACKAGE_WEIGHT'                => array(),
            'PACKAGE_HEIGHT'                => array(),
            'PACKAGE_WIDTH'                 => array(),
            'PACKAGE_LENGTH'                => array(),
            'HAZMAT'                        => array(),
            'ORMD'                          => array(),
            'CHEMICAL_INDICATOR'            => array(),
            'PESTICIDE_INDICATOR'           => array(),
            'AEROSOL_INDICATOR'             => array(),
            'RPPC_INDICATOR'                => array(),
            'BATTERY_TYPE'                  => array(),
            'NON_SPILLABLE_BATTERY'         => array(),
            'FUEL_RESTRICTION'              => array(),
            'SHIP_ALONE'                    => array(),
            'RH_CATEGORY_ID_PATH'           => array(),
            'RH_CATEGORY_NAME_PATH'         => array(),
            'RH_CATEGORY_URL_PATH'          => array(),
            'GPC'                           => array(),
            'COMMODITY_WEIGHT'              => array(),
            'HS_CODE'                       => array(),
            'CURRENCY'                      => array(),
        );

        return $columns;
    } // }}}

    protected function defineDataset()
    { // {{{
        global $sql_tbl;

        return array (
            self::DATA_SOURCE =>
                "$sql_tbl[products] AS PRODUCTS"
                . " INNER JOIN $sql_tbl[products_categories] AS CATEGORYDATA"
                    . " ON PRODUCTS.productid = CATEGORYDATA.productid"
                    . " AND CATEGORYDATA.main = '" . \XCPitneyBowesDefs::YES . "'"
                . " INNER JOIN $sql_tbl[products_lng_current] AS LANGUAGEDATA"
                    . " ON PRODUCTS.productid = LANGUAGEDATA.productid"
                . " INNER JOIN $sql_tbl[pricing] AS PRICEDATA"
                    . " ON PRODUCTS.productid = PRICEDATA.productid"
                    . " AND PRICEDATA.quantity = '1'"
                    . " AND PRICEDATA.variantid = '0'"
                . " INNER JOIN $sql_tbl[product_pb_exports] AS STATUS"
                    . " ON PRODUCTS.productid = STATUS.productid"
                    . " AND STATUS.exported = '" . \XCPitneyBowesDefs::DATASET_STATUS_PENDING . "'",

//            self::DATA_FILTER =>
//                "PRODUCTS.forsale = '" . \XCPitneyBowesDefs::YES . "'"
        );
    } // }}}

    // {{{ Getters and formatters

    /**
     * Get column value for 'MERCHANT_COMMODITY_REF_ID' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getMERCHANT_COMMODITY_REF_IDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['productid'];
    } // }}}

    /**
     * Get column value for 'COMMODITY_NAME_TITLE' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOMMODITY_NAME_TITLEColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['product'];
    } // }}}

    /**
     * Get column value for 'SHORT_DESCRIPTION' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getSHORT_DESCRIPTIONColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['descr'];
    } // }}}

    /**
     * Get column value for 'LONG_DESCRIPTION' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getLONG_DESCRIPTIONColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['fulldescr'];
    } // }}}

    /**
     * Get column value for 'RETAILER_ID' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getRETAILER_IDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'COMMODITY_URL' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOMMODITY_URLColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
        return func_get_resource_url('product', $dataset['productid']);
    } // }}}

    /**
     * Get column value for 'RETAILER_UNIQUE_ID' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getRETAILER_UNIQUE_IDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'PCH_CATEGORY_ID' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPCH_CATEGORY_IDColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'RH_CATEGORY_ID' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getRH_CATEGORY_IDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['categoryid'];
    } // }}}

    /**
     * Get column value for 'STANDARD_PRICE' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getSTANDARD_PRICEColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['price'];
    } // }}}

    /**
     * Get column value for 'WEIGHT_UNIT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getWEIGHT_UNITColumnValue(array $dataset, $name, $info)
    { // {{{
        return \XCPitneyBowesDefs::UNITS_WEIGHT;
    } // }}}

    /**
     * Get column value for 'DISTANCE_UNIT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getDISTANCE_UNITColumnValue(array $dataset, $name, $info)
    { // {{{
        return \XCPitneyBowesDefs::UNITS_DISTANCE;
    } // }}}

    /**
     * Get column value for 'COO' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOOColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'IMAGE_URL' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getIMAGE_URLColumnValue(array $dataset, $name, $info)
    { // {{{
        global $xcart_catalogs;
        return $xcart_catalogs['customer'] . "/image.php?type=P&id={$dataset['productid']}";
    } // }}}

    /**
     * Get column value for 'PARENT_SKU' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPARENT_SKUColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'CHILD_SKU' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCHILD_SKUColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'PARCELS_PER_SKU' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPARCELS_PER_SKUColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'UPC' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getUPCColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'UPC_CHECK_DIGIT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getUPC_CHECK_DIGITColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'GTIN' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getGTINColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'MPN' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getMPNColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'ISBN' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getISBNColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'BRAND' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getBRANDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'MANUFACTURER' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getMANUFACTURERColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'MODEL_NUMBER' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getMODEL_NUMBERColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'MANUFACTURER_STOCK_NUMBER' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getMANUFACTURER_STOCK_NUMBERColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'COMMODITY_CONDITION' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOMMODITY_CONDITIONColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'COMMODITY_HEIGHT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOMMODITY_HEIGHTColumnValue(array $dataset, $name, $info)
    { // {{{
        return func_dim_in_centimeters($dataset['height']);
    } // }}}

    /**
     * Get column value for 'COMMODITY_WIDTH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOMMODITY_WIDTHColumnValue(array $dataset, $name, $info)
    { // {{{
        return func_dim_in_centimeters($dataset['width']);
    } // }}}

    /**
     * Get column value for 'COMMODITY_LENGTH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOMMODITY_LENGTHColumnValue(array $dataset, $name, $info)
    { // {{{
        return func_dim_in_centimeters($dataset['length']);
    } // }}}

    /**
     * Get column value for 'PACKAGE_WEIGHT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPACKAGE_WEIGHTColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'PACKAGE_HEIGHT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPACKAGE_HEIGHTColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'PACKAGE_WIDTH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPACKAGE_WIDTHColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'PACKAGE_LENGTH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPACKAGE_LENGTHColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'HAZMAT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getHAZMATColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'ORMD' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getORMDColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'CHEMICAL_INDICATOR' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCHEMICAL_INDICATORColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'PESTICIDE_INDICATOR' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getPESTICIDE_INDICATORColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'AEROSOL_INDICATOR' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getAEROSOL_INDICATORColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'RPPC_INDICATOR' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getRPPC_INDICATORColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'BATTERY_TYPE' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getBATTERY_TYPEColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'NON_SPILLABLE_BATTERY' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getNON_SPILLABLE_BATTERYColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'FUEL_RESTRICTION' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getFUEL_RESTRICTIONColumnValue(array $dataset, $name, $info)
    { // {{{
        return $this->getExtrafieldValue($dataset['productid'], 0, $name);
    } // }}}

    /**
     * Get column value for 'SHIP_ALONE' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getSHIP_ALONEColumnValue(array $dataset, $name, $info)
    { // {{{
        return $dataset['separate_box'];
    } // }}}

    /**
     * Get column value for 'RH_CATEGORY_ID_PATH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getRH_CATEGORY_ID_PATHColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'RH_CATEGORY_NAME_PATH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getRH_CATEGORY_NAME_PATHColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'RH_CATEGORY_URL_PATH' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getRH_CATEGORY_URL_PATHColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'GPC' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getGPCColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'COMMODITY_WEIGHT' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCOMMODITY_WEIGHTColumnValue(array $dataset, $name, $info)
    { // {{{
        return func_weight_in_grams($dataset['weight']);
    } // }}}

    /**
     * Get column value for 'HS_CODE' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getHS_CODEColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    } // }}}

    /**
     * Get column value for 'CURRENCY' column
     *
     * @param array   $dataset Dataset
     * @param string  $name    Column name
     * @param integer $info    Column info
     *
     * @return string
     */
    protected function getCURRENCYColumnValue(array $dataset, $name, $info)
    { // {{{
        // TODO: to implement
    }

    // }}} Getters and formatters

    protected function getExtrafieldValue($productid, $variantid, $service_name)
    { // {{{
        global $active_modules, $sql_tbl;

        $result = '';

        if (!empty($active_modules['Extra_Fields'])) {
            $query = "SELECT EXTRAFIELDVALUES.value"
                . " FROM $sql_tbl[extra_fields] AS EXTRAFIELDS"
                . " INNER JOIN $sql_tbl[extra_field_values] AS EXTRAFIELDVALUES"
                    . " ON EXTRAFIELDS.fieldid = EXTRAFIELDVALUES.fieldid"
                    . " AND EXTRAFIELDS.service_name = '$service_name'"
                    . " AND EXTRAFIELDVALUES.productid = '$productid'"
                    . " AND EXTRAFIELDVALUES.variantid = '$variantid'";

            $result = func_query_first_cell($query);
        }

        return $result;
    } // }}}

    protected function generateUpdateSQL($dataCache)
    { // {{{
        // Generate SQL
        global $sql_tbl;
        $query = "REPLACE INTO $sql_tbl[product_pb_exports] VALUES ";
        // Get last record
        $lastElement = end($dataCache);
        // Loop through the cache elements
        foreach ($dataCache as $cacheEntry) {
            $query .= "($cacheEntry[productid], $cacheEntry[exported])";
            if ($lastElement !== $cacheEntry) {
                $query .= ", ";
            }
        }
        // Flush cached records
        return db_query($query);
    } // }}}

    /**
     * On export finish action
     * 
     * @return type
     */
    protected function onFinished()
    {
        $filename = $this->getRealPath();
        // Set step data
        $this->_step_progress = 0;
        $this->_step_name = self::STEP_NAME_FINALIZE;
        // Skip empty files
        if (filesize($filename) > 0) {
            // get file handler
            $handler = fopen($filename, 'r');

            // read header
            fgetcsv($handler, 0, self::CSV_DELIMITER, self::CSV_ENCLOSURE);

            $stepCounter = 1;
            $dataCache = array();

            $lineCounter = 1;
            $linesTotal = $this->getLinesCount($this->getRealPath());

            // read lines
            while ($data = fgetcsv($handler, 0, self::CSV_DELIMITER, self::CSV_ENCLOSURE)) {

                // Add element to cache
                $dataCache[] = array (
                    'productid' => $data[0],
                    'exported' => \XCPitneyBowesDefs::DATASET_STATUS_EXPORTED,
                );

                if ($stepCounter == self::RECORDS_PER_STEP) {
                    $this->generateUpdateSQL($dataCache);
                    // Clear buffer
                    $dataCache = array();
                    // Reset counter
                    $stepCounter = 0;
                }

                $this->_step_progress = ($lineCounter / $linesTotal) * 100;

                $stepCounter++;
                $lineCounter++;
            }

            if (!empty($dataCache)) {
                // Flush the remaining part
                $this->generateUpdateSQL($dataCache);
                // Free memory
                $dataCache = array();
            }

            fclose($handler);
        } else {
            // remove empty file
            unlink($filename);
        }
        // Finished
        $this->_step_progress = 100;
    }

} // }}}
