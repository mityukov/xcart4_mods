<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * Classes
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v4 (xcart_4_7_5), 2016-02-18 13:44:28, Processor.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Catalog\FileExchange;

/**
 * @see https://wiki.ecommerce.pb.com/display/TECH4/Pitney+Bowes+Ecommerce+-+Technical+Wiki
 */
class Processor { // {{{

    /**
     * @var boolean Libraries are included
     */
    protected $libraryLoaded = false;

    /**
     * @var object API config
     */
    protected $config;

    /**
     * Constructor
     */
    public function __construct()
    { // {{{
        $this->config = \XCPitneyBowesConfig::getInstance()->getConfigAsObject();
    } // }}}

    /**
     * Transfer generated catalog to PB server
     *
     * @return boolean
     */
    public function submitCatalog($files)
    { // {{{
        global $sql_tbl;

        $result = false;
        if (is_array($files) && count($files) > 0) {
            ob_start();

            $sftp = $this->loginToSFTP();

            if ($sftp) {
                $sftp->chdir('tmp');
                foreach ($files as $filename => $realpath) {
                    $sftp->put($filename, $realpath, \phpseclib\Net\SFTP::SOURCE_LOCAL_FILE);
                    $sftp->rename($filename, '../inbound/'. $filename);

                    $export = array (
                        'filename' => $filename,
                        'export_date' => XC_TIME,
                        'status' => \XCPitneyBowesDefs::EXPORT_STATUS_PENDING,
                        'errors' => '',
                    );

                    func_array2insert($sql_tbl['pb_exports'], $export);
                }

                $result = true;
            }

            $buffer = ob_get_contents();
            ob_end_clean();

            func_pitney_bowes_debug_log($buffer);
        }

        return $result;
    } // }}}

    /**
     * Check PB server for notifications
     *
     * @return boolean
     */
    public function checkNotifications()
    { // {{{
        ob_start();

        $sftp = $this->loginToSFTP();

        if ($sftp) {
            $sftp->chdir('outbound');
            $raw = $sftp->rawlist();
            $files = array_filter(
                array_map(
                    function ($file) {
                        return ($file['type'] == NET_SFTP_TYPE_REGULAR) ? $file : false;
                    },
                $raw)
            );

            $this->processNotifications($files, $sftp);
        }

        $buffer = ob_get_contents();
        ob_end_clean();

        func_pitney_bowes_debug_log($buffer);
    } // }}}

    protected function processNotifications($files, $sftp)
    { // {{{
        $responses = array_filter($files, function ($file) {
            return in_array(pathinfo($file['filename'], \PATHINFO_EXTENSION), array('ok', 'log', 'err'), true);
        });

        if (!empty($responses)) {
            $this->processExportResponses($responses, $sftp);
        }

        $eligibility = array_filter($files, function ($file) {
            return (pathinfo($file['filename'], \PATHINFO_EXTENSION) === 'csv' && strpos(pathinfo($file['filename'], \PATHINFO_FILENAME), 'commodity-eligibility') !== false);
        });

        if (!empty($eligibility)) {
            $this->processCommodityEligibility($eligibility, $sftp);
        }
    } // }}}

    protected function processExportResponses($files, $sftp)
    { // {{{
        global $sql_tbl;

        // get export responses
        $exports = func_query(
            "SELECT * FROM $sql_tbl[pb_exports] WHERE status = '"
                . \XCPitneyBowesDefs::EXPORT_STATUS_PENDING
            . "'");

        // Make sure we have files to check
        if (!empty($files) && !empty($exports)) {

            foreach ($files as $file) {
                foreach ($exports as $export) {

                    if (pathinfo($export['filename'], \PATHINFO_FILENAME) == pathinfo($file['filename'], \PATHINFO_FILENAME)) {

                        $originalStatus = $export['status'];

                        switch (pathinfo($file['filename'], \PATHINFO_EXTENSION)) {
                            case 'ok':
                                $export['status'] = \XCPitneyBowesDefs::EXPORT_STATUS_APPROVED;
                                break;

                            case 'err':
                                $export['status'] = \XCPitneyBowesDefs::EXPORT_STATUS_FAILED;
                                $export['errors'] = $sftp->get($file['filename']);
                                break;

                            default:
                                break;
                        }

                        if ($originalStatus != $export['status']) {
                            func_array2insert($sql_tbl['pb_exports'], $export, true);
                        }

                        $sftp->delete($file['filename']);
                        break;
                    }
                }
            }
        }
    } // }}}

    protected function processCommodityEligibility($files, $sftp)
    { // {{{
        $importer = new \XCPitneyBowesImporter();

        $importer->deleteAllFiles();

        $importDir = $importer->getImportDir();

        foreach ($files as $filename => $info) {
            $downloaded = $sftp->get($filename, $importDir . XC_DS . $filename);
            if ($downloaded) {
                $sftp->delete($filename);
            }
        }

        $importer->import();
    } // }}}

    protected function loginToSFTP()
    { // {{{
        $this->loadLibrary();

        $result = true;
        $sftpClient = new \phpseclib\Net\SFTP(str_replace('sftp://', '', $this->config->sftp_endpoint));
        if (!$sftpClient->login($this->config->sftp_username, $this->config->sftp_password)) {
            $result = false;
        } else {
            $sftpClient->chdir($this->config->sftp_catalog_directory);
        }

        return $result ? $sftpClient : $result;
    } // }}}

    /**
     * Load PHPSEC library
     */
    protected function loadLibrary()
    { // {{{
        global $xcart_dir;

        if (!$this->libraryLoaded) {
            require_once $xcart_dir . XC_DS . 'include' . XC_DS . 'lib' . XC_DS . 'phpseclib' . XC_DS . 'Autoloader.php';
            $this->libraryLoaded = true;
        }
    } // }}}

} // }}}
