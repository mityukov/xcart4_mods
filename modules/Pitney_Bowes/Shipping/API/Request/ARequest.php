<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * API
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, ARequest.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\API\Request;

use \XCart\Modules\PitneyBowes\Shipping\API;

/**
 * Abstract api request
 */
class ARequest
{
    const POST  = 'post';
    const GET   = 'get';
    const PUT   = 'put';

    const CONTENT_TYPE   = 'text/plain';

    /**
     * @var string
     */
    protected $url;

    /**
     * @var array
     */
    protected $inputData;

    /**
     * @var string
     */
    protected $httpType = self::POST;

    /**
     * @var \XCart\Modules\PitneyBowes\HTTP\Request
     */
    protected $request;

    /**
     * @var \PEAR2\HTTP\Request\Response
     */
    protected $response;

    /**
     * @var \XCart\Modules\PitneyBowes\Shipping\API\Mapper\IMapper Input mapper
     */
    protected $inputMapper;

    /**
     * @var \XCart\Modules\PitneyBowes\Shipping\API\Mapper\IMapper Output mapper
     */
    protected $outputMapper;

    /**
     * Construct
     */
    public function __construct($url_base, $inputData)
    {
        $this->url          = $url_base;
        $this->inputData    = $inputData;

        $this->request = new \XCart\Modules\PitneyBowes\HTTP\Request($this->url);
        $this->request->verb = $this->httpType;

        $this->setContentType(static::CONTENT_TYPE);
    }

    /**
     * Prepare parameters for request
     * 
     * @return mixed
     */
    protected function prepareParameters()
    {
        $result = null;

        if ($this->inputMapper) {
            $this->inputMapper->setInputData($this->inputData);
            $result = $this->inputMapper->getMapped();
        }

        return $result ?: $this->inputData;
    }

    /**
     * Set input mapper
     * 
     * @param \XCart\Modules\PitneyBowes\Shipping\API\Mapper\IMapper $mapper Mapper
     * 
     * @return void
     */
    public function setInputMapper(API\Mapper\IMapper $mapper)
    {
        $this->inputMapper = $mapper;
    }

    /**
     * Set output mapper
     * 
     * @param \XCart\Modules\PitneyBowes\Shipping\API\Mapper\IMapper $mapper Mapper
     * 
     * @return void
     */
    public function setOutputMapper(API\Mapper\IMapper $mapper)
    {
        $this->outputMapper = $mapper;
    }

    /**
     * Send request
     */
    public function sendRequest()
    {
        $this->request->body = $this->prepareParameters();

        $this->response = $this->request->sendRequest();
    }

    /**
     * Set auth
     *
     * @param string $type
     * @param string $value
     */
    public function setAuth($type, $value)
    {
        $this->request->setHeader(
            'Authorization',
            sprintf('%s %s', $type, $value));
    }

    /**
     * Set content type
     *
     * @param string $value
     */
    public function setContentType($value)
    {
        $this->request->setHeader('Content-Type', $value);
    }

    /**
     * Get response
     */
    public function getResponse()
    {
        $result = null;

        if ($this->outputMapper) {
            $this->outputMapper->setInputData($this->response);
            $this->outputMapper->setInputData($this->inputData, 'requested');
            $result = $this->outputMapper->getMapped();
        } else {
            $result = $this->getRawResponse();
        }

        return $result;
    }

    /**
     * Get raw request
     */
    public function getRawRequest()
    {
        return $this->prepareParameters();
    }

    /**
     * Get raw response
     */
    public function getRawResponse()
    {
        return $this->response->body;
    }
}
