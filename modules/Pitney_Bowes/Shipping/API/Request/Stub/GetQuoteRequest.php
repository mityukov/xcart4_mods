<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * API - DEV TOOL 4 TESTS
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, GetQuoteRequest.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\API\Request\Stub;

use \XCart\Modules\PitneyBowes\Shipping\API;

/**
 * https://wiki.ecommerce.pb.com/display/TECH4/Get+Quote
 */
class GetQuoteRequest extends API\Request\GetQuoteRequest
{
    protected $body = '{
          "quoteCurrency": "USD",
          "fxRatesUsed": [],
          "transactionId": "123456789",
          "totalCommodity": {
            "value": "8.99",
            "currency": "USD"
          },
          "totalTransportation": {
            "currency": "USD",
            "merchantShippingIdentifier": "STANDARD",
            "speed": "STANDARD",
            "shipping": {
              "value": "10.99",
              "currency": "USD"
            },
            "handling": {
              "value": "4.00",
              "currency": "USD"
            },
            "total": {
              "value": "14.99",
              "currency": "USD"
            },
            "minDays": 3,
            "maxDays": 5
          },
          "totalImportation": {
            "importationCurrency": "USD",
            "approximateDuty": {
              "value": "4.00",
              "currency": "USD"
            },
            "approximateTax": {
              "value": "5.00",
              "currency": "USD"
            },
            "approximateBrokerage": {
              "value": "1.00",
              "currency": "USD"
            },
            "total": {
              "value": "10.00",
              "currency": "USD"
            }
          },
          "total": {
            "value": "15.67",
            "currency": "USD"
          },
          "errors": []
    }';

    /**
     * 
     */
    public function getResponse()
    {
        $this->response = new \PEAR2\HTTP\Request\Response(
            array(),
            $this->body,
            array(),
            array()
        );

        return parent::getResponse();
    }
}
