<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * CreateInboundParcels - InputMapper
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Michael Bugrov
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v4 (xcart_4_7_5), 2016-02-18 13:44:28, InputMapper.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\Mapper\CreateInboundParcels;

use \XCart\Modules\PitneyBowes\Shipping\API;

/**
 * Get quote input mapper
 */
class InputMapper extends API\Mapper\JsonPostProcessedMapper
{

    /**
     * Is mapper able to map?
     * 
     * @return boolean
     */
    protected function isApplicable()
    {
        return $this->inputData
            && !empty($this->inputData['pbParcel'])
            && !empty($this->inputData['pbParcel']['orderid'])
            && !empty($this->inputData['pbParcel']['parcel_number'])
            && !empty($this->inputData['pbParcel']['items']);
    }
    /**
     * Perform actual mapping
     * 
     * @return mixed
     */
    protected function performMap()
    {
        $request = array();

        $request['merchantOrderNumber']         = $this->inputData['pbParcel']['orderid'];
        $request['parcelIdentificationNumber']  = $this->inputData['pbParcel']['parcel_number'];
        $request['inboundParcelCommodities']    = $this->getInboundParcelCommodities();
        $request['returnDetails']               = $this->getReturnDetails();

        $request['size'] = $this->getTotalSize($request['inboundParcelCommodities']);

        $request['shipperTrackingNumber']       = $this->inputData['pbParcel']['parcel_number'];

        return $request;
    }

    /**
     * Get total size
     * 
     * @param array $commodities List of commodities in parcel
     * 
     * @return array
     */
    protected function getTotalSize(array $commodities)
    {
        $weight = 0;

        foreach ($commodities as $commodity) {
            $weight += $commodity['quantity'] * $commodity['size']['weight'];
        }

        return array(
            'weight'        => $weight,
            'weightUnit'    => $this->getWeightUnit(),
        );
    }

    /**
     * Get commodities list
     * 
     * @return array
     */
    protected function getInboundParcelCommodities()
    {
        $commodities = array();

        foreach ($this->inputData['pbParcel']['items'] as $parcelItem) {
            $commodities[] = $this->getInboundParcelCommodity($parcelItem);
        }

        return $commodities;
    }

    /**
     * Get commodity
     * 
     * @param \XCart\Modules\PitneyBowes\PBParcel $parcelItem Parcel item
     * 
     * @return array
     */
    protected function getInboundParcelCommodity($parcelItem)
    {
        $item = array(
            'merchantComRefId'  => $parcelItem['productid'],
            'quantity'          => $parcelItem['quantity'],
            'size'  => array(
                'weight'        => $parcelItem['weight'],
                'weightUnit'    => $this->getWeightUnit(),
            )
        );

        if (!empty($parcelItem['coo'])) {
            $item['coo'] = $parcelItem['coo'];
        }

        return $item;
    }

    /**
     * Get return details
     * 
     * @return array
     */
    protected function getReturnDetails()
    {
        return array(
            'returnAddress'         => $this->getReturnAddress(),
            'contactInformation'    => $this->getReturnContactInformation(),
        );
    }

    /**
     * Get return details address
     * 
     * @return array
     */
    protected function getReturnAddress()
    {
        $address = $this->inputData['origAddress'];

        return array(
            'street1'           => $address['address'],
            'city'              => $address['city'],
            'provinceOrState'   => $address['state'],
            'country'           => $address['country'],
            'postalOrZipCode'   => $address['zipcode'],
        );
    }

    /**
     * Get return details address
     * 
     * @return array
     */
    protected function getReturnContactInformation()
    {
        $address = $this->inputData['origAddress'];

        $storeowner = array(
            'familyName'    => $address['lastname'],
            'givenName'     => $address['firstname'],
            'email'         => $address['email'],
            'phoneNumbers'  => array(
                array(
                    'number' => $address['phone'],
                    'type' => 'other'
                ),
            ),
        );

        return $storeowner;
    }

    /**
     * Get weight unit
     * 
     * @return string
     */
    protected function getWeightUnit()
    {
        return \XCPitneyBowesDefs::UNITS_WEIGHT;
    }
}
