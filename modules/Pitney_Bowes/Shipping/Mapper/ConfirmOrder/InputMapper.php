<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/* * ***************************************************************************\
  +-----------------------------------------------------------------------------+
  | X-Cart Software license agreement                                           |
  | Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>            |
  | All rights reserved.                                                        |
  +-----------------------------------------------------------------------------+
  | PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
  | FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
  | AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
  |                                                                             |
  | THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
  | SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
  | (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
  | FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
  | THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
  | LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
  | INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
  | (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
  | LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
  | NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
  | PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
  | THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
  | SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
  | GRANTED BY THIS AGREEMENT.                                                  |
  +-----------------------------------------------------------------------------+
  \**************************************************************************** */

/**
 * ConfirmOrder - InputMapper
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Qualiteam software Ltd <info@x-cart.com>
 * @copyright  Copyright (c) 2001-2016 Qualiteam software Ltd <info@x-cart.com>
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    3f68edb4b06a5e8ffa772f74caab24df25f6e212, v2 (xcart_4_7_5), 2016-02-18 13:44:28, InputMapper.php, aim
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace XCart\Modules\PitneyBowes\Shipping\Mapper\ConfirmOrder;

use \XCart\Modules\PitneyBowes\Shipping\API;

/**
 * Get quote input mapper
 */
class InputMapper extends API\Mapper\JsonPostProcessedMapper
{
    /**
     * Is mapper able to map?
     * 
     * @return boolean
     */
    protected function isApplicable()
    {
        return $this->inputData
            && !empty($this->inputData['ormus_transid'])
            && !empty($this->inputData['orderid']);
    }

    /**
     * Perform actual mapping
     * 
     * @return mixed
     */
    protected function performMap()
    {
        $confirm = array();

        $confirm['transactionId']           = $this->inputData['ormus_transid'];
        $confirm['merchantOrderNumber']     = $this->inputData['orderid'];
        $confirm['purchaser']               = $this->getPurchaser();
        $confirm['purchaserBillingAddress'] = $this->getPurchaserBillingAddress();

        return $confirm;
    }

    /**
     * Postprocess mapped data
     * 
     * @return array
     */
    protected function getPurchaser()
    {
        $address = $this->inputData['billAddress'] ?: $this->inputData['destAddress'];

        $purchaser = array(
            'familyName'    => $address['lastname'],
            'givenName'     => $address['firstname'],
            'email'         => $address['email'],
            'phoneNumbers'  => array(
                array(
                    'number' => $address['phone'],
                    'type' => 'other'
                ),
            ),
        );

        return $purchaser;
    }

    /**
     * Postprocess mapped data
     * 
     * @return array
     */
    protected function getPurchaserBillingAddress()
    {
        $bilAddress = $this->inputData['billAddress'];

        // TODO add error checking

        return array(
            'street1'           => $bilAddress['address'],
            'city'              => $bilAddress['city'],
            'provinceOrState'   => $bilAddress['state'],
            'country'           => $bilAddress['country'],
            'postalOrZipCode'   => $bilAddress['zipcode'],
        );
    }
}
