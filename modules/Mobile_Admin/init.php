<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Mobile Admin module initialization
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Mobile Admin
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id$
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if (!defined('XCART_START')) { header('Location: ../../'); die('Access denied'); }

if (defined('QUICK_START')) {
    return;
}

$_module_dir  = $xcart_dir . XC_DS . 'modules' . XC_DS . 'Mobile_Admin';

require_once $xcart_dir . XC_DS . 'include' . XC_DS . 'lib' . XC_DS . 'MobileAdmin' . XC_DS . 'Autoload.php';
require_once $_module_dir . XC_DS . 'classes' . XC_DS . 'MobileAdminCore.php';
require_once $_module_dir . XC_DS . 'classes' . XC_DS . 'MobileAdminDataHandler.php';

func_add_event_listener('order.new.notify', 'func_mobile_admin_new_order_notify');
func_add_event_listener('products.lowstock.notify', 'func_mobile_admin_low_stock_notify');

if (empty($config['Mobile_Admin']['mobile_admin_api_key'])) {
    $config['Mobile_Admin']['mobile_admin_api_key'] = func_mobile_admin_gen_new_key();

    $smarty->assign('config', $config);
    $mail_smarty->assign('config', $config);
}

/**
 * Call notification manager from customer browser via ajax
 */
x_session_register('callPushNotificationManager');
$mobileAdminNotificationManagerUrl = '';

if ($callPushNotificationManager) {
    $callPushNotificationManager = false;

    if (\MobileAdmin\MobileAdminCore::getInstance()->isAjaxQueryCallAllowed()) {
        $mobileAdminNotificationManagerUrl = \MobileAdmin\MobileAdminCore::getInstance()->getNotificationManagerUrl();
    }
}

$smarty->assign('mobileAdminNotificationManagerUrl', $mobileAdminNotificationManagerUrl);
