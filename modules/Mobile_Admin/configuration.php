<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * MobileAdmin configuration
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Modules
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id$
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if ( !defined('XCART_START') ) { header("Location: ../../"); die("Access denied"); }

if ('POST' == $REQUEST_METHOD) {
    if ('get_new_key' == $_POST['mobile_admin_mode']) {

        $new_key = func_mobile_admin_gen_new_key();

        $smarty->assign('mobile_admin_api_key', $new_key);

    } elseif ('unlink' == $_POST['mobile_admin_mode']) {

        $dev_id = (int)$_POST['mobile_admin_dev_id'];
        $result = db_query("DELETE FROM $sql_tbl[gcm_users] WHERE id = '$dev_id'");

    } elseif ('change_dev_status' == $_POST['mobile_admin_mode']) {

        $dev_id = (int)$_POST['mobile_admin_dev_id'];
        $var_name = 'device_status_' . $dev_id;
        $dev_new_status = $_POST[$var_name];

        func_array2update(
            'gcm_users',
            array(
                'status' => $dev_new_status
            ),
            "id = '$dev_id'"
        );
    }

    func_header_location('configuration.php?option=Mobile_Admin');
}

$mobile_admin_devices = func_mobile_admin_get_all_devices();
$subscription_state = func_mobile_admin_get_subscription_state($xcart_http_host);

$smarty->assign('mobile_admin_subscription', $subscription_state['subscribed']);
$smarty->assign('mobile_admin_subscription_till', $subscription_state['endDate']);
$smarty->assign('mobile_admin_connector_URL', \MobileAdmin\MobileAdminCore::getInstance()->getMobileAdminLogin());
$smarty->assign('mobile_admin_QR_url', $http_location . '/qr_generator.php?mkey=' . \MobileAdmin\MobileAdminCore::getInstance()->getApiKey());

$url_to_connector = urlencode(\MobileAdmin\MobileAdminCore::getInstance()->getConnectorUrl());

$smarty->assign('mobile_admin_connector_URL_encoded', $url_to_connector);
$smarty->assign('mobile_admin_api_key', \MobileAdmin\MobileAdminCore::getInstance()->getApiKey());
$smarty->assign('mobile_admin_devices', $mobile_admin_devices);
$smarty->assign('custom_admin_module_config_tpl_file', 'modules/Mobile_Admin/admin/configuration.tpl');
