<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Mobile Admin module initialization
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Mobile Admin
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id$
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace MobileAdmin;

define('MOBILE_ADMIN_QUEUE_RUN_TYPE', 1);
define('MOBILE_ADMIN_DEBUG', 0);

class MobileAdminCore extends Core
{
    const ENGINE_VERSION = 'XCart4';

    /**
     * Mobile Admin Core class constructor
     */
    protected function __construct()
    {
        parent::__construct();

        $this->configPathTranslation['General:currency_format'] = 'getConfigCurrencyFormat';
    }

    /**
     * Get store config value
     *
     * @param string $valuePath Value path as string separated by :
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function getConfigValue($valuePath)
    {
        global $config;

        if (
            isset($this->configPathTranslation[$valuePath])
            && method_exists($this, $this->configPathTranslation[$valuePath])
        ) {
            // Call if value has getter setup inside class
            $return = $this->{$this->configPathTranslation[$valuePath]}();
        } else {
            $path = explode(':', $valuePath);

            if (!empty($path)) {
                $return = $config;
                foreach ($path as $v) {
                    if (isset($return[$v])) {
                        $return = $return[$v];
                    } else {
                        throw new Exception(
                            Exception::CONFIG_VALUE_NOT_FOUND,
                            array(
                                'path' => $valuePath
                            )
                        );
                    }
                }
            } else {
                throw new Exception(
                    Exception::CONFIG_NO_TRANSLATION_FOR_PATH,
                    array(
                        'path' => $valuePath
                    )
                );
            }
        }

        return $return;
    }

    /**
     * Get HTTP host
     *
     * @return string
     */
    public function getHttpHost()
    {
        global $xcart_http_host;

        return $xcart_http_host;
    }

    /**
     * Get HTTPS host
     *
     * @return string
     */
    public function getHttpsHost()
    {
        global $xcart_https_host;

        return $xcart_https_host;
    }

    /**
     * Perform HTTP request
     *
     * @param string  $url      Request body
     * @param string  $body     Request URL
     * @param string  $method   Request method OPTIONAL
     * @param array   $headers  Request headers OPTIONAL
     * @param integer $timeout  Request timeout OPTIONAL
     * @param boolean $skipCert Skip certificate check OPTIONAL
     * @param array   $options  Additional connection options
     *
     * @return array
     */
    public function httpRequest($url, $body, $method = 'GET', $headers = array(), $timeout = 60, $skipCert = false, $options = array())
    {
        x_load('http');

        return func_https_request(
            $method,
            $url,
            $body,
            NULL,
            NULL,
            'application/json',
            NULL,
            NULL,
            NULL,
            $headers
        );
    }

    /**
     * Get the language variable value / perform the phrase translation
     *
     * @param string $name Language variable name
     * @param array $arguments Substitute array OPTIONAL
     *
     * @return string
     */
    public function translate($name, array $arguments = array())
    {
        $return = func_get_langvar_by_name($name, $arguments, false, true);

        return empty($return) ? $name . (empty($arguments) ? '' : ': ' . implode(';', $arguments)) : $return;
    }

    /**
     * Add log file to the system log
     *
     * @param string $name Log name
     * @param string $data Log data
     *
     * @return void
     */
    public function addLogFile($name, $data)
    {
        x_log_add($name, $data);
    }

    /**
     * Get server time in Unix timestamp
     *
     * @return integer
     */
    public function getTime()
    {
        return XC_TIME;
    }

    /**
     * Get time shift in seconds
     *
     * @return integer
     */
    public function getTimeShift()
    {
        global $config;

        return $config['Appearance']['timezone_offset'];
    }

    /**
     * Get allowed order statuses
     *
     * @return array
     */
    public function getAllowedOrderStatuses()
    {
        return array(
            'status'            => array(
                array(
                    'code'  => 'I',
                    'name'  => func_get_langvar_by_name('lbl_not_finished', NULL, false, true),
                    'avail' => TRUE
                ),
                array(
                    'code'  => 'Q',
                    'name'  => func_get_langvar_by_name('lbl_queued', NULL, false, true),
                    'avail' => TRUE
                ),
                array(
                    'code'  => 'A',
                    'name'  => func_get_langvar_by_name('lbl_pre_authorized', NULL, false, true),
                    'avail' => FALSE
                ),
                array(
                    'code'  => 'P',
                    'name'  => func_get_langvar_by_name('lbl_processed', NULL, false, true),
                    'avail' => TRUE
                ),
                array(
                    'code'  => 'B',
                    'name'  => func_get_langvar_by_name('lbl_backordered', NULL, false, true),
                    'avail' => TRUE
                ),
                array(
                    'code'  => 'D',
                    'name'  => func_get_langvar_by_name('lbl_declined', NULL, false, true),
                    'avail' => TRUE
                ),
                array(
                    'code'  => 'F',
                    'name'  => func_get_langvar_by_name('lbl_failed', NULL, false, true),
                    'avail' => TRUE
                ),
                array(
                    'code'  => 'C',
                    'name'  => func_get_langvar_by_name('lbl_complete', NULL, false, true),
                    'avail' => TRUE
                ),
                array(
                    'code'  => 'X',
                    'name'  => func_get_langvar_by_name('lbl_xpc_order', NULL, false, true),
                    'avail' => TRUE
                )
            ),
            'payment_status'    => array(),
            'fulfilment_status' => array()
        );
    }

    /**
     * Get current store engine
     *
     * @return string
     */
    protected function getConfigStoreEngine()
    {
        return self::ENGINE_VERSION;
    }

    /**
     * Get current store version
     *
     * @return string
     */
    protected function getConfigStoreVersion()
    {
        global $sql_tbl;

        return func_query_first_cell("SELECT value FROM $sql_tbl[config] WHERE name='version'");
    }

    /**
     * Get list of order statuses used by the store
     *
     * @return array
     */
    protected function getConfigOrderStatuses()
    {
        return $this->getAllowedOrderStatuses();
    }

    /**
     * Get currency format with the current currency symbol
     *
     * @return string
     */
    public function getConfigCurrencyFormat()
    {
        global $config;

        return str_replace('$', $config['General']['currency_symbol'], $config['General']['currency_format']);
    }

    /**
     * Initialize Mobile Admin config
     *
     * @return \MobileAdmin\Config
     */
    protected function initConfig()
    {
        global $config, $https_location, $var_dirs;

        $apiKey = $config['Mobile_Admin']['mobile_admin_api_key'];

        $connectorUrl = $https_location . '/mobile_admin_api.php?key=' . $apiKey;

        \MobileAdmin\Config::getInstance()->setData(
            array(
                'apiKey'                => $apiKey,
                'connectorUrl'          => $connectorUrl,
                'connectionTimeout'     => 10,
                'directorySeparator'    => XC_DS,
                'qrLibCacheDir'         => $var_dirs['var'] . XC_DS . 'mobile_admin' . XC_DS . 'qrcode' . XC_DS . 'cache' . XC_DS,
                'qrLibLogDir'           => $var_dirs['var'] . XC_DS . 'mobile_admin' . XC_DS . 'qrcode' . XC_DS . 'log' . XC_DS,
                'queueManagerLockFile'  => $var_dirs['var'] . XC_DS . 'mobile_admin' . XC_DS . 'nqmanager'
            )
        );
    }

    /**
     * Initiate Mobile Admin data handler
     *
     * @return \MobileAdmin\DataHandler
     */
    protected function initDataHandler()
    {
        return new MobileAdminDataHandler();
    }

    public function callNotificationManager()
    {
        global $callPushNotificationManager;

        x_session_register('callPushNotificationManager');

        $callPushNotificationManager = true;

        return parent::callNotificationManager();
    }


}
