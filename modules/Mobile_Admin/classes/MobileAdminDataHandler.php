<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Mobile Admin module initialization
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Mobile Admin
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id$
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

namespace MobileAdmin;

class MobileAdminDataHandler extends DataHandler
{
    /**
     * Register device within shop
     *
     * @param \MobileAdmin\Containers\DeviceDataContainer $device Device data
     *
     * @return boolean
     */
    public function registerDevice($device)
    {
        $return = func_array2insert(
            'gcm_users',
            array(
                'gcm_regid'     => addslashes($device->regid),
                'manufacturer'  => addslashes($device->manufacturer),
                'model'         => addslashes($device->model),
                'serial'        => addslashes($device->serial),
                'os_type'       => addslashes($device->os_type),
                'os_version'    => addslashes($device->os_version),
                'imsi'          => addslashes($device->imsi),
                'status'        => 'Y'
            )
        );

        return (false === $return) ? false : true;
    }

    /**
     * Get device info by registration ID
     *
     * @param string $regId Registration ID
     *
     * @return mixed
     */
    public function getDevice($regId)
    {
        global $sql_tbl;

        $regId = addslashes($regId);

        $result = func_query_first("SELECT * FROM $sql_tbl[gcm_users] WHERE gcm_regid = '$regId'");

        if (!empty($result)) {
            $result['regid']    = $result['gcm_regid'];
            $result['date']     = $result['created_at'];
        }

        return (empty($result)) ? NULL : new Containers\DeviceDataContainer($result);
    }

    /**
     * Delete registered device
     *
     * @param string $regId Registration ID
     *
     * @return boolean
     */
    public function deleteDevice($regId)
    {
        global $sql_tbl;

        $regId = addslashes($regId);

        $return = func_query("DELETE FROM $sql_tbl[gcm_users] WHERE gcm_regid = '$regId'");

        return $return;
    }

    /**
     * Get all registered devices
     *
     * @return \MobileAdmin\Containers\DeviceDataContainer[]
     */
    public function getAllDevices()
    {
        global $sql_tbl;

        $result = func_query("SELECT DISTINCT * FROM $sql_tbl[gcm_users]");

        $return = array();

        if (!empty($result)) {
            foreach ($result as $device) {
                $device['date'] = $device['created_at'];
                $device['regid'] = $device['gcm_regid'];

                $return[] = new Containers\DeviceDataContainer($device);
            }
        }

        return $return;
    }

    /**
     * Get all devices available for receiving messages
     *
     * @return \MobileAdmin\Containers\DeviceDataContainer[]
     */
    public function getAllAvailableDevices()
    {
        global $sql_tbl;

        $result = func_query("SELECT DISTINCT * FROM $sql_tbl[gcm_users] WHERE status = 'Y'");

        $return = array();

        if (!empty($result)) {
            foreach ($result as $device) {
                $device['date'] = $device['created_at'];
                $device['regid'] = $device['gcm_regid'];

                $return[] = new Containers\DeviceDataContainer($device);
            }
        }

        return $return;
    }

    /**
     * Get all registered IDs
     *
     * @return array
     */
    public function getAllRegIds()
    {
        $devices = $this->getAllDevices();

        $return = array();

        if (!empty($devices)) {
            foreach ($devices as $device) {
                $return[] = $device->regid;
            }
        }

        return $return;
    }

    /**
     * Get dashboard data
     *
     * @return \MobileAdmin\Containers\DashboardDataContainer
     */
    public function getDashboardData()
    {
        global $sql_tbl, $active_modules;

        $dateRange = Time::getInstance()->getDateConditionAsTimestamp(Time::SEARCH_TIME_THIS_DAY);

        $dateCondition = "AND o.date >= '$dateRange[from]' AND o.date <= '$dateRange[to]'";
        $reviewDateCondition = "AND datetime >= '$dateRange[to]' AND datetime <= '$dateRange[to]'";

        $todaySales = func_query_first_cell("
            SELECT SUM(o.total)
            FROM $sql_tbl[orders] o
            WHERE (status = 'P' OR status = 'C') $dateCondition
        ");

        $todaySales = price_format($todaySales);

        $low_stock = func_query_first_cell("
            SELECT COUNT(*)
            FROM $sql_tbl[products]
            WHERE avail <= low_avail_limit AND forsale != 'N'
        ");

        $todayVisitors = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[customers] WHERE last_login >= '$dateRange[from]'");

        $todaySold = func_query_first_cell("
            SELECT SUM(od.amount)
            FROM $sql_tbl[orders] o
            JOIN $sql_tbl[order_details] od ON od.orderid = o.orderid
            WHERE (o.status = 'P' OR o.status = 'C') $dateCondition
        ");

        if (!empty($active_modules['Advanced_Customer_Reviews'])) {
            $todayReviews = func_query_first_cell("
                SELECT COUNT(*)
                FROM $sql_tbl[product_reviews]
                WHERE 1 $reviewDateCondition
          ");
        } else {
            $todayReviews = Message::getInstance()->translate(Message::REVIEWS_NOT_AVAIL);
        }

        $todayOrders = $this->getOrdersList(0, 3, $dateRange);

        return new Containers\DashboardDataContainer(
            array(
                'today_sales'           => $todaySales,
                'low_stock'             => $low_stock,
                'today_visitors'        => $todayVisitors,
                'today_sold'            => (int)$todaySold,
                'reviews_today'         => $todayReviews,
                'today_orders'          => $todayOrders,
                'today_orders_count'    => count($todayOrders)
            )
        );
    }

    /**
     * Get orders list
     *
     * @param \MobileAdmin\Containers\ActionData\LastOrdersInput $input Input data
     *
     * @return \MobileAdmin\Containers\OrdersListItemContainer[]
     */
    public function getLastOrdersList($input)
    {
        return $this->getOrdersList(
            $input->from,
            $input->size,
            $input->date,
            $input->status,
            $input->search
        );
    }

    /**
     * Get order info
     *
     * @param \MobileAdmin\Containers\ActionData\OrderInfoInput $input Input data
     *
     * @return \MobileAdmin\Containers\OrderDataContainer
     */
    public function getOrderInfo($input)
    {
        $orderInfo = $this->getOrderById($input->id);

        $order = $orderInfo['order'];
        $order['details'] = $orderInfo['products'];

        $orderItems = array();

        foreach ($order['details'] as $key => $orderItem) {

            if (
                isset($order['details'][$key]['product_options'])
                && is_array($order['details'][$key]['product_options'])
            ) {
                $options = array();

                $orderItem['product_options'] = array_values($orderItem['product_options']);

                if (!empty($orderItem['product_options'])) {
                    foreach ($orderItem['product_options'] as $option) {
                        $options[] = new Containers\ProductOptionContainer($option);
                    }
                }

                $orderItem['product_options'] = new Containers\ArrayContainer($options);

            } else {
                $orderItem['product_options'] = array();
            }

            $orderItems[] = new Containers\OrderItemContainer($orderItem);
        }

        $order['details'] = new Containers\ArrayContainer($orderItems);

        // Order was placed anonymously so we try to identify the user by order id instead
        if (0 == $order['userid']) {
            $order['userid'] = 'A' . $order['orderid'];
        }

        return new Containers\OrderDataContainer($order);
    }

    /**
     * Set tracking numbers
     *
     * @param \MobileAdmin\Containers\ActionData\ChangeTrackingInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     *
     * @throws \MobileAdmin\Exception
     */
    public function setTrackingNumbers($input)
    {
        if ($input->tracking_number) {
            $result = func_array2update(
                'orders',
                array(
                    'tracking' => $input->tracking_number
                ),
                "orderid = '" . $input->order_id . "'"
            );
        } else {
            throw new Exception(Exception::TRACKING_EMPTY);
        }

        return new Containers\UploadActionResponseContainer(
            array(
                'upload_status' => (string)$result,
                'upload_type'   => 'update',
                'upload_data'   => 'tracking_number',
                'id'            => $input->order_id
            )
        );
    }

    /**
     * Set order status
     *
     * @param \MobileAdmin\Containers\ActionData\ChangeStatusInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     *
     * @throws \MobileAdmin\Exception
     */
    public function setOrderStatus($input)
    {
        global $active_modules;

        x_load('order');

        if (
            !empty($input->pph_order_details)
            && !empty($active_modules['PayPal_Here'])
        ) {
            $pph_order_details = $input->pph_order_details;

            $res = func_mobile_admin_process_pph_payment($input->order_id, $pph_order_details);

            $input->order_id = $res['id'];

            if (empty($res)) {
                throw new Exception(Exception::STATUS_EMPTY);
            } else {
                func_change_order_status($input->order_id, $res['status'], $res['adv_info']);
            }

        } elseif ($input->status) {
            func_change_order_status($input->order_id, $input->status);
        } else {
            throw new Exception(Exception::STATUS_EMPTY);
        }

        return new Containers\UploadActionResponseContainer(
            array(
                'upload_status' => (string)TRUE,
                'upload_type'   => 'update',
                'upload_data'   => 'status',
                'id'            => $input->order_id
            )
        );
    }

    /**
     * Set product availability
     *
     * @param \MobileAdmin\Containers\ActionData\ChangeAvailableInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    public function setProductAvailability($input)
    {
        $productInfo = $this->getProductData($input->id);

        $result = '';

        if (!empty($productInfo)) {
            $result = func_array2update(
                'products',
                array(
                    'forsale' => ($input->available == 1) ? 'Y' : 'N'
                ),
                "productid = '" . $input->product_id . "'"
            );
        }

        return new Containers\UploadActionResponseContainer(
            array(
                'upload_status' => (string) $result,
                'upload_type'   => 'update',
                'upload_data'   => 'availability',
                'id'            => $input->product_id
            )
        );
    }

    /**
     * Get products list
     *
     * @param \MobileAdmin\Containers\ActionData\ProductsInput $input Input data
     *
     * @return \MobileAdmin\Containers\ProductsListItemContainer[]
     */
    public function getProductsList($input)
    {
        global $sql_tbl;

        $lowStockCondition = $input->low_stock ? "AND avail <= low_avail_limit AND p.forsale != 'N'" : '';
        $searchCondition = $input->search ? "AND (lng.product LIKE '%"
            . $input->search . "%' OR p.productcode LIKE '%" . $input->search . "%')" : '';

        $from = $input->from;
        $size = $input->size;

        $products = func_query("
            SELECT p.productid, p.productcode, pp.price, p.avail, lng.product
            FROM $sql_tbl[products] p
            JOIN $sql_tbl[products_lng_current] lng ON lng.productid = p.productid
            LEFT JOIN $sql_tbl[pricing] pp ON pp.productid = p.productid
            WHERE 1 $lowStockCondition $searchCondition AND pp.variantid = 0
            LIMIT $from, $size
        ");

        $return = array();

        if (!empty($products)) {
            foreach ($products as $product) {
                $return[] = new Containers\ProductsListItemContainer($product);
            }
        }

        return $return;
    }

    /**
     * Get product info by product ID
     *
     * @param \MobileAdmin\Containers\ActionData\ProductInfoInput $input Input data
     *
     * @return \MobileAdmin\Containers\ProductInfoContainer
     */
    public function getProductInfo($input)
    {
        global $current_area;

        $current_area = 'A';

        $productInfo = $this->getProductData($input->id);

        $productInfo['image_url'] = $this->parseProductImagePath($productInfo['image_url']);

        $productInfo['url'] = $productInfo['image_url'];

        if (!empty($productInfo['images'])) {
            $imagesData = array();

            foreach ($productInfo['images'] as $type => $image) {
                $imagesData[$type] = array();

                $imagesData[$type] = new Containers\ProductImageContainer($image);
            }

            $productInfo['images'] = new Containers\ArrayContainer($imagesData);
        }

        $tmpVariants = func_get_product_variants($input->id);

        if (
            !empty($tmpVariants)
            && is_array($tmpVariants)
        ) {
            foreach ($tmpVariants as $key => $tmpVariant) {
                $tmpVariant['variant_id'] = $key;

                $tmpVariant['image_path_W'] = $this->parseProductImagePath(
                    $tmpVariant['image_path_W'],
                    $productInfo['image_url']
                );

                $options = array();
                foreach ($tmpVariant['options'] as $oid => $option) {
                    $options[$oid] = new Containers\ProductOptionContainer($option);
                }

                $tmpVariant['options'] = new Containers\ArrayContainer($options);
                $tmpVariant['options_arr'] = new Containers\ArrayContainer(array_values($options));

                $tmpVariants[$key] = new Containers\ProductVariantContainer($tmpVariant);
            }
        } else {
            $tmpVariants = array();
        }

        $productInfo['variants'] = new Containers\ArrayContainer(array_values($tmpVariants));

        $tmp_opts = func_get_product_options_data($input->id, NULL);

        if (!empty($tmp_opts)) {
            $options = array();
            foreach ($tmp_opts as $key => $tmpOption) {
                $tmpOption['option_id'] = $key;

                $options[] = new Containers\ProductOptionContainer($tmpOption);
            }

            $productInfo['options'] = new Containers\ArrayContainer($options);
        }

        return new Containers\ProductInfoContainer($productInfo);
    }

    /**
     * Update product price
     *
     * @param \MobileAdmin\Containers\ActionData\UpdateProductPriceInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     */
    public function updateProductPrice($input)
    {
        $productInfo = $this->getProductData($input->id);

        $result = '';

        if (!empty($productInfo)) {
            $result = FALSE;

            $isVariant = !empty($active_modules['Product_Options']) && func_mobadmin_product_is_variant($input->id);

            if (!$isVariant) {
                $result = func_array2update(
                    'pricing',
                    array(
                        'price' => $input->price
                    ),
                    "productid='" . $input->id . "' AND quantity='1' AND membershipid = '0' AND variantid = '" . $input->variantid . "'"
                );
            }
        }

        return new Containers\UploadActionResponseContainer(
            array(
                'upload_status' => (string) $result,
                'upload_type'   => 'update',
                'upload_data'   => 'product',
                'id'            => $input->id
            )
        );
    }

    /**
     * Get reviews
     *
     * @param \MobileAdmin\Containers\ActionData\ReviewsInput $input Input data
     *
     * @return \MobileAdmin\Containers\ReviewContainer[]
     */
    public function getReviews($input)
    {
        global $sql_tbl, $active_modules;

        $from = (int) $input->from;
        $size = (int) $input->size;

        if (!empty($active_modules['Advanced_Customer_Reviews'])) {
            $query = "
                SELECT r.review_id, r.productid, r.email, r.message, lng.product
                FROM $sql_tbl[product_reviews] r
                JOIN $sql_tbl[products_lng_current] lng ON r.productid = lng.productid
                ORDER BY r.datetime DESC
                LIMIT $from, $size";

        } else {
            $query = "
                SELECT r.review_id, r.productid, r.email, r.message, lng.product
                FROM $sql_tbl[product_reviews] r
                JOIN $sql_tbl[products_lng_current] lng ON r.productid = lng.productid
                ORDER BY r.review_id DESC
                LIMIT $from, $size";
        }

        $reviews = func_query($query);

        $return = array();

        if (!empty($reviews)) {
            foreach ($reviews as $review) {
                $return[] = new Containers\ReviewContainer($review);
            }
        }

        return $return;
    }

    /**
     * Delete review
     *
     * @param \MobileAdmin\Containers\ActionData\DeleteReviewInput $input Input data
     *
     * @return \MobileAdmin\Containers\UploadActionResponseContainer
     *
     * @throws \MobileAdmin\Exception
     */
    public function deleteReview($input)
    {
        global $sql_tbl;

        $found = func_query_first_cell("SELECT review_id FROM $sql_tbl[product_reviews] WHERE review_id = '" . $input->id . "'");

        if ($found) {
            $result = db_query("DELETE FROM $sql_tbl[product_reviews] WHERE review_id = '" . $input->id . "'");
        } else {
            throw new Exception(Exception::REVIEW_NOT_FOUND);
        }

        return new Containers\UploadActionResponseContainer(
            array(
                'upload_status' => (string)$result,
                'upload_type'   => 'delete',
                'upload_data'   => 'review',
                'id'            => $input->id
            )
        );
    }

    /**
     * Get users list
     *
     * @param \MobileAdmin\Containers\ActionData\UsersInput $input Input Data
     *
     * @return \MobileAdmin\Containers\UsersListItemContainer[]
     */
    public function getUsers($input)
    {
        global $sql_tbl;

        $from   = $input->from;
        $size   = $input->size;
        $search = $input->search;

        $substring_query = $search ? "AND (c.username LIKE '%$search%' OR c.firstname LIKE '%$search%' OR "
            . "c.lastname LIKE '%$search%' OR c.login LIKE '%$search%')" : '';

        $users = func_query("
            SELECT c.id, c.login, c.username, c.usertype, c.title, c.firstname, c.lastname, c.email, c.last_login, ab.phone, SUM(DISTINCT o.orderid) AS total_orders
            FROM $sql_tbl[customers] c
            JOIN $sql_tbl[address_book] ab ON ab.userid = c.id
            LEFT JOIN $sql_tbl[orders] o ON o.userid = c.id
            WHERE 1 $substring_query
            GROUP BY c.id
            ORDER BY c.last_login DESC
            LIMIT $from, $size
        ");

        $return = array();

        foreach ($users as $k => $u) {
            $u['last_login'] = (0 == $u['last_login']) ?
                Message::getInstance()->translate(Message::NOT_AVAILABLE)
                : gmdate('M-d-Y', Time::getInstance()->convertToUserTime($u['last_login']));
            $u['total_orders'] = (int) $u['total_orders'];

            $return[] = new Containers\UsersListItemContainer($u);
        }

        return $return;
    }

    /**
     * Get user info
     *
     * @param \MobileAdmin\Containers\ActionData\UserInfoInput $input Input data
     *
     * @return \MobileAdmin\Containers\UserInfoContainer
     *
     * @throws \MobileAdmin\Exception
     */
    public function getUserInfo($input)
    {
        if ($this->checkIfAnonymousId($input->id)) {
            // Fetch anonymous user info from order
            $userInfo = $this->getUserInfoFromOrder($this->getOrderIdFromAnonymousId($input->id));
        } else {
            $id = (int) $input->id;

            $userInfo = $this->getUserData($id);
        }

        if (!$userInfo) {
            throw new Exception(Exception::CUSTOMER_NOT_FOUND);
        }

        $keys = array_flip(
            array(
                'id',
                'login',
                'username',
                'usertype',
                'title',
                'firstname',
                'lastname',
                'company',
                'email',
                'userid',
                'address',
                'last_login'
            )
        );

        $userInfo = array_intersect_key($userInfo, $keys);

        $addressBook = array();

        if (!empty($userInfo['address'])) {
            if (
                isset($userInfo['address']['S'])
                && !empty($userInfo['address']['S'])
            ) {
                $addressBook['S'] = new Containers\AddressBookItemContainer($userInfo['address']['S']);
            }

            if (
                isset($userInfo['address']['B'])
                && !empty($userInfo['address']['B'])
            ) {
                $addressBook['B'] = new Containers\AddressBookItemContainer($userInfo['address']['S']);
            }
        }

        foreach (array('S', 'B') as $t) {
            if (!isset($addressBook[$t])) {
                $addressBook[$t] = new Containers\AddressBookItemContainer(array());
            }
        }

        $userInfo['address'] = new \MobileAdmin\Containers\ArrayContainer($addressBook);

        return new Containers\UserInfoContainer($userInfo);
    }

    /**
     * Set user status
     *
     * @param \MobileAdmin\Containers\ActionData\UserStatusInput $input Input data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     *
     * @throws \MobileAdmin\Exception
     */
    public function setUserStatus($input)
    {
        $userInfo = $this->getUserData($input->id);

        if ($userInfo) {

            $result = func_update_user_status($userInfo, $input->status);

            if (!$result) {
                throw new Exception(Exception::CANT_CHANGE_CUSTOMER_STATUS);
            }

        } else {
            throw new Exception(Exception::CUSTOMER_NOT_FOUND);
        }

        return new \MobileAdmin\Containers\APIResponseContainer(
            array(
                'status'    => \MobileAdmin\Controller::STATUS_SUCCESS,
                'message'   => \MobileAdmin\Message::getInstance()->translate(\MobileAdmin\Message::SUCCESS),
            )
        );
    }

    /**
     * Get orders list for user
     *
     * @param \MobileAdmin\Containers\ActionData\UserOrdersInput $input Input data
     *
     * @return \MobileAdmin\Containers\OrdersListItemContainer[]
     */
    public function getUserOrdersList($input)
    {
        return $this->getOrdersList(
            $input->from,
            $input->size,
            array(),
            '',
            '',
            $input->user_id
        );
    }

    /**
     * Generate discount coupon
     *
     * @param \MobileAdmin\Containers\ActionData\GenerateDiscountCouponInput $input Input data
     *
     * @return \MobileAdmin\Containers\APIResponseContainer
     *
     * @throws \MobileAdmin\Exception
     */
    public function generateDiscountCoupon($input)
    {
        global $sql_tbl, $active_modules;

        if (empty($active_modules['Discount_Coupons'])) {
            throw new Exception(Exception::DC_MODULE_NOT_FOUND);
        }

        $discount       = (double) $input->discount;
        $cart_minimum   = (double) $input->cart_minimum;
        $expires        = (int) $input->expires;
        $usages         = (int) $input->usages;
        $per_user       = (TRUE == (bool) $input->per_user) ? 'Y' : 'N';
        $status         = (in_array($input ->status, array('A', 'D', 'U'))) ? $input->status : 'A';

        $apply_category_once = $apply_product_once = 'N';

        if (
            empty($code)
            || preg_match("/" . func_coupon_validation_regexp() . "/", $code)
        ) {
            throw new Exception(Exception::DC_INVALID_CODE);
        }

        if (0 < func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[discount_coupons] WHERE coupon='$code'")) {
            throw new Exception(Exception::DC_CODE_EXISTS);
        }

        if (
            (
                0 >= $discount
                && 'free_ship' != $input->type
            )
            || (
                100 < $discount
                && 'percent' == $input->type
            )
        ) {
            throw new \MobileAdmin\Exception(\MobileAdmin\Exception::DC_DISCOUNT_ERROR);
        }

        $coupon_data = array(
            'coupon'              => $code,
            'discount'            => $discount,
            'coupon_type'         => $input->type,
            'minimum'             => $cart_minimum,
            'times'               => $usages,
            'per_user'            => $per_user,
            'expire'              => $expires,
            'status'              => $status,
//                'provider'            => $logged_userid,
//                'productid'           => $productid_new,
//                'categoryid'          => $categoryid_new,
//                'recursive'           => $recursive,
            'apply_category_once' => $apply_category_once,
            'apply_product_once'  => $apply_product_once,
        );

        func_array2insert('discount_coupons', $coupon_data);

        return new \MobileAdmin\Containers\APIResponseContainer(
            array(
            'status'    => \MobileAdmin\Controller::STATUS_SUCCESS,
            'message'   => \MobileAdmin\Message::getInstance()->translate(\MobileAdmin\Message::DC_CREATE_SUCCESS),
            'data'      => $input->code
            )
        );
    }

    /**
     * Add notification to queue
     *
     * @param string $message Notification message
     *
     * @return boolean
     */
    public function addNotificationToQueue($message)
    {
        global $sql_tbl;

        $return = false;

        $md5 = md5($message);

        $count = func_query_first_cell("SELECT COUNT(*) FROM $sql_tbl[notification_queue] WHERE hash = '$md5' AND status = '"
            . \MobileAdmin\PushNotification::QUEUE_STATUS_QUEUED . "'");

        if (0 == $count) {
            db_query("LOCK TABLES $sql_tbl[notification_queue] WRITE");

            $return = func_array2insert(
                'notification_queue',
                array(
                    'hash'      => $md5,
                    'message'   => addslashes($message),
                    'status'    => \MobileAdmin\PushNotification::QUEUE_STATUS_QUEUED,
                    'date'      => MobileAdminCore::getInstance()->getTime()
                )
            );

            db_query('UNLOCK TABLES');
        }

        return (false === $return) ? false : true;
    }

    /**
     * Get notification from queue
     *
     * @return \MobileAdmin\Containers\PushNotifications\QueuedNotificationContainer
     */
    public function popQueuedNotification()
    {
        global $sql_tbl;

        $return = null;

        db_query("LOCK TABLES $sql_tbl[notification_queue] WRITE");

        $notification = func_query_first("SELECT * FROM $sql_tbl[notification_queue] WHERE status='"
            . \MobileAdmin\PushNotification::QUEUE_STATUS_QUEUED . "' ORDER BY date ASC");

        if (!empty($notification)) {
            $return = new \MobileAdmin\Containers\PushNotifications\QueuedNotificationContainer($notification);

            func_array2update(
                'notification_queue',
                array(
                    'status' => \MobileAdmin\PushNotification::QUEUE_STATUS_SENDING
                ),
                "id = '$notification[id]'"
            );
        }

        db_query('UNLOCK TABLES');

        return $return;
    }

    /**
     * Update notification status
     *
     * @param integer $id Notification ID
     * @param string $status Notification status
     *
     * @return void
     */
    public function updateNotificationStatus($id, $status)
    {
        global $sql_tbl;

        db_query("LOCK TABLES $sql_tbl[notification_queue] WRITE");

        func_array2update(
            'notification_queue',
            array(
                'status' => $status
            ),
            "id = '$id'"
        );

        db_query('UNLOCK TABLES');
    }

    /**
     * Get orders list
     *
     * @param integer $from      SQL LIMIT from
     * @param integer $size      SQL LIMIT amount
     * @param array   $date      Search intervals OPTIONAL
     * @param string  $status    Search status OPTIONAL
     * @param string  $search    Search string OPTIONAL
     * @param integer $profileId Profile ID OPTIONAL
     *
     * @return \MobileAdmin\Containers\OrdersListItemContainer[]
     */
    protected function getOrdersList($from = 0, $size = 5, array $date = array(), $status = '', $search = '', $profileId = 0)
    {
        global $sql_tbl;

        $return = array();

        $dateCondition     = '';
        $statusCondition   = '';
        $searchCondition   = '';
        $userCondition      = '';

        if ($search) {
            $like = '%' . $search . '%';
            $searchCondition = "AND (o.firstname LIKE '$like' OR o.lastname LIKE '$like' OR o.email LIKE '$like' OR o.b_phone LIKE '$like' OR o.orderid LIKE '$like')";
        }

        if ($date) {
            $dateCondition = "AND o.date >= '$date[from]' AND o.date <= '$date[to]'";
        }

        if ($status) {
            $statusCondition = "AND o.status = '$status'";
        }

        if (0 != $profileId) {
            $userCondition = "AND o.userid = '$profileId'";
        }

        $orders = func_query("
            SELECT o.orderid, o.status, o.total, o.title, o.firstname, o.lastname, o.date, SUM(od.amount) AS items
            FROM $sql_tbl[orders] o
            JOIN $sql_tbl[order_details] od ON o.orderid = od.orderid
            WHERE 1 $dateCondition $statusCondition $searchCondition $userCondition
            GROUP BY o.orderid
            ORDER BY o.date DESC
            LIMIT $from, $size
        ");

        if (empty($orders)) {

            $orders = array();
        }

        foreach ($orders as $k => $order) {
            $order['date']  = Time::getInstance()->convertToUserTime($order['date']);
            $order['month'] = Time::getInstance()->getUserMonthName($order['date'], false);
            $order['day']   = Time::getInstance()->getUserDay($order['date'], false);

            $return[] = new Containers\OrdersListItemContainer($order);
        }

        return $return;
    }


    protected function getOrderById($id)
    {
        x_load('order');

        return func_order_data($id);
    }

    /**
     * Get user info
     *
     * @param integer $id User ID
     *
     * @return mixed
     */
    protected function getUserData($id)
    {
        x_load('user');

        $return = func_userinfo($id);

        $return['last_login'] = (0 == $return['last_login']) ?
            Message::getInstance()->translate(Message::NOT_AVAILABLE)
            : gmdate('M-d-Y', Time::getInstance()->convertToUserTime($return['last_login']));

        return $return;
    }

    /**
     * Gt product info
     *
     * @param integer $id Product ID
     *
     * @return array
     *
     * @throws \MobileAdmin\Exception
     */
    protected function getProductData($id)
    {
        global $current_area;

        x_load('product');

        $current_area = 'A';

        $productInfo = func_select_product($id, 0, false);

        if (
            empty($productInfo)
            || false === $productInfo
        ) {
            throw new Exception(Exception::PRODUCT_NOT_FOUND);
        }

        return $productInfo;
    }

    /**
     * Parse product image path
     *
     * @param string $image   Image path
     * @param string $default Default image path OPTIONAL
     *
     * @return string
     */
    protected function parseProductImagePath($image, $default = '')
    {
        global $http_location;

        $return = $default;

        $isUrl = parse_url($image, PHP_URL_SCHEME);

        if (
            !empty($image)
            && empty($isUrl)
        ) {
            if (false === strpos($image, 'default_image.gif')) {
                $imagePath = pathinfo($image);

                $imagePath = array_merge(explode(XC_DS, $imagePath['dirname']), array($imagePath['basename']));
            } else {
                $imagePath = array('default_image.gif');
            }

            if (
                '.' == $imagePath[0]
                || empty($imagePath[0])
            ) {
                unset($imagePath[0]);
            }

            $return = $http_location . '/' . implode('/', $imagePath);
        } elseif (!empty($isUrl)) {
            $return = $image;
        }

        return $return;
    }

    /**
     * Check if provided ID is anonymous user ID
     *
     * @param string $id Profile ID
     *
     * @return boolean
     */
    protected function checkIfAnonymousId($id)
    {
        return ('A' == substr($id, 0, 1)) ? true : false;
    }

    /**
     * Extract order ID from anonymous user ID
     *
     * @param string $id Profile ID
     *
     * @return integer
     */
    protected function getOrderIdFromAnonymousId($id)
    {
        return (int) substr($id, 1, strlen($id) - 1);
    }

    /**
     * Get user info from order
     *
     * @param integer $id Order ID
     *
     * @return array
     */
    protected function getUserInfoFromOrder($id)
    {
        x_load('order');

        $return = array();

        $order_info = $this->getOrderById($id);

        if (!empty($order_info)) {
            $return = $order_info['userinfo'];

            $return['id']           = $return['userid'];
            $return['login']        = $return['email'];
            $return['usertype']     = 'C';
            $return['last_login']   = gmdate('M-d-Y', Time::getInstance()->convertToUserTime($return['last_login']));
        }

        return $return;
    }
}
