<?php
/* vim: set ts=4 sw=4 sts=4 et: */
/*****************************************************************************\
+-----------------------------------------------------------------------------+
| X-Cart Software license agreement                                           |
| Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>            |
| All rights reserved.                                                        |
+-----------------------------------------------------------------------------+
| PLEASE READ  THE FULL TEXT OF SOFTWARE LICENSE AGREEMENT IN THE "COPYRIGHT" |
| FILE PROVIDED WITH THIS DISTRIBUTION. THE AGREEMENT TEXT IS ALSO AVAILABLE  |
| AT THE FOLLOWING URL: http://www.x-cart.com/license.php                     |
|                                                                             |
| THIS AGREEMENT EXPRESSES THE TERMS AND CONDITIONS ON WHICH YOU MAY USE THIS |
| SOFTWARE PROGRAM AND ASSOCIATED DOCUMENTATION THAT QUALITEAM SOFTWARE LTD   |
| (hereinafter referred to as "THE AUTHOR") OF REPUBLIC OF CYPRUS IS          |
| FURNISHING OR MAKING AVAILABLE TO YOU WITH THIS AGREEMENT (COLLECTIVELY,    |
| THE "SOFTWARE"). PLEASE REVIEW THE FOLLOWING TERMS AND CONDITIONS OF THIS   |
| LICENSE AGREEMENT CAREFULLY BEFORE INSTALLING OR USING THE SOFTWARE. BY     |
| INSTALLING, COPYING OR OTHERWISE USING THE SOFTWARE, YOU AND YOUR COMPANY   |
| (COLLECTIVELY, "YOU") ARE ACCEPTING AND AGREEING TO THE TERMS OF THIS       |
| LICENSE AGREEMENT. IF YOU ARE NOT WILLING TO BE BOUND BY THIS AGREEMENT, DO |
| NOT INSTALL OR USE THE SOFTWARE. VARIOUS COPYRIGHTS AND OTHER INTELLECTUAL  |
| PROPERTY RIGHTS PROTECT THE SOFTWARE. THIS AGREEMENT IS A LICENSE AGREEMENT |
| THAT GIVES YOU LIMITED RIGHTS TO USE THE SOFTWARE AND NOT AN AGREEMENT FOR  |
| SALE OR FOR TRANSFER OF TITLE. THE AUTHOR RETAINS ALL RIGHTS NOT EXPRESSLY  |
| GRANTED BY THIS AGREEMENT.                                                  |
+-----------------------------------------------------------------------------+
\*****************************************************************************/

/**
 * Mobile Admin module functions
 *
 * @category   X-Cart
 * @package    X-Cart
 * @subpackage Mobile Admin
 * @author     Ruslan R. Fazlyev <rrf@x-cart.com>
 * @copyright  Copyright (c) 2001-2013 Qualiteam software Ltd <info@x-cart.com>. All rights reserved
 * @license    http://www.x-cart.com/license.php X-Cart license agreement
 * @version    $Id$
 * @link       http://www.x-cart.com/
 * @see        ____file_see____
 */

if (!defined('XCART_START')) { header('Location: ../../'); die('Access denied');}

function func_mobile_admin_gen_new_key()
{
    $new_key = \MobileAdmin\MobileAdminCore::getInstance()->generateNewKey();
    func_mobile_admin_save_config_var('Mobile_Admin', 'mobile_admin_api_key', $new_key);

    \MobileAdmin\MobileAdminCore::getInstance()->registerStore();

    return $new_key;
}

function func_mobile_admin_save_config_var($category, $name, $value)
{
    return func_array2update(
        'config',
        array(
            'value' => $value,
        ),
        "name = '$name' AND category = '$category'"
    );
}

function func_mobile_admin_get_subscription_state()
{
    return \MobileAdmin\MobileAdminCore::getInstance()->getSubscriptionInfo();
}

function func_mobile_admin_get_all_devices()
{
    $device_list = \MobileAdmin\MobileAdminCore::getInstance()->getDataHandler()->getAllDevices();

    $return = array();

    if (!empty($device_list)) {
        foreach ($device_list as $device) {
            $return[] = $device->toArray();
        }
    }

    return $return;
}

function func_mobile_admin_process_pph_payment($order_id, $pph_order_details)
{
    global $active_modules;

    if (empty($active_modules['PayPal_Here'])) {
        return array();
    }

    $order_id = func_pph_get_orderid_from_pph_orderid($order_id);

    x_load('order');

    $order_data = func_order_data($order_id);

    $return = array();

    if (
        !empty($order_data)
        && isset($order_data['order']['extra']['paypal_here']['sid'])
    ) {
        $adv_info = array();

        foreach ($pph_order_details as $i => $v) {
            $adv_info[] = $i . '=' . $v;
        }

        $adv_info = implode('&', $adv_info);

        $return = array(
            'id'        => $order_id,
            'status'    => func_pph_get_order_status_by_transaction_type($pph_order_details['type']),
            'adv_info'  => $adv_info
        );
    }

    return $return;
}

function func_mobile_admin_new_order_notify($order)
{
    \MobileAdmin\MobileAdminCore::getInstance()->getPushNotification()->sendNewOrderNotification($order['orderid']);
}

function func_mobile_admin_low_stock_notify($product)
{
    \MobileAdmin\MobileAdminCore::getInstance()->getPushNotification()->sendLowStockNotification($product['productid']);
}

/**
 * Store events listeners
 *
 * @param array|void $data Events listeners list
 *
 * @return array
 * @see    ____func_see____
 */
if (!function_exists('func_events_storage')) {
function func_events_storage($data = null)
{
    static $store = array();

    if (is_array($data)) {
        $store = $data;
    }

    return $store;
}
}

/**
 * Call event
 *
 * @param string $event Event name
 *
 * @return mixed
 * @see    ____func_see____
 */
if (!function_exists('func_call_event')) {
function func_call_event($event) {
    $events = func_events_storage();

    $result = null;
    if (isset($events[$event])) {
        $args = func_get_args();
        array_shift($args);
        $result = true;
        foreach ($events[$event] as $callback) {
            if ($callback) {
                $r = call_user_func_array($callback, $args);
                if (false === $r) {
                    break;

                } elseif (isset($r)) {
                    $result = $r;
                }
            }
        }
    }

    return $result;
}
}

/**
 * Add event listener
 *
 * @param string   $event    Event name
 * @param callback $callback Event listener
 *
 * @return boolean|integer
 * @see    ____func_see____
 */
if (!function_exists('func_add_event_listener')) {
function func_add_event_listener($event, $callback) {
    $events = func_events_storage();

    if (!isset($events[$event])) {
        $events[$event] = array();
    }

    if (is_callable($callback)) {
        $idx = count($events[$event]);
        $events[$event][] = $callback;
        func_events_storage($events);

    } else {
        $idx = false;
    }

    return $idx;
}
}

function func_mobadmin_product_is_variant($productid)
{
    return \XCVariantsSQL::isVariant($productid);
}
